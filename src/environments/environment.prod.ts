export const environment = {
  production: true,
  apiUrl: "https://app.pregnabit.com/api/admin",
  url: "https://app.pregnabit.com"
};
