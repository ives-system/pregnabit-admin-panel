export const environment = {
  production: false,
  apiUrl: "https://rc.pregnabit.com/api/admin",
  url: "https://rc.pregnabit.com"
};
