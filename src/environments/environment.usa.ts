export const environment = {
  production: false,
  apiUrl: "https://usa.pregnabit.com/api/admin",
  url: "https://usa.pregnabit.com"
};
