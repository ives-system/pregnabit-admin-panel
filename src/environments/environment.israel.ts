export const environment = {
  production: false,
  apiUrl: "https://israel.pregnabit.com/api/admin",
  url: "https://israel.pregnabit.com"
};
