export const environment = {
  production: false,
  apiUrl: "https://portugal.pregnabit.com/api/admin",
  url: "https://portugal.pregnabit.com"
};
