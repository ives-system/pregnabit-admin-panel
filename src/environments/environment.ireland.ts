export const environment = {
  production: false,
  apiUrl: "https://ireland.pregnabit.com/api/admin",
  url: "https://ireland.pregnabit.com"
};
