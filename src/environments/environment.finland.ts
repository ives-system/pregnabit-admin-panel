export const environment = {
  production: true,
  apiUrl: "https://finland.pregnabit.com/api/admin",
  url: "https://finland.pregnabit.com"
};
