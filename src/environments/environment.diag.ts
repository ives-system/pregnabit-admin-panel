export const environment = {
  production: true,
  apiUrl: "https://diag.pregnabit.com/api/admin",
  url: "https://diag.pregnabit.com"
};
