export const environment = {
  production: false,
  apiUrl: "https://preprod.pregnabit.com/api/admin",
  url: "https://preprod.pregnabit.com"
};
