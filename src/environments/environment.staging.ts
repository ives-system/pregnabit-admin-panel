export const environment = {
  production: false,
  apiUrl: "https://staging.pregnabit.com/api/admin",
  url: "https://staging.pregnabit.com"
};
