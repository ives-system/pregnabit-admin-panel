var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var helpers_1 = require("../../helpers");
var LogoutComponent = (function () {
    function LogoutComponent(_router, _authService) {
        this._router = _router;
        this._authService = _authService;
    }
    LogoutComponent.prototype.ngOnInit = function () {
        var _this = this;
        helpers_1.Helpers.setLoading(true);
        this._authService.logout()
            .then(function (result) {
            _this._router.navigate(['/login']);
        });
    };
    LogoutComponent = __decorate([
        core_1.Component({
            selector: 'app-logout',
            templateUrl: './logout.component.html',
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], LogoutComponent);
    return LogoutComponent;
})();
exports.LogoutComponent = LogoutComponent;
//# sourceMappingURL=logout.component.js.map