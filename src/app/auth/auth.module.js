var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var testing_1 = require("@angular/http/testing");
var http_client_1 = require("../_services/http_client");
var core_2 = require('@ngx-translate/core');
var auth_routing_routing_1 = require("./auth-routing.routing");
var auth_component_1 = require("./auth.component");
var alert_component_1 = require("./_directives/alert.component");
var logout_component_1 = require("./logout/logout.component");
var password_component_1 = require("./password/password.component");
var auth_guard_1 = require("./_guards/auth.guard");
var role_guard_1 = require("./_guards/role.guard");
var alert_service_1 = require("./_services/alert.service");
var authentication_service_1 = require("./_services/authentication.service");
var user_service_1 = require("./_services/user.service");
var AuthModule = (function () {
    function AuthModule() {
    }
    AuthModule = __decorate([
        core_1.NgModule({
            declarations: [
                auth_component_1.AuthComponent,
                alert_component_1.AlertComponent,
                logout_component_1.LogoutComponent,
                password_component_1.PasswordComponent
            ],
            imports: [
                common_1.CommonModule,
                forms_1.FormsModule,
                http_1.HttpModule,
                auth_routing_routing_1.AuthRoutingModule,
                core_2.TranslateModule
            ],
            providers: [
                auth_guard_1.AuthGuard,
                role_guard_1.RoleGuard,
                alert_service_1.AlertService,
                authentication_service_1.AuthenticationService,
                user_service_1.UserService,
                // api backend simulation
                testing_1.MockBackend,
                http_1.BaseRequestOptions,
                http_client_1.HttpClient
            ],
            entryComponents: [alert_component_1.AlertComponent]
        })
    ], AuthModule);
    return AuthModule;
})();
exports.AuthModule = AuthModule;
//# sourceMappingURL=auth.module.js.map