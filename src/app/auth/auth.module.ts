import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";
import {BaseRequestOptions, HttpModule} from "@angular/http";
import {MockBackend} from "@angular/http/testing";
import {HttpClient} from "../_services/http_client";
import { TranslateModule } from '@ngx-translate/core';


import {AuthRoutingModule} from "./auth-routing.routing";
import {AuthComponent} from "./auth.component";
import {AlertComponent} from "./_directives/alert.component";
import {LogoutComponent} from "./logout/logout.component";
import {PasswordComponent} from "./password/password.component";
import {AuthGuard} from "./_guards/auth.guard";
import {RoleGuard} from "./_guards/role.guard";
import {AlertService} from "./_services/alert.service";
import {AuthenticationService} from "./_services/authentication.service";
import {UserService} from "./_services/user.service";

@NgModule({
	declarations: [
		AuthComponent,
		AlertComponent,
		LogoutComponent,
    PasswordComponent
	],
	imports: [
		CommonModule,
		FormsModule,
		HttpModule,
		AuthRoutingModule,
    TranslateModule
	],
	providers: [
		AuthGuard,
    RoleGuard,
		AlertService,
		AuthenticationService,
		UserService,
		// api backend simulation
		MockBackend,
		BaseRequestOptions,
    HttpClient
	],
	entryComponents: [AlertComponent]
})

export class AuthModule {
}
