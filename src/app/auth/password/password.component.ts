import {Component, ComponentFactoryResolver, OnInit, ViewEncapsulation, ViewChild, ViewContainerRef} from "@angular/core";
import {Router, ActivatedRoute} from "@angular/router";
import {AlertService} from "../_services/alert.service";
import {AlertComponent} from "../_directives/alert.component";
import {UserService} from "../_services/user.service";
import {AuthenticationService} from "../_services/authentication.service";
import {ScriptLoaderService} from "../../_services/script-loader.service";
import {Helpers} from "../../helpers";
import { TranslateService } from '@ngx-translate/core';
import {SwitchLanguage} from "../../_services/switch_language";

@Component({
  templateUrl: './password.component.html',
  encapsulation: ViewEncapsulation.None,
})

export class PasswordComponent implements OnInit {

  model: any = {};
  loading = false;
  reset_password_token:any;

  @ViewChild('alertSignin', {read: ViewContainerRef}) alertSignin: ViewContainerRef;

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private _script: ScriptLoaderService,
    private _router: Router,
    private _alertService: AlertService,
    private _authService: AuthenticationService,
    private cfr: ComponentFactoryResolver,
    private translate: TranslateService,
    public switchLanguage: SwitchLanguage) {
  }

  ngOnInit()  {
    this.route.queryParams.subscribe(params => {
      this.reset_password_token = params['reset_password_token'];
    });
    this._script.load('body', 'assets/vendors/base/vendors.bundle.js', 'assets/demo/default/base/scripts.bundle.js')
      .then(() => {
        Helpers.setLoading(false);
      });
  }

  updatePassword() {
    this.loading = true;
    this.userService.updatePassword(this.reset_password_token, this.model.password, this.model.passwordConfirmation).then((result) => {
      if(result.changed) {
        this.model = {};
        this.showAlert('alertSignin');
        this.translate.get('admin_panel.reset_password.password_changed').subscribe((res: string) => {
          this._alertService.success(res, true);
        });
        this.loading = false;
        setTimeout(() => this._router.navigateByUrl('/login'), 3000)
      } else {
        this.showAlert('alertSignin');
        this.translate.get('admin_panel.reset_password.error').subscribe((res: string) => {
          this._alertService.error(res);
        });
        this.loading = false;
      };
    }, (err) => {
      this.showAlert('alertSignin');
      this.translate.get('admin_panel.reset_password.error').subscribe((res: string) => {
        this._alertService.error(res);
      });
      this.loading = false;
    });
  }

  showAlert(target) {
    this[target].clear();
    let factory = this.cfr.resolveComponentFactory(AlertComponent);
    let ref = this[target].createComponent(factory);
    ref.changeDetectorRef.detectChanges();
  }

  setLanguage(lang) {
    this.switchLanguage.switchLanguage(lang)
  }
}
