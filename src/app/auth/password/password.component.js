var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var alert_component_1 = require("../_directives/alert.component");
var helpers_1 = require("../../helpers");
var PasswordComponent = (function () {
    function PasswordComponent(route, userService, _script, _router, _alertService, _authService, cfr, translate, switchLanguage) {
        this.route = route;
        this.userService = userService;
        this._script = _script;
        this._router = _router;
        this._alertService = _alertService;
        this._authService = _authService;
        this.cfr = cfr;
        this.translate = translate;
        this.switchLanguage = switchLanguage;
        this.model = {};
        this.loading = false;
    }
    PasswordComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.queryParams.subscribe(function (params) {
            _this.reset_password_token = params['reset_password_token'];
        });
        this._script.load('body', 'assets/vendors/base/vendors.bundle.js', 'assets/demo/default/base/scripts.bundle.js')
            .then(function () {
            helpers_1.Helpers.setLoading(false);
        });
    };
    PasswordComponent.prototype.updatePassword = function () {
        var _this = this;
        this.loading = true;
        this.userService.updatePassword(this.reset_password_token, this.model.password, this.model.passwordConfirmation).then(function (result) {
            if (result.changed) {
                _this.model = {};
                _this.showAlert('alertSignin');
                _this.translate.get('admin_panel.reset_password.password_changed').subscribe(function (res) {
                    _this._alertService.success(res, true);
                });
                _this.loading = false;
                setTimeout(function () { return _this._router.navigateByUrl('/login'); }, 3000);
            }
            else {
                _this.showAlert('alertSignin');
                _this.translate.get('admin_panel.reset_password.error').subscribe(function (res) {
                    _this._alertService.error(res);
                });
                _this.loading = false;
            }
            ;
        }, function (err) {
            _this.showAlert('alertSignin');
            _this.translate.get('admin_panel.reset_password.error').subscribe(function (res) {
                _this._alertService.error(res);
            });
            _this.loading = false;
        });
    };
    PasswordComponent.prototype.showAlert = function (target) {
        this[target].clear();
        var factory = this.cfr.resolveComponentFactory(alert_component_1.AlertComponent);
        var ref = this[target].createComponent(factory);
        ref.changeDetectorRef.detectChanges();
    };
    PasswordComponent.prototype.setLanguage = function (lang) {
        this.switchLanguage.switchLanguage(lang);
    };
    __decorate([
        core_1.ViewChild('alertSignin', { read: core_1.ViewContainerRef })
    ], PasswordComponent.prototype, "alertSignin");
    PasswordComponent = __decorate([
        core_1.Component({
            templateUrl: './password.component.html',
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], PasswordComponent);
    return PasswordComponent;
})();
exports.PasswordComponent = PasswordComponent;
//# sourceMappingURL=password.component.js.map