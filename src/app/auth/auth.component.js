var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var alert_component_1 = require("./_directives/alert.component");
var login_custom_1 = require("./_helpers/login-custom");
var helpers_1 = require("../helpers");
var AuthComponent = (function () {
    function AuthComponent(_router, _script, _userService, _route, _authService, _alertService, cfr, switchLanguage, translate) {
        this._router = _router;
        this._script = _script;
        this._userService = _userService;
        this._route = _route;
        this._authService = _authService;
        this._alertService = _alertService;
        this.cfr = cfr;
        this.switchLanguage = switchLanguage;
        this.translate = translate;
        this.model = {};
        this.loading = false;
    }
    AuthComponent.prototype.ngOnInit = function () {
        this.model.remember = false;
        // get return url from route parameters or default to '/'
        this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/';
        this._router.navigate([this.returnUrl]);
        this._script.load('body', 'assets/vendors/base/vendors.bundle.js', 'assets/demo/default/base/scripts.bundle.js')
            .then(function () {
            helpers_1.Helpers.setLoading(false);
            login_custom_1.LoginCustom.init();
        });
    };
    AuthComponent.prototype.signin = function () {
        var _this = this;
        this.loading = true;
        this._authService.login(this.model.email, this.model.password)
            .subscribe(function (data) {
            _this._router.navigate([_this.returnUrl]);
        }, function (error) {
            _this.showAlert('alertSignin');
            _this.translate.get('admin_panel.login.wrong_credentials').subscribe(function (res) {
                _this._alertService.error(res);
            });
            _this.loading = false;
        });
    };
    AuthComponent.prototype.forgotPass = function () {
        var _this = this;
        this.loading = true;
        this._userService.forgotPassword(this.model.email)
            .then(function (data) {
            _this.showAlert('alertSignin');
            _this.translate.get('admin_panel.reset_password.info_send').subscribe(function (res) {
                _this._alertService.success(res, true);
            });
            _this.loading = false;
            login_custom_1.LoginCustom.displaySignInForm();
            _this.model = {};
        }, function (error) {
            _this.showAlert('alertForgotPass');
            _this.translate.get('admin_panel.reset_password.wrong_email').subscribe(function (res) {
                _this._alertService.error(res);
            });
            _this.loading = false;
        });
    };
    AuthComponent.prototype.setLanguage = function (lang) {
        this.switchLanguage.switchLanguage(lang);
    };
    AuthComponent.prototype.showAlert = function (target) {
        this[target].clear();
        var factory = this.cfr.resolveComponentFactory(alert_component_1.AlertComponent);
        var ref = this[target].createComponent(factory);
        ref.changeDetectorRef.detectChanges();
    };
    __decorate([
        core_1.ViewChild('alertSignin', { read: core_1.ViewContainerRef })
    ], AuthComponent.prototype, "alertSignin");
    __decorate([
        core_1.ViewChild('alertSignup', { read: core_1.ViewContainerRef })
    ], AuthComponent.prototype, "alertSignup");
    __decorate([
        core_1.ViewChild('alertForgotPass', { read: core_1.ViewContainerRef })
    ], AuthComponent.prototype, "alertForgotPass");
    AuthComponent = __decorate([
        core_1.Component({
            selector: ".m-grid.m-grid--hor.m-grid--root.m-page",
            templateUrl: './templates/login-3.component.html',
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], AuthComponent);
    return AuthComponent;
})();
exports.AuthComponent = AuthComponent;
//# sourceMappingURL=auth.component.js.map