var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var Rx_1 = require("rxjs/Rx");
var AuthGuard = (function () {
    function AuthGuard(_router, _userService) {
        this._router = _router;
        this._userService = _userService;
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        var _this = this;
        return new Rx_1.Observable(function (observer) {
            if (localStorage.getItem('currentUser')) {
                observer.next(true);
                observer.complete();
                return;
            }
            _this._userService.currentUser().then(function (response) {
                _this._userService.user = response.data;
                observer.next(true);
                observer.complete();
            }).catch(function (response) { _this.handleError(response, observer, state); });
        });
    };
    AuthGuard.prototype.handleError = function (response, observer, state) {
        observer.next(false);
        observer.complete();
        this._router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    };
    AuthGuard = __decorate([
        core_1.Injectable()
    ], AuthGuard);
    return AuthGuard;
})();
exports.AuthGuard = AuthGuard;
//# sourceMappingURL=auth.guard.js.map