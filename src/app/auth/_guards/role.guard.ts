import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';
import { UserService } from '../_services/user.service'
import { Injectable } from "@angular/core";

@Injectable()
export class RoleGuard implements CanActivate {
  constructor(private userService: UserService, private router: Router) { }
  async canActivate(route: ActivatedRouteSnapshot) {
    const user = JSON.parse(localStorage.getItem('user'))
    if (route.data.expectedRoles.includes(user.role)) {
      return true
    } else {
      this.router.navigateByUrl('/404');
    }
  }
}
