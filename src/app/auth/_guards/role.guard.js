var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var RoleGuard = (function () {
    function RoleGuard(userService, router) {
        this.userService = userService;
        this.router = router;
    }
    RoleGuard.prototype.canActivate = function (route) {
        if (route.data.expectedRoles.includes(this.userService.user.role)) {
            return true;
        }
        else {
            this.router.navigateByUrl('/404');
        }
    };
    RoleGuard = __decorate([
        core_1.Injectable()
    ], RoleGuard);
    return RoleGuard;
})();
exports.RoleGuard = RoleGuard;
//# sourceMappingURL=role.guard.js.map