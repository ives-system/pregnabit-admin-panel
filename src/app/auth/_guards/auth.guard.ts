import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {UserService} from "../_services/user.service";
import {Observable} from "rxjs/Rx";

@Injectable()
export class AuthGuard implements CanActivate {

	constructor(
    private _router: Router,
    private _userService: UserService
  ) {}

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {

    return new Observable<boolean>((observer) => {
      if(localStorage.getItem('currentUser')) {
        observer.next(true);
        observer.complete();
        return
      }

      this._userService.currentUser().then(response => {
        this._userService.user = response.data;
        observer.next(true);
        observer.complete();
      }).catch(response => { this.handleError(response, observer, state) });
    });
  }

  handleError(response, observer, state) {
    observer.next(false);
    observer.complete();
    this._router.navigate(['/login'], {queryParams: {returnUrl: state.url}});
  }
}
