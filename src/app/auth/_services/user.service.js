var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
require('rxjs/add/operator/toPromise');
var UserService = (function () {
    function UserService(http, httpClient) {
        this.http = http;
        this.httpClient = httpClient;
    }
    UserService.prototype.currentUser = function () {
        return this.httpClient.get('/users/current_user')
            .toPromise()
            .then(function (response) { return response.json(); });
    };
    UserService.prototype.fetchCurrentUser = function () {
        if (!this.isAuthenticated()) {
            this.forceFetchCurrentUser();
        }
    };
    UserService.prototype.forceFetchCurrentUser = function () {
        var _this = this;
        this.currentUser().then(function (response) {
            _this.user = response.data;
        }).catch(function (response) { _this.handleError(response); });
    };
    UserService.prototype.isAuthenticated = function () {
        return this.user !== undefined;
    };
    UserService.prototype.handleError = function (error) { };
    UserService.prototype.forgotPassword = function (email) {
        return this.httpClient.post('/password', { email: email })
            .toPromise()
            .then(function (response) { return response.json(); });
    };
    UserService.prototype.updatePassword = function (resetPasswordToken, password, passwordConfirmation) {
        return this.httpClient.put('/password', { admin_user: { reset_password_token: resetPasswordToken, password: password, password_confirmation: passwordConfirmation } })
            .toPromise()
            .then(function (response) { return response.json(); });
    };
    UserService = __decorate([
        core_1.Injectable()
    ], UserService);
    return UserService;
})();
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map