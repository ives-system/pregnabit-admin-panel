var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
var environment_1 = require("../../../environments/environment");
var apiUrl = environment_1.environment.apiUrl;
var AuthenticationService = (function () {
    function AuthenticationService(http, userService) {
        this.http = http;
        this.userService = userService;
    }
    AuthenticationService.prototype.login = function (email, password) {
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        headers.append('X-Authentication-Token', localStorage.getItem('token'));
        return this.http.post(apiUrl + '/login', JSON.stringify({ email: email, password: password }), { headers: headers })
            .map(function (response) {
            var token = response.json().authenticationToken;
            var user = response.json().user;
            if (user && token) {
                localStorage.setItem('currentUser', user);
                localStorage.setItem('token', token);
            }
        });
    };
    AuthenticationService.prototype.logout = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new http_1.Headers();
            headers.append('X-Authentication-Token', localStorage.getItem('token'));
            _this.http.post(apiUrl + '/logout', {}, { headers: headers })
                .subscribe(function (res) {
                _this.userService.user = undefined;
                localStorage.removeItem('currentUser');
                localStorage.removeItem('token');
                resolve(res.json());
            });
        });
    };
    AuthenticationService.prototype.heartbeat = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new http_1.Headers();
            headers.append('X-Authentication-Token', localStorage.getItem('token'));
            _this.http.post(apiUrl + '/heartbeat', {}, { headers: headers })
                .subscribe(function (res) {
                resolve(res.json());
            }, function (err) {
                reject(err);
            });
        });
    };
    AuthenticationService = __decorate([
        core_1.Injectable()
    ], AuthenticationService);
    return AuthenticationService;
})();
exports.AuthenticationService = AuthenticationService;
//# sourceMappingURL=authentication.service.js.map