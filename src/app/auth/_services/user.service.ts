import {Injectable} from "@angular/core";
import {Headers, Http, RequestOptions, Response} from "@angular/http";
import {HttpClient} from "../../_services/http_client";
import 'rxjs/add/operator/toPromise';


import {User} from "../_models/index";

@Injectable()
export class UserService {
  user: any;
  constructor
  (
    private http: Http,
    private httpClient: HttpClient
  ) {
  }


  currentUser(): Promise<any> {
    const locale = localStorage.getItem('language')
    return this.httpClient.get('/users/current_user?locale=' + locale)
      .toPromise()
      .then(response => response.json())
  }

  fetchCurrentUser(): void {
    if (!this.isAuthenticated()) {

      this.forceFetchCurrentUser();
    }
  }

  forceFetchCurrentUser(): void {
    this.currentUser().then( response => {
      localStorage.setItem('user', JSON.stringify(response))
      this.user = response
    }).catch(response => { this.handleError(response) });
  }

  isAuthenticated() {
    return this.user !== undefined;
  }

  handleError(error) {
    console.log('kotarba', error)
  }



  forgotPassword(email: string): Promise<any>  {
		return this.httpClient.post('/password', {email: email})
      .toPromise()
      .then(response => response.json());
	}

  updatePassword(resetPasswordToken, password, passwordConfirmation): Promise<any> {
    return this.httpClient.put('/password', {admin_user: { reset_password_token: resetPasswordToken, password: password, password_confirmation: passwordConfirmation }})
      .toPromise()
      .then(response => response.json());
  }

}
