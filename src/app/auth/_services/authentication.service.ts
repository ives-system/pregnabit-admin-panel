import {Injectable} from "@angular/core";
import {Http, Headers, Response} from "@angular/http";
import "rxjs/add/operator/map";
import {environment} from "../../../environments/environment";
import { UserService } from '../_services/user.service'


let apiUrl = environment.apiUrl;

@Injectable()
export class AuthenticationService {

	constructor(
    private http: Http,
    private userService: UserService
  ) {
	}

	login(email: string, password: string) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    headers.append('X-Authentication-Token', localStorage.getItem('token'));
    return this.http.post(apiUrl + '/login', JSON.stringify({email: email, password: password}), {headers: headers, withCredentials: true})
			.map((response: Response) => {
        let token = response.json().authenticationToken;
        let user = response.json().user;
				if (user && token) {
					localStorage.setItem('currentUser', JSON.stringify(user));
          localStorage.setItem('token', token);
        }
			});
	}

  logout(){
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('X-Authentication-Token', localStorage.getItem('token'));
      this.http.post(apiUrl + '/logout', {}, {headers: headers, withCredentials: true})
        .subscribe(res => {
          this.userService.user = undefined;
          localStorage.removeItem('currentUser');
          localStorage.removeItem('token');
          resolve(res.json());
        });
    });
  }

  heartbeat() {
    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('X-Authentication-Token', localStorage.getItem('token'));
      resolve(true)
      // this.http.post(apiUrl + '/heartbeat', {}, {headers: headers})
      //   .subscribe(res => {
      //     resolve(res.json());
      //   }, (err) => {
      //     reject(err);
      //   });
    });
  }

}
