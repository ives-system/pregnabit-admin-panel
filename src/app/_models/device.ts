export class Device {
  identifier:any;
  pin:any;
  medicalUnitId:any;
  doctorId:any;
  patientId:any;
  merchantUserId:any;
  dateOfDeviceRelease:any;
  comments:any;
}
