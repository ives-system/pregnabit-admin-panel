export class Doctor {
  firstName:any;
  lastName:any;
  phoneNumber:any;
  email:any;
  title:any;
  licence:any;
  specializations:any;
  merchantId:any;
  medicalUnitId:any;
  company:any;
  isBusinessOwner:any;
  nip:any;
  street:any;
  streetNumber:any;
  apartmentNumber:any;
  zipCode:any;
  city:any;
  typeOfActivity:any;
  companySize:any;
  canEditPatients:any;
  canEditDevices:any;
  canSeeAllPatients:any;
  canSeeMedicalRecords:any;
  visibleExaminations:any;

}
