import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Patient } from '../../../../_models/patient';
import { PatientService } from '../../../../_services/patient.service';
import { DoctorService } from '../../../../_services/doctor.service';
import { TranslateService } from '@ngx-translate/core';
import { Ng2TelInput } from "ng2-tel-input";

@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./patients_edit.component.html",
  providers: [DoctorService, PatientService],
  encapsulation: ViewEncapsulation.None,
})
export class PatientsEditComponent implements OnInit {
  id: number;
  patient: Patient;
  patientName:any;
  editPatientForm: FormGroup;
  submitted = false;
  doctors: any;
  pregnancyCountError = false;

  @ViewChild(Ng2TelInput) phoneNumber: Ng2TelInput;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    public doctorService: DoctorService,
    public formBuilder: FormBuilder,
    private patientService: PatientService,
    private translate: TranslateService
  ) {
    this.patient = new Patient();
    this.editPatientForm = formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      phoneNumber: [''],
      street: [''],
      zipCode: ['', Validators.compose([Validators.maxLength(10) ,Validators.pattern(/^[0-9A-Za-z-]+$/)])],
      city: [''],
      identifier: ['', Validators.compose([Validators.required, Validators.pattern(/^[a-zA-Z0-9_-]{1,30}$/)])],
      doctorId: ['', Validators.required],
      birthday: ['', Validators.compose([Validators.required, Validators.pattern(/^(0[1-9]|[12][0-9]|3[01])([-\/.])(0[1-9]|1[012])\2(19|20)\d\d$/)])],
      pregnancyCount: ['', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/)])],
      birthCount: ['', Validators.compose([ Validators.required, Validators.pattern(/^[0-9]*$/)])],
      pregnancyWeek: ['', Validators.compose([Validators.required, Validators.max(42), Validators.min(1), Validators.pattern(/^[0-9]*$/)])],
      pregnancyDay: ['', Validators.compose([Validators.required, Validators.pattern(/^([1-7])$/)])],
      diagnosis: [''],
      medicineTaken: [''],
      lang: ['', Validators.required]

    });
  }


  ngOnInit()  {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.patientService.show(this.id).then(response => {
        this.patient = response;
        this.patientName = this.patient['fullName'];
        this.phoneNumber.ngTelInput.intlTelInput('setNumber', this.patient['phoneNumber']);
      })
    });
    this.doctorService.list().then(response => {
      this.doctors = response;
    })
  }

  validateBirthCount() {
    const invalidCount = this.patient.birthCount > this.patient.pregnancyCount
    if (invalidCount) {
      this.pregnancyCountError = true
      this.editPatientForm.controls['pregnancyCount'].setErrors({ 'incorrect': true })
      this.editPatientForm.controls['birthCount'].setErrors({ 'incorrect': true })
    } else {
      this.editPatientForm.controls['pregnancyCount'].setErrors(null)
      this.editPatientForm.controls['birthCount'].setErrors(null)
      this.pregnancyCountError = false
    }
  }

  updatePatient() {
    this.submitted = true;
    if(this.editPatientForm.valid) {
      this.patientService.update(this.patient).then((result) => {
        this.translate.get('admin_panel.patients.form.updated').subscribe((res: string) => {
          this.toastr.success(res);
        });
        this.router.navigateByUrl('/patients/'+this.id);
      }, (err) => {
        var error = JSON.parse(err._body).messages.join(', ');
        this.translate.get('admin_panel.common.error').subscribe((res: string) => {
          this.toastr.error(error, res);
        });
      });
    } else {
      console.log('invalid');
    }
  }

  telInputObject(obj) {
  }

  hasError: boolean;
  onError(obj) {
    this.hasError = obj;
  }

  getNumber(obj) {
    console.log(obj)
    this.patient['phoneNumber'] = obj;
  }

  onCountryChange(obj) {
  }

}
