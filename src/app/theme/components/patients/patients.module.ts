import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { PatientsIndexComponent } from './index/patients_index.component';
import { PatientsNewComponent } from './new/patients_new.component';
import { PatientsShowComponent } from './show/patients_show.component';
import { PatientsEditComponent } from './edit/patients_edit.component';
import { PatientsEditPasswordComponent } from './edit_password/patients_edit_password.component';
import { LayoutModule } from '../../layouts/layout.module';
import { DefaultComponent } from '../../pages/default/default.component';
import { ValidatorsService } from '../../../_services/validators.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { Ng2TelInputModule } from 'ng2-tel-input';

const routes: Routes = [
  {
    "path": "",
    "component": DefaultComponent,
    "children": [
      {
        "path": "",
        "component": PatientsIndexComponent
      },
      {
        "path": "new",
        "component": PatientsNewComponent
      },
      {
        "path": ":id",
        "component": PatientsShowComponent
      },
      {
        "path": "edit\/:id",
        "component": PatientsEditComponent
      },
      {
        "path": "edit_password\/:id",
        "component": PatientsEditPasswordComponent
      }
    ]
  }
];


@NgModule({imports: [
  CommonModule,
  RouterModule.forChild(routes),
  LayoutModule,
  FormsModule,
  ReactiveFormsModule,
  ToastrModule.forRoot({positionClass: 'toast-bottom-right'}),
  NgbModule,
  TranslateModule,
  Ng2TelInputModule.forRoot()
],exports: [
  RouterModule
], providers: [
  ValidatorsService
],declarations: [
  PatientsIndexComponent,
  PatientsShowComponent,
  PatientsEditPasswordComponent,
  PatientsEditComponent,
  PatientsNewComponent
]})


export class PatientsModule  {

}
