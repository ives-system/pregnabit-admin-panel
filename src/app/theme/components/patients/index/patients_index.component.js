var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var doctor_service_1 = require('../../../../_services/doctor.service');
var patient_service_1 = require('../../../../_services/patient.service');
var PatientsIndexComponent = (function () {
    function PatientsIndexComponent(doctorService, patientService) {
        this.doctorService = doctorService;
        this.patientService = patientService;
        this.limit = 10;
        this.query = '';
        this.filters = { doctorId: '' };
    }
    PatientsIndexComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadRecords(1);
        this.doctorService.list().then(function (response) {
            _this.doctors = response;
        });
    };
    PatientsIndexComponent.prototype.changeTypeOfPatients = function (type) {
        this.typeOfPatients = type;
        this.loadRecords(1);
    };
    PatientsIndexComponent.prototype.loadRecords = function (page) {
        var _this = this;
        if (page === void 0) { page = this.pagination.offset; }
        this.patientService.index({
            offset: page, limit: this.limit, query: this.query, sortModel: this.sortModel,
            sortOrder: this.sortOrder, doctorId: this.filters.doctorId, typeOfPatients: this.typeOfPatients
        }).then(function (response) {
            _this.pagination = response.pagination;
            _this.patients = response.patients;
        });
    };
    PatientsIndexComponent.prototype.sort = function (sortModel) {
        if (this.sortModel != sortModel) {
            this.sortOrder = 'asc';
        }
        else {
            if (this.sortOrder == 'asc') {
                this.sortOrder = 'desc';
            }
            else {
                this.sortOrder = 'asc';
            }
        }
        this.sortModel = sortModel;
        this.loadRecords();
    };
    PatientsIndexComponent.prototype.changeLimit = function (limit) {
        this.limit = limit;
        this.loadRecords(1);
    };
    PatientsIndexComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./patients_index.component.html",
            providers: [patient_service_1.PatientService, doctor_service_1.DoctorService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], PatientsIndexComponent);
    return PatientsIndexComponent;
})();
exports.PatientsIndexComponent = PatientsIndexComponent;
//# sourceMappingURL=patients_index.component.js.map