import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { DoctorService } from '../../../../_services/doctor.service';
import { PatientService } from '../../../../_services/patient.service';

@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./patients_index.component.html",
  providers: [PatientService, DoctorService],
  encapsulation: ViewEncapsulation.None,
})
export class PatientsIndexComponent implements OnInit {

  limit=10;
  query='';
  pagination:any;
  patients:any;
  sortModel:any;
  sortOrder:any;
  typeOfPatients:any;
  doctors:any;
  filters = {doctorId: ''};

  constructor(
    private doctorService: DoctorService,
    private patientService: PatientService
  )  {}

  ngOnInit()  {
    this.loadRecords(1);
    this.doctorService.list().then(response => {
      this.doctors = response;
    });
  }

  changeTypeOfPatients(type) {
    this.typeOfPatients = type;
    this.loadRecords(1);
  }




  loadRecords(page=this.pagination.offset) {
    this.patientService.index({
      offset: page, limit: this.limit, query: this.query, sortModel: this.sortModel,
      sortOrder: this.sortOrder, doctorId: this.filters.doctorId, typeOfPatients: this.typeOfPatients
    }).then(response => {
      this.pagination = response.pagination;
      this.patients = response.patients;
    })
  }

  sort(sortModel) {
    if(this.sortModel != sortModel) {
      this.sortOrder = 'asc';
    } else {
      if(this.sortOrder == 'asc') {
        this.sortOrder = 'desc';
      } else {
        this.sortOrder = 'asc';
      }
    }
    this.sortModel = sortModel;
    this.loadRecords();
  }

  changeLimit(limit) {
    this.limit = limit;
    this.loadRecords(1);
  }
}
