import { Component, ViewEncapsulation } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Patient } from '../../../../_models/patient';
import { DoctorService } from '../../../../_services/doctor.service';
import { PatientService } from '../../../../_services/patient.service';
import { TranslateService } from '@ngx-translate/core';
import CountrySelector from "../../../../_utils/country_selector";

@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./patients_new.component.html",
  providers: [DoctorService, PatientService],
  encapsulation: ViewEncapsulation.None,
})
export class PatientsNewComponent {
  patient: Patient;
  newPatientForm: FormGroup;
  submitted = false;
  doctors:any;
  pregnancyCountError = false;

  constructor(
    private router: Router,
    private toastr: ToastrService,
    public patientService: PatientService,
    public doctorService: DoctorService,
    public formBuilder: FormBuilder,
    private translate: TranslateService

  ) {
    this.patient = new Patient();
    this.newPatientForm = formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      phoneNumber: [''],
      street: [''],
      zipCode: ['', Validators.compose([Validators.maxLength(10) ,Validators.pattern(/^[0-9A-Za-z-]+$/)])],
      city: [''],
      identifier: ['', Validators.compose([Validators.required, Validators.pattern(/^[a-zA-Z0-9_-]{1,30}$/)])],
      doctorId: ['', Validators.required],
      birthday: ['', Validators.compose([Validators.required, Validators.pattern(/^(0[1-9]|[12][0-9]|3[01])([-\/.])(0[1-9]|1[012])\2(19|20)\d\d$/)])],
      pregnancyCount: ['', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/)])],
      birthCount: ['', Validators.compose([Validators.required, Validators.pattern(/^[0-9]*$/)])],
      pregnancyWeek: ['', Validators.compose([Validators.required, Validators.min(1), Validators.max(42), Validators.pattern(/^[0-9]*$/)])],
      pregnancyDay: ['', Validators.compose([Validators.required, Validators.pattern(/^([1-7])$/)])],
      diagnosis: [''],
      medicineTaken: [''],
      lang: ['', Validators.required]

    });
  }

  ngOnInit()  {
    this.doctorService.list().then(response => {
      console.log('response', response)
      this.doctors = response;
    })
  }

  createPatient() {
    this.submitted = true;
    if(this.newPatientForm.valid) {
      this.patientService.create(this.patient).then((result) => {
        this.translate.get('admin_panel.patients.form.added').subscribe((res: string) => {
          this.toastr.success(res);
        });
        this.router.navigateByUrl('/patients/'+ result.id);
      }, (err) => {
        var error = JSON.parse(err._body).messages.patient.join(', ');
        this.translate.get('admin_panel.common.error').subscribe((res: string) => {
          this.toastr.error(error, res);
        });
      });
    } else {
      console.log('invalid');
    }
  }

  validateBirthCount() {
    const invalidCount = this.patient.birthCount > this.patient.pregnancyCount
    if (invalidCount) {
      this.pregnancyCountError = true
      this.newPatientForm.controls['pregnancyCount'].setErrors({ 'incorrect': true })
      this.newPatientForm.controls['birthCount'].setErrors({ 'incorrect': true })
    } else {
      this.pregnancyCountError = false
      this.newPatientForm.controls['pregnancyCount'].setErrors(null)
      this.newPatientForm.controls['birthCount'].setErrors(null)
    }
  }

  telInputObject(obj) {
    CountrySelector.select(obj);
  }

  hasError: boolean;
  onError(obj) {
    this.hasError = obj;
    console.log('hasError: ', obj);
  }

  getNumber(obj) {
    this.patient['phoneNumber'] = obj;
  }

}
