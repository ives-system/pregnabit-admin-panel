var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var patient_1 = require('../../../../_models/patient');
var doctor_service_1 = require('../../../../_services/doctor.service');
var patient_service_1 = require('../../../../_services/patient.service');
var PatientsNewComponent = (function () {
    function PatientsNewComponent(router, toastr, patientService, doctorService, formBuilder, translate) {
        this.router = router;
        this.toastr = toastr;
        this.patientService = patientService;
        this.doctorService = doctorService;
        this.formBuilder = formBuilder;
        this.translate = translate;
        this.submitted = false;
        this.patient = new patient_1.Patient();
        this.newPatientForm = formBuilder.group({
            firstName: ['', forms_1.Validators.required],
            lastName: ['', forms_1.Validators.required],
            email: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.email])],
            phoneNumber: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.pattern(/^[0-9]+$/)])],
            street: [''],
            zipCode: ['', forms_1.Validators.pattern(/^[0-9]{2}-[0-9]{3}$/)],
            city: [''],
            pesel: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.pattern(/^[0-9]{11}$/)])],
            doctorId: ['', forms_1.Validators.required],
            birthday: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.pattern(/^(0[1-9]|[12][0-9]|3[01])([-\/.])(0[1-9]|1[012])\2(19|20)\d\d$/)])],
            pregnancyCount: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.pattern(/^[0-9]*$/)])],
            birthCount: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.pattern(/^[0-9]*$/)])],
            pregnancyWeek: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.pattern(/^[0-9]*$/)])],
            pregnancyDay: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.pattern(/^([1-7])$/)])],
            diagnosis: [''],
            medicineTaken: [''],
            lang: ['', forms_1.Validators.required]
        });
    }
    PatientsNewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.doctorService.list().then(function (response) {
            _this.doctors = response;
        });
    };
    PatientsNewComponent.prototype.createPatient = function () {
        var _this = this;
        this.submitted = true;
        if (this.newPatientForm.valid) {
            this.patientService.create(this.patient).then(function (result) {
                _this.translate.get('admin_panel.patients.form.added').subscribe(function (res) {
                    _this.toastr.success(res);
                });
                _this.router.navigateByUrl('/patients/' + result.detailedPatient.id);
            }, function (err) {
                var error = JSON.parse(err._body).messages.patient.join(', ');
                _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                    _this.toastr.error(error, res);
                });
            });
        }
        else {
            console.log('invalid');
        }
    };
    PatientsNewComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./patients_new.component.html",
            providers: [doctor_service_1.DoctorService, patient_service_1.PatientService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], PatientsNewComponent);
    return PatientsNewComponent;
})();
exports.PatientsNewComponent = PatientsNewComponent;
//# sourceMappingURL=patients_new.component.js.map