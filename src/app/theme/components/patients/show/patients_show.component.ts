import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { PatientService } from '../../../../_services/patient.service';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { changedAuditValue, newAuditValue } from '../../../../_utils/audit_helper';


@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./patients_show.component.html",
  providers: [PatientService],
  encapsulation: ViewEncapsulation.None,
})
export class PatientsShowComponent implements OnInit {

  id: number;
  patient: any;
  limit=10;
  pagination:any;
  audits:any;
  changeLimit:any;


  constructor(
    private route: ActivatedRoute,
    public patientService: PatientService,
    private toastr: ToastrService,
    private router: Router,
    private translate: TranslateService
  )  {

  }


  ngOnInit()  {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.patientService.show(this.id).then(response => {
        this.patient = response;
        console.log('patient', this.patient)

      })
    })
    this.loadRecords(1);
  }

  changedParameter(change) {
    return changedAuditValue(change)
  }

  newParameter(change) {
    return newAuditValue(change)
  }

  resendConfirmationEmail() {
    this.patientService.resendConfirmationEmail(this.id).then((result) => {
      console.log(result)
      this.translate.get('admin_panel.doctors.confirmation_email_sended_success').subscribe((res: string) => {
        this.toastr.success(res);
      });
      this.router.navigateByUrl('/patients/'+ this.id);
    }, (err) => {
      this.translate.get('admin_panel.common.error').subscribe((res: string) => {
        this.toastr.error(res);
      });
    });
  }


  loadRecords(page=this.pagination.offset) {
    this.patientService.getAudits({id: this.id, limit: this.limit, offset: page}).then(response => {
      console.log(response)
      this.audits = response.audits;
      this.pagination = response.pagination;
    })
  }

}
