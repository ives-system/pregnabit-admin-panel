var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var patient_service_1 = require('../../../../_services/patient.service');
var PatientsShowComponent = (function () {
    function PatientsShowComponent(route, patientService, toastr, router, translate) {
        this.route = route;
        this.patientService = patientService;
        this.toastr = toastr;
        this.router = router;
        this.translate = translate;
        this.limit = 10;
    }
    PatientsShowComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = +params['id'];
            _this.patientService.show(_this.id).then(function (response) {
                _this.patient = response.detailedPatient;
            });
        });
        this.loadRecords(1);
    };
    PatientsShowComponent.prototype.resendConfirmationEmail = function () {
        var _this = this;
        this.patientService.resendConfirmationEmail(this.id).then(function (result) {
            console.log(result);
            _this.translate.get('admin_panel.doctors.confirmation_email_sended_success').subscribe(function (res) {
                _this.toastr.success(res);
            });
            _this.router.navigateByUrl('/patients/' + _this.id);
        }, function (err) {
            _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                _this.toastr.error(res);
            });
        });
    };
    PatientsShowComponent.prototype.loadRecords = function (page) {
        var _this = this;
        if (page === void 0) { page = this.pagination.offset; }
        this.patientService.getAudits({ id: this.id, limit: this.limit, offset: page }).then(function (response) {
            _this.audits = response.audits;
            _this.pagination = response.pagination;
        });
    };
    PatientsShowComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./patients_show.component.html",
            providers: [patient_service_1.PatientService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], PatientsShowComponent);
    return PatientsShowComponent;
})();
exports.PatientsShowComponent = PatientsShowComponent;
//# sourceMappingURL=patients_show.component.js.map