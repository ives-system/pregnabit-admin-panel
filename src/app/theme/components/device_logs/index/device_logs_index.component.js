var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var device_log_service_1 = require('../../../../_services/device_log.service');
var DeviceLogsIndexComponent = (function () {
    function DeviceLogsIndexComponent(deviceLogService, translate) {
        var _this = this;
        this.deviceLogService = deviceLogService;
        this.translate = translate;
        this.limit = 10;
        this.query = '';
        this.filters = { status: '' };
        this.states = {};
        translate.onLangChange.subscribe(function (lang) {
            _this.loadTranslates();
        });
    }
    DeviceLogsIndexComponent.prototype.ngOnInit = function () {
        this.loadRecords(1);
        this.loadTranslates();
    };
    DeviceLogsIndexComponent.prototype.loadRecords = function (page) {
        var _this = this;
        if (page === void 0) { page = this.pagination.offset; }
        this.deviceLogService.index({
            offset: page, limit: this.limit, query: this.query, sortModel: this.sortModel,
            sortOrder: this.sortOrder, status: this.filters.status
        }).then(function (response) {
            _this.pagination = response.pagination;
            _this.deviceLogs = response.deviceLogs;
        });
    };
    DeviceLogsIndexComponent.prototype.sort = function (sortModel) {
        if (this.sortModel != sortModel) {
            this.sortOrder = 'asc';
        }
        else {
            if (this.sortOrder == 'asc') {
                this.sortOrder = 'desc';
            }
            else {
                this.sortOrder = 'asc';
            }
        }
        this.sortModel = sortModel;
        this.loadRecords();
    };
    DeviceLogsIndexComponent.prototype.changeLimit = function (limit) {
        this.limit = limit;
        this.loadRecords(1);
    };
    DeviceLogsIndexComponent.prototype.loadTranslates = function () {
        var _this = this;
        this.translate.get([
            'admin_panel.device_logs.states.in_progress',
            'admin_panel.device_logs.states.error',
            'admin_panel.device_logs.states.completed'])
            .subscribe(function (res) {
            _this.states = {
                'in_progress': res['admin_panel.device_logs.states.in_progress'],
                'error': res['admin_panel.device_logs.states.error'],
                'completed': res['admin_panel.device_logs.states.completed'] };
        });
    };
    DeviceLogsIndexComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./device_logs_index.component.html",
            providers: [device_log_service_1.DeviceLogService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], DeviceLogsIndexComponent);
    return DeviceLogsIndexComponent;
})();
exports.DeviceLogsIndexComponent = DeviceLogsIndexComponent;
//# sourceMappingURL=device_logs_index.component.js.map