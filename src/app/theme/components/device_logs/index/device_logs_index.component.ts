import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { DeviceLogService } from '../../../../_services/device_log.service';
import { TranslateService } from '@ngx-translate/core';



@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./device_logs_index.component.html",
  providers: [DeviceLogService],
  encapsulation: ViewEncapsulation.None,
})
export class DeviceLogsIndexComponent implements OnInit {

  limit=10;
  query='';
  pagination:any;
  deviceLogs:any;
  sortModel:any;
  sortOrder:any;
  filters = {status: ''};
  states = {};
  constructor(
    private deviceLogService: DeviceLogService,
    private translate: TranslateService
  )  {
    translate.onLangChange.subscribe((lang) => {
      this.loadTranslates();
    })
  }

  ngOnInit()  {
    this.loadRecords(1);
    this.loadTranslates()
  }


  loadRecords(page=this.pagination.offset) {
    this.deviceLogService.index({
      offset: page, limit: this.limit, query: this.query, sortModel: this.sortModel,
      sortOrder: this.sortOrder, status: this.filters.status
    }).then(response => {
      this.pagination = response.pagination;
      this.deviceLogs = response.deviceLogs;
    })
  }

  sort(sortModel) {
    if(this.sortModel != sortModel) {
      this.sortOrder = 'asc';
    } else {
      if(this.sortOrder == 'asc') {
        this.sortOrder = 'desc';
      } else {
        this.sortOrder = 'asc';
      }
    }
    this.sortModel = sortModel;
    this.loadRecords();
  }

  changeLimit(limit) {
    this.limit = limit;
    this.loadRecords(1);
  }

  loadTranslates() {
    this.translate.get([
      'admin_panel.device_logs.states.in_progress',
      'admin_panel.device_logs.states.error',
      'admin_panel.device_logs.states.completed'])
      .subscribe((res: string) => {
      this.states = {
        'in_progress': res['admin_panel.device_logs.states.in_progress'],
        'error': res['admin_panel.device_logs.states.error'],
        'completed': res['admin_panel.device_logs.states.completed']}
      });
  }




}
