var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var merchant_user_1 = require('../../../../_models/merchant_user');
var merchant_user_service_1 = require('../../../../_services/merchant_user.service');
var password_validator_1 = require('../../../../_validators/password_validator');
var MerchantUsersNewComponent = (function () {
    function MerchantUsersNewComponent(router, toastr, merchantUserService, formBuilder, translate) {
        this.router = router;
        this.toastr = toastr;
        this.merchantUserService = merchantUserService;
        this.formBuilder = formBuilder;
        this.translate = translate;
        this.submitted = false;
        this.merchantUser = new merchant_user_1.MerchantUser();
        this.newMerchantUserForm = formBuilder.group({
            firstName: ['', forms_1.Validators.required],
            lastName: ['', forms_1.Validators.required],
            email: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.email])],
            password: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(8)])],
            passwordConfirmation: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(8)])]
        }, {
            validator: password_validator_1.PasswordValidation.MatchPassword
        });
    }
    MerchantUsersNewComponent.prototype.createMerchantUser = function () {
        var _this = this;
        this.submitted = true;
        if (this.newMerchantUserForm.valid) {
            this.merchantUserService.create(this.merchantUser).then(function (result) {
                _this.translate.get('admin_panel.merchant_users.form.added').subscribe(function (res) {
                    _this.toastr.success(res);
                });
                _this.router.navigateByUrl('/merchant_users/' + result.detailedMerchantUser.id);
            }, function (err) {
                var error = JSON.parse(err._body).messages.merchant_user.join(', ');
                _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                    _this.toastr.error(error, res);
                });
            });
        }
        else {
            console.log('invalid');
        }
    };
    MerchantUsersNewComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./merchant_users_new.component.html",
            providers: [merchant_user_service_1.MerchantUserService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], MerchantUsersNewComponent);
    return MerchantUsersNewComponent;
})();
exports.MerchantUsersNewComponent = MerchantUsersNewComponent;
//# sourceMappingURL=merchant_users_new.component.js.map