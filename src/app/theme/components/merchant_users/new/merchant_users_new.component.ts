import { Component, ViewEncapsulation } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MerchantUser } from '../../../../_models/merchant_user';
import { MerchantUserService } from '../../../../_services/merchant_user.service';
import { PasswordValidation } from '../../../../_validators/password_validator';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./merchant_users_new.component.html",
  providers: [MerchantUserService],
  encapsulation: ViewEncapsulation.None,
})
export class MerchantUsersNewComponent {
  merchantUser: MerchantUser;
  newMerchantUserForm: FormGroup;
  submitted = false;

  constructor(
    private router: Router,
    private toastr: ToastrService,
    public merchantUserService: MerchantUserService,
    public formBuilder: FormBuilder,
    private translate: TranslateService

  ){
    this.merchantUser = new MerchantUser();
    this.newMerchantUserForm = formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.compose([Validators.required,Validators.email])],
      password: ['', Validators.compose([Validators.required,Validators.minLength(8), Validators.pattern(/(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[\W]).{8}/)])],
      passwordConfirmation: ['', Validators.compose([Validators.required,Validators.minLength(8), Validators.pattern(/(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[\W]).{8}/)])],
    }, {
      validator: PasswordValidation.MatchPassword
    });
  }


  createMerchantUser() {
    this.submitted = true;
    if(this.newMerchantUserForm.valid) {
      this.merchantUserService.create(this.merchantUser).then((result) => {
        this.translate.get('admin_panel.merchant_users.form.added').subscribe((res: string) => {
          this.toastr.success(res);
        });
        this.router.navigateByUrl('/merchant_users/'+ result.id);
      }, (err) => {
        var error = JSON.parse(err._body).messages.merchant_user.join(', ');
        this.translate.get('admin_panel.common.error').subscribe((res: string) => {
          this.toastr.error(error, res);
        });
      });
    } else {
      console.log('invalid');
    }
  }


}
