import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { MerchantUsersIndexComponent } from './index/merchant_users_index.component';
import { MerchantUsersNewComponent } from './new/merchant_users_new.component';
import { MerchantUsersShowComponent } from './show/merchant_users_show.component';
import { MerchantUsersEditComponent } from './edit/merchant_users_edit.component';
import { MerchantUsersEditPasswordComponent } from './edit_password/merchant_users_edit_password.component';
import { LayoutModule } from '../../layouts/layout.module';
import { DefaultComponent } from '../../pages/default/default.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';


const routes: Routes = [
  {
    "path": "",
    "component": DefaultComponent,
    "children": [
      {
        "path": "",
        "component": MerchantUsersIndexComponent
      },
      {
        "path": "new",
        "component": MerchantUsersNewComponent
      },
      {
        "path": ":id",
        "component": MerchantUsersShowComponent
      },
      {
        "path": "edit\/:id",
        "component": MerchantUsersEditComponent
      },
      {
        "path": "edit_password\/:id",
        "component": MerchantUsersEditPasswordComponent
      }
    ]
  }
];


@NgModule({imports: [
  CommonModule,
  RouterModule.forChild(routes),
  LayoutModule,
  FormsModule,
  ReactiveFormsModule,
  ToastrModule.forRoot({positionClass: 'toast-bottom-right'}),
  NgbModule,
  TranslateModule
],exports: [
  RouterModule
],declarations: [
  MerchantUsersIndexComponent,
  MerchantUsersNewComponent,
  MerchantUsersShowComponent,
  MerchantUsersEditComponent,
  MerchantUsersEditPasswordComponent
]})


export class MerchantUsersModule  {

}
