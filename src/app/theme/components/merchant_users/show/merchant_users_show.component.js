var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var merchant_user_service_1 = require('../../../../_services/merchant_user.service');
var MerchantUsersShowComponent = (function () {
    function MerchantUsersShowComponent(route, merchantUserService) {
        this.route = route;
        this.merchantUserService = merchantUserService;
    }
    MerchantUsersShowComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = +params['id'];
            _this.merchantUserService.show(_this.id).then(function (response) {
                _this.merchantUser = response.detailedMerchantUser;
                console.log(response);
            });
        });
    };
    MerchantUsersShowComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./merchant_users_show.component.html",
            providers: [merchant_user_service_1.MerchantUserService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], MerchantUsersShowComponent);
    return MerchantUsersShowComponent;
})();
exports.MerchantUsersShowComponent = MerchantUsersShowComponent;
//# sourceMappingURL=merchant_users_show.component.js.map