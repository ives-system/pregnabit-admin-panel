import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { MerchantUserService } from '../../../../_services/merchant_user.service';




@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./merchant_users_show.component.html",
  providers: [MerchantUserService],
  encapsulation: ViewEncapsulation.None,
})
export class MerchantUsersShowComponent implements OnInit {

  id: number;
  merchantUser: any;
  constructor(
    private route: ActivatedRoute,
    public merchantUserService: MerchantUserService
  )  {

  }


  ngOnInit()  {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.merchantUserService.show(this.id).then(response => {
        this.merchantUser = response;
        console.log(response);
      })
    })
  }

}
