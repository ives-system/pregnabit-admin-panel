var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var common_1 = require('@angular/common');
var router_1 = require('@angular/router');
var ngx_toastr_1 = require('ngx-toastr');
var merchant_users_index_component_1 = require('./index/merchant_users_index.component');
var merchant_users_new_component_1 = require('./new/merchant_users_new.component');
var merchant_users_show_component_1 = require('./show/merchant_users_show.component');
var merchant_users_edit_component_1 = require('./edit/merchant_users_edit.component');
var merchant_users_edit_password_component_1 = require('./edit_password/merchant_users_edit_password.component');
var layout_module_1 = require('../../layouts/layout.module');
var default_component_1 = require('../../pages/default/default.component');
var ng_bootstrap_1 = require('@ng-bootstrap/ng-bootstrap');
var core_2 = require('@ngx-translate/core');
var routes = [
    {
        "path": "",
        "component": default_component_1.DefaultComponent,
        "children": [
            {
                "path": "",
                "component": merchant_users_index_component_1.MerchantUsersIndexComponent
            },
            {
                "path": "new",
                "component": merchant_users_new_component_1.MerchantUsersNewComponent
            },
            {
                "path": ":id",
                "component": merchant_users_show_component_1.MerchantUsersShowComponent
            },
            {
                "path": "edit\/:id",
                "component": merchant_users_edit_component_1.MerchantUsersEditComponent
            },
            {
                "path": "edit_password\/:id",
                "component": merchant_users_edit_password_component_1.MerchantUsersEditPasswordComponent
            }
        ]
    }
];
var MerchantUsersModule = (function () {
    function MerchantUsersModule() {
    }
    MerchantUsersModule = __decorate([
        core_1.NgModule({ imports: [
                common_1.CommonModule,
                router_1.RouterModule.forChild(routes),
                layout_module_1.LayoutModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                ngx_toastr_1.ToastrModule.forRoot({ positionClass: 'toast-bottom-right' }),
                ng_bootstrap_1.NgbModule,
                core_2.TranslateModule
            ], exports: [
                router_1.RouterModule
            ], declarations: [
                merchant_users_index_component_1.MerchantUsersIndexComponent,
                merchant_users_new_component_1.MerchantUsersNewComponent,
                merchant_users_show_component_1.MerchantUsersShowComponent,
                merchant_users_edit_component_1.MerchantUsersEditComponent,
                merchant_users_edit_password_component_1.MerchantUsersEditPasswordComponent
            ] })
    ], MerchantUsersModule);
    return MerchantUsersModule;
})();
exports.MerchantUsersModule = MerchantUsersModule;
//# sourceMappingURL=merchant_users.module.js.map