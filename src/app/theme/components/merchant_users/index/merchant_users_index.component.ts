import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { MerchantUserService } from '../../../../_services/merchant_user.service';


@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./merchant_users_index.component.html",
  providers: [MerchantUserService],
  encapsulation: ViewEncapsulation.None,
})
export class MerchantUsersIndexComponent implements OnInit {

  limit=10;
  query='';
  pagination:any;
  merchants:any;
  sortModel:any;
  sortOrder:any;
  constructor(
    private merchantUserService: MerchantUserService
  )  {}

  ngOnInit()  {
    this.loadRecords(1);
  }


  loadRecords(page=this.pagination.offset) {
    this.merchantUserService.index({offset: page, limit: this.limit, query: this.query, sortModel: this.sortModel, sortOrder: this.sortOrder}).then(response => {
      this.pagination = response.pagination;
      this.merchants = response.merchants;
      console.log(this.merchants)
    })
  }

  sort(sortModel) {
    if(this.sortModel != sortModel) {
      this.sortOrder = 'asc';
    } else {
      if(this.sortOrder == 'asc') {
        this.sortOrder = 'desc';
      } else {
        this.sortOrder = 'asc';
      }
    }
    this.sortModel = sortModel;
    this.loadRecords();
  }

  changeLimit(limit) {
    this.limit = limit;
    this.loadRecords(1);
  }



}
