import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MerchantUser } from '../../../../_models/merchant_user';
import { MerchantUserService } from '../../../../_services/merchant_user.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./merchant_users_edit.component.html",
  providers: [MerchantUserService],
  encapsulation: ViewEncapsulation.None,
})
export class MerchantUsersEditComponent implements OnInit {
  id: number;
  merchantUserName: any;
  merchantUser: MerchantUser;
  editMerchantUserForm: FormGroup;
  submitted = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    public merchantUserService: MerchantUserService,
    public formBuilder: FormBuilder,
    private translate: TranslateService
  ){
    this.merchantUser = new MerchantUser();
    this.editMerchantUserForm = formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.compose([Validators.required,Validators.email])]
    })

  }


  ngOnInit()  {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.merchantUserService.show(this.id).then(response => {
        this.merchantUser = response;
        this.merchantUserName = this.merchantUser['name'];
      })
    })
  }

  updateMerchantUser() {
    this.submitted = true;
    if(this.editMerchantUserForm.valid) {
      this.merchantUserService.update(this.merchantUser).then((result) => {
        this.translate.get('admin_panel.merchant_users.form.updated').subscribe((res: string) => {
          this.toastr.success(res);
        });
        this.router.navigateByUrl('/merchant_users/'+this.id);
      }, (err) => {
        var error = JSON.parse(err._body).messages.merchant_user.join(', ');
        this.translate.get('admin_panel.common.error').subscribe((res: string) => {
          this.toastr.error(error, res);
        });
      });
    } else {
      console.log('invalid');
    }
  }


}
