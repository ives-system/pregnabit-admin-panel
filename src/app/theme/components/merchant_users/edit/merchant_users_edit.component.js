var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var merchant_user_1 = require('../../../../_models/merchant_user');
var merchant_user_service_1 = require('../../../../_services/merchant_user.service');
var MerchantUsersEditComponent = (function () {
    function MerchantUsersEditComponent(route, router, toastr, merchantUserService, formBuilder, translate) {
        this.route = route;
        this.router = router;
        this.toastr = toastr;
        this.merchantUserService = merchantUserService;
        this.formBuilder = formBuilder;
        this.translate = translate;
        this.submitted = false;
        this.merchantUser = new merchant_user_1.MerchantUser();
        this.editMerchantUserForm = formBuilder.group({
            firstName: ['', forms_1.Validators.required],
            lastName: ['', forms_1.Validators.required],
            email: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.email])]
        });
    }
    MerchantUsersEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = +params['id'];
            _this.merchantUserService.show(_this.id).then(function (response) {
                _this.merchantUser = response.detailedMerchantUser;
                _this.merchantUserName = _this.merchantUser['name'];
            });
        });
    };
    MerchantUsersEditComponent.prototype.updateMerchantUser = function () {
        var _this = this;
        this.submitted = true;
        if (this.editMerchantUserForm.valid) {
            this.merchantUserService.update(this.merchantUser).then(function (result) {
                _this.translate.get('admin_panel.merchant_users.form.updated').subscribe(function (res) {
                    _this.toastr.success(res);
                });
                _this.router.navigateByUrl('/merchant_users/' + _this.id);
            }, function (err) {
                var error = JSON.parse(err._body).messages.merchant_user.join(', ');
                _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                    _this.toastr.error(error, res);
                });
            });
        }
        else {
            console.log('invalid');
        }
    };
    MerchantUsersEditComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./merchant_users_edit.component.html",
            providers: [merchant_user_service_1.MerchantUserService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], MerchantUsersEditComponent);
    return MerchantUsersEditComponent;
})();
exports.MerchantUsersEditComponent = MerchantUsersEditComponent;
//# sourceMappingURL=merchant_users_edit.component.js.map