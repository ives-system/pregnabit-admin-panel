import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { LayoutModule } from '../../layouts/layout.module';
import { DefaultComponent } from '../../pages/default/default.component';
import { TranslateModule } from '@ngx-translate/core';


const routes: Routes = [
  {
    "path": "",
    "component": DefaultComponent,
    "children": [
      {
        "path": "",
        "component": DashboardComponent
      }
    ]
  }
];
@NgModule({imports: [
  CommonModule,RouterModule.forChild(routes),LayoutModule, TranslateModule
],exports: [
  RouterModule
],declarations: [
  DashboardComponent
]})
export class DashboardModule  {



}
