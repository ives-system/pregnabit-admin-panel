var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var examination_service_1 = require('../../../_services/examination.service');
var c3 = require('c3');
var DashboardComponent = (function () {
    function DashboardComponent(examinationService, translate) {
        var _this = this;
        this.examinationService = examinationService;
        this.translate = translate;
        translate.onLangChange.subscribe(function (lang) {
            _this.loadChart();
        });
    }
    DashboardComponent.prototype.ngAfterViewInit = function () {
        this.loadChart();
    };
    DashboardComponent.prototype.loadChart = function () {
        var _this = this;
        this.examinationService.getStats().then(function (response) {
            var x = response[0];
            var y = response[1];
            _this.translate.get('admin_panel.dashboard.number_of_examinations').subscribe(function (res) {
                y[0] = res;
            });
            _this.chartData = [x, y];
            var chart = c3.generate({
                bindto: '#chart',
                data: {
                    x: 'x',
                    columns: _this.chartData,
                    type: 'spline'
                },
                axis: {
                    x: {
                        type: 'category',
                        tick: {
                            rotate: 75,
                            multiline: false
                        }
                    },
                    y: {
                        min: 0,
                        padding: { bottom: 0 }
                    }
                },
                legend: {
                    show: false
                },
                color: {
                    pattern: ['#9f6dc5']
                }
            });
        });
    };
    DashboardComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./dashboard.component.html",
            providers: [examination_service_1.ExaminationService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], DashboardComponent);
    return DashboardComponent;
})();
exports.DashboardComponent = DashboardComponent;
//# sourceMappingURL=dashboard.component.js.map