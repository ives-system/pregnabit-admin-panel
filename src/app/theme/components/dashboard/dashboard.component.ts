import { Component, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { Helpers } from '../../../helpers';
import { ExaminationService } from '../../../_services/examination.service';
import { TranslateService } from '@ngx-translate/core';
import * as c3 from 'c3';

@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./dashboard.component.html",
  providers: [ExaminationService],
  encapsulation: ViewEncapsulation.None,
})
export class DashboardComponent implements AfterViewInit {


  chartData:any;

  constructor(
    private examinationService: ExaminationService,
    private translate: TranslateService
  )  {
    translate.onLangChange.subscribe((lang) => {
      this.loadChart();
    })
  }

  ngAfterViewInit() {
    this.loadChart()
  }

  loadChart() {
    this.examinationService.getStats().then(response => {
      let x = response[0];
      let y = response[1];
      this.translate.get('admin_panel.dashboard.number_of_examinations').subscribe((res: string) => {
        y[0] = res;
      });
      this.chartData = [x, y];
      let chart = c3.generate({
        bindto: '#chart',
        data: {
          x: 'x',
          columns: this.chartData,
          type: 'spline'
        },
        axis: {
          x: {
            type: 'category',
            tick: {
              rotate: 75,
              multiline: false
            },
          },
          y: {
            min: 0,
            padding: {bottom: 0}
            //tick: {
            //  format: function (d) {
            //    return (parseInt(d) == d) ? d : null;
            //  }
            //}
          }
        },
        legend: {
          show: false
        },
        color: {
          pattern: ['#9f6dc5']
        }
      });
    });
  }

}
