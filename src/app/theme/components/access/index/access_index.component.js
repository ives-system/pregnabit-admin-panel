var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var access_service_1 = require('../../../../_services/access.service');
var AccessIndexComponent = (function () {
    function AccessIndexComponent(accessService, toastr, translate) {
        this.accessService = accessService;
        this.toastr = toastr;
        this.translate = translate;
        this.access = {
            arePatientsBlocked: false,
            areDoctorsBlocked: false,
            areCallCentersBlocked: false
        };
    }
    AccessIndexComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.accessService.index().then(function (response) {
            console.log(response);
            _this.access = {
                arePatientsBlocked: response.access.are_patients_blocked,
                areDoctorsBlocked: response.access.are_doctors_blocked,
                areCallCentersBlocked: response.access.are_call_centers_blocked
            };
        });
    };
    AccessIndexComponent.prototype.lockPortalForPatients = function () {
        var _this = this;
        this.accessService.lockForPatients().then(function (result) {
            _this.access.arePatientsBlocked = result.are_patients_blocked;
            _this.translate.get('admin_panel.access.successfully_locked_for_patients').subscribe(function (res) {
                _this.toastr.success(res);
            });
        }, function (err) {
            _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                _this.toastr.error(res);
            });
        });
    };
    AccessIndexComponent.prototype.unlockPortalForPatients = function () {
        var _this = this;
        this.accessService.unlockForPatients().then(function (result) {
            _this.access.arePatientsBlocked = result.are_patients_blocked;
            _this.translate.get('admin_panel.access.successfully_unlocked_for_patients').subscribe(function (res) {
                _this.toastr.success(res);
            });
        }, function (err) {
            _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                _this.toastr.error(res);
            });
        });
    };
    AccessIndexComponent.prototype.lockPortalForDoctors = function () {
        var _this = this;
        this.accessService.lockForDoctors().then(function (result) {
            _this.access.areDoctorsBlocked = result.are_doctors_blocked;
            _this.translate.get('admin_panel.access.successfully_locked_for_doctors').subscribe(function (res) {
                _this.toastr.success(res);
            });
        }, function (err) {
            _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                _this.toastr.error(res);
            });
        });
    };
    AccessIndexComponent.prototype.unlockPortalForDoctors = function () {
        var _this = this;
        this.accessService.unlockForDoctors().then(function (result) {
            _this.access.areDoctorsBlocked = result.are_doctors_blocked;
            _this.translate.get('admin_panel.access.successfully_unlocked_for_doctors').subscribe(function (res) {
                _this.toastr.success(res);
            });
        }, function (err) {
            _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                _this.toastr.error(res);
            });
        });
    };
    AccessIndexComponent.prototype.lockPortalForCallCenters = function () {
        var _this = this;
        this.accessService.lockForCallCenters().then(function (result) {
            _this.access.areCallCentersBlocked = result.are_call_centers_blocked;
            _this.translate.get('admin_panel.access.successfully_locked_for_call_centers').subscribe(function (res) {
                _this.toastr.success(res);
            });
        }, function (err) {
            _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                _this.toastr.error(res);
            });
        });
    };
    AccessIndexComponent.prototype.unlockPortalForCallCenters = function () {
        var _this = this;
        this.accessService.unlockForCallCenters().then(function (result) {
            _this.access.areCallCentersBlocked = result.are_call_centers_blocked;
            _this.translate.get('admin_panel.access.successfully_unlocked_for_call_centers').subscribe(function (res) {
                _this.toastr.success(res);
            });
        }, function (err) {
            _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                _this.toastr.error(res);
            });
        });
    };
    AccessIndexComponent.prototype.showConfirmationForPatients = function (e) {
        var _this = this;
        if (this.access.arePatientsBlocked) {
            this.translate.get('admin_panel.access.confirmation_to_unlock_portal_for_patients').subscribe(function (res) {
                if (window.confirm(res)) {
                    _this.unlockPortalForPatients();
                }
                else {
                    e.preventDefault();
                }
            });
        }
        else {
            this.translate.get('admin_panel.access.confirmation_to_lock_portal_for_patients').subscribe(function (res) {
                if (window.confirm(res)) {
                    _this.lockPortalForPatients();
                }
                else {
                    e.preventDefault();
                }
            });
        }
    };
    AccessIndexComponent.prototype.showConfirmationForDoctors = function (e) {
        var _this = this;
        if (this.access.areDoctorsBlocked) {
            this.translate.get('admin_panel.access.confirmation_to_unlock_portal_for_doctors').subscribe(function (res) {
                if (window.confirm(res)) {
                    _this.unlockPortalForDoctors();
                }
                else {
                    e.preventDefault();
                }
            });
        }
        else {
            this.translate.get('admin_panel.access.confirmation_to_lock_portal_for_doctors').subscribe(function (res) {
                if (window.confirm(res)) {
                    _this.lockPortalForDoctors();
                }
                else {
                    e.preventDefault();
                }
            });
        }
    };
    AccessIndexComponent.prototype.showConfirmationForCallCenters = function (e) {
        var _this = this;
        if (this.access.areCallCentersBlocked) {
            this.translate.get('admin_panel.access.confirmation_to_unlock_portal_for_call_centers').subscribe(function (res) {
                if (window.confirm(res)) {
                    _this.unlockPortalForCallCenters();
                }
                else {
                    e.preventDefault();
                }
            });
        }
        else {
            this.translate.get('admin_panel.access.confirmation_to_lock_portal_for_call_centers').subscribe(function (res) {
                if (window.confirm(res)) {
                    _this.lockPortalForCallCenters();
                }
                else {
                    e.preventDefault();
                }
            });
        }
    };
    AccessIndexComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./access_index.component.html",
            providers: [access_service_1.AccessService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], AccessIndexComponent);
    return AccessIndexComponent;
})();
exports.AccessIndexComponent = AccessIndexComponent;
//# sourceMappingURL=access_index.component.js.map