import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { AccessService } from '../../../../_services/access.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';



@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./access_index.component.html",
  providers: [AccessService],
  encapsulation: ViewEncapsulation.None,
})
export class AccessIndexComponent implements OnInit {

  access = {
    arePatientsBlocked:false,
    areDoctorsBlocked:false,
    areCallCentersBlocked:false
  };


  constructor(
    private accessService: AccessService,
    public toastr: ToastrService,
    private translate: TranslateService
  )  {}

  ngOnInit()  {
    this.accessService.index().then(response => {
      console.log(response);
      this.access = {
        arePatientsBlocked: response.access.are_patients_blocked,
        areDoctorsBlocked: response.access.are_doctors_blocked,
        areCallCentersBlocked: response.access.are_call_centers_blocked
      }
    })
  }


  lockPortalForPatients() {
    this.accessService.lockForPatients().then((result) => {
      this.access.arePatientsBlocked = result.are_patients_blocked;
      this.translate.get('admin_panel.access.successfully_locked_for_patients').subscribe((res: string) => {
        this.toastr.success(res);
      });
    }, (err) => {
      this.translate.get('admin_panel.common.error').subscribe((res: string) => {
        this.toastr.error(res);
      });
    });
  }

  unlockPortalForPatients() {
    this.accessService.unlockForPatients().then((result) => {
      this.access.arePatientsBlocked = result.are_patients_blocked;
      this.translate.get('admin_panel.access.successfully_unlocked_for_patients').subscribe((res: string) => {
        this.toastr.success(res);
      });
    }, (err) => {
      this.translate.get('admin_panel.common.error').subscribe((res: string) => {
        this.toastr.error(res);
      });
    });
  }

  lockPortalForDoctors() {
    this.accessService.lockForDoctors().then((result) => {
      this.access.areDoctorsBlocked = result.are_doctors_blocked;
      this.translate.get('admin_panel.access.successfully_locked_for_doctors').subscribe((res: string) => {
        this.toastr.success(res);
      });
    }, (err) => {
      this.translate.get('admin_panel.common.error').subscribe((res: string) => {
        this.toastr.error(res);
      });
    });
  }

  unlockPortalForDoctors() {
    this.accessService.unlockForDoctors().then((result) => {
      this.access.areDoctorsBlocked = result.are_doctors_blocked;
      this.translate.get('admin_panel.access.successfully_unlocked_for_doctors').subscribe((res: string) => {
        this.toastr.success(res);
      });
    }, (err) => {
      this.translate.get('admin_panel.common.error').subscribe((res: string) => {
        this.toastr.error(res);
      });
    });
  }

  lockPortalForCallCenters() {
    this.accessService.lockForCallCenters().then((result) => {
      this.access.areCallCentersBlocked = result.are_call_centers_blocked;
      this.translate.get('admin_panel.access.successfully_locked_for_call_centers').subscribe((res: string) => {
        this.toastr.success(res);
      });
    }, (err) => {
      this.translate.get('admin_panel.common.error').subscribe((res: string) => {
        this.toastr.error(res);
      });
    });
  }

  unlockPortalForCallCenters() {
    this.accessService.unlockForCallCenters().then((result) => {
      this.access.areCallCentersBlocked = result.are_call_centers_blocked;
      this.translate.get('admin_panel.access.successfully_unlocked_for_call_centers').subscribe((res: string) => {
        this.toastr.success(res);
      });
    }, (err) => {
      this.translate.get('admin_panel.common.error').subscribe((res: string) => {
        this.toastr.error(res);
      });
    });
  }


  showConfirmationForPatients(e: any) {
    if (this.access.arePatientsBlocked) {
      this.translate.get('admin_panel.access.confirmation_to_unlock_portal_for_patients').subscribe((res: string) => {
        if (window.confirm(res)) {
          this.unlockPortalForPatients();
        } else {
          e.preventDefault();
        }
      })
    } else {
      this.translate.get('admin_panel.access.confirmation_to_lock_portal_for_patients').subscribe((res: string) => {
        if (window.confirm(res)) {
          this.lockPortalForPatients();
        } else {
          e.preventDefault();
        }
      })
    }
  }

  showConfirmationForDoctors(e: any) {
    if (this.access.areDoctorsBlocked) {
      this.translate.get('admin_panel.access.confirmation_to_unlock_portal_for_doctors').subscribe((res: string) => {
        if (window.confirm(res)) {
          this.unlockPortalForDoctors();
        } else {
          e.preventDefault();
        }
      })
    } else {
      this.translate.get('admin_panel.access.confirmation_to_lock_portal_for_doctors').subscribe((res: string) => {
        if (window.confirm(res)) {
          this.lockPortalForDoctors();
        } else {
          e.preventDefault();
        }
      })
    }
  }

  showConfirmationForCallCenters(e: any) {
    if (this.access.areCallCentersBlocked) {
      this.translate.get('admin_panel.access.confirmation_to_unlock_portal_for_call_centers').subscribe((res: string) => {
        if (window.confirm(res)) {
          this.unlockPortalForCallCenters();
        } else {
          e.preventDefault();
        }
      })
    } else {
      this.translate.get('admin_panel.access.confirmation_to_lock_portal_for_call_centers').subscribe((res: string) => {
        if (window.confirm(res)) {
          this.lockPortalForCallCenters();
        } else {
          e.preventDefault();
        }
      })
    }
  }

}
