import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { DocumentationPreviewService } from '../../../../_services/documentation_preview.service';



@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./documentation_previews_index.component.html",
  providers: [DocumentationPreviewService],
  encapsulation: ViewEncapsulation.None,
})
export class DocumentationPreviewsIndexComponent implements OnInit {

  limit=10;
  query='';
  pagination:any;
  documentationPreviews:any;
  sortModel:any;
  sortOrder:any;
  constructor(
    private documentationPreviewService: DocumentationPreviewService
  )  {}

  ngOnInit()  {
    this.loadRecords(1);
  }


  loadRecords(page=this.pagination.offset) {
    this.documentationPreviewService.index({offset: page, limit: this.limit, query: this.query, sortModel: this.sortModel, sortOrder: this.sortOrder}).then(response => {
      this.pagination = response.pagination;
      this.documentationPreviews = response.documentationPreviews;
    })
  }

  sort(sortModel) {
    if(this.sortModel != sortModel) {
      this.sortOrder = 'asc';
    } else {
      if(this.sortOrder == 'asc') {
        this.sortOrder = 'desc';
      } else {
        this.sortOrder = 'asc';
      }
    }
    this.sortModel = sortModel;
    this.loadRecords();
  }

  changeLimit(limit) {
    this.limit = limit;
    this.loadRecords(1);
  }


}
