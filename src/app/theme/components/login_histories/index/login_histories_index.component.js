var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var login_history_service_1 = require('../../../../_services/login_history.service');
var LoginHistoriesIndexComponent = (function () {
    function LoginHistoriesIndexComponent(loginHistoryService) {
        this.loginHistoryService = loginHistoryService;
        this.limit = 10;
        this.query = '';
    }
    LoginHistoriesIndexComponent.prototype.ngOnInit = function () {
        this.loadRecords(1);
    };
    LoginHistoriesIndexComponent.prototype.loadRecords = function (page) {
        var _this = this;
        if (page === void 0) { page = this.pagination.offset; }
        this.loginHistoryService.index({ offset: page, limit: this.limit, query: this.query, sortModel: this.sortModel, sortOrder: this.sortOrder }).then(function (response) {
            _this.pagination = response.pagination;
            _this.loginHistories = response.loginHistories;
        });
    };
    LoginHistoriesIndexComponent.prototype.sort = function (sortModel) {
        if (this.sortModel != sortModel) {
            this.sortOrder = 'asc';
        }
        else {
            if (this.sortOrder == 'asc') {
                this.sortOrder = 'desc';
            }
            else {
                this.sortOrder = 'asc';
            }
        }
        this.sortModel = sortModel;
        this.loadRecords();
    };
    LoginHistoriesIndexComponent.prototype.changeLimit = function (limit) {
        this.limit = limit;
        this.loadRecords(1);
    };
    LoginHistoriesIndexComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./login_histories_index.component.html",
            providers: [login_history_service_1.LoginHistoryService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], LoginHistoriesIndexComponent);
    return LoginHistoriesIndexComponent;
})();
exports.LoginHistoriesIndexComponent = LoginHistoriesIndexComponent;
//# sourceMappingURL=login_histories_index.component.js.map