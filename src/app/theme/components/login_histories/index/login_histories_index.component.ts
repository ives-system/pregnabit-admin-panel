import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { LoginHistoryService } from '../../../../_services/login_history.service';



@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./login_histories_index.component.html",
  providers: [LoginHistoryService],
  encapsulation: ViewEncapsulation.None,
})
export class LoginHistoriesIndexComponent implements OnInit {

  limit=10;
  query='';
  pagination:any;
  loginHistories:any;
  sortModel:any;
  sortOrder:any;
  constructor(
    private loginHistoryService: LoginHistoryService
  )  {}

  ngOnInit()  {
    this.loadRecords(1);
  }


  loadRecords(page=this.pagination.offset) {
    this.loginHistoryService.index({offset: page, limit: this.limit, query: this.query, sortModel: this.sortModel, sortOrder: this.sortOrder}).then(response => {
      this.pagination = response.pagination;
      this.loginHistories = response.loginHistories;
    })
  }

  userProfileTypeLink (user) {
    if (user.userProfileType === 'superadmin') {
      return 'admin_user'
    } else {
      return user.userProfileType
    }
  }

  sort(sortModel) {
    if(this.sortModel != sortModel) {
      this.sortOrder = 'asc';
    } else {
      if(this.sortOrder == 'asc') {
        this.sortOrder = 'desc';
      } else {
        this.sortOrder = 'asc';
      }
    }
    this.sortModel = sortModel;
    this.loadRecords();
  }

  changeLimit(limit) {
    this.limit = limit;
    this.loadRecords(1);
  }


}
