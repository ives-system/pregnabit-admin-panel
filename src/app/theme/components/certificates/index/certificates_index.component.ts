import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Helpers } from '../../../../helpers';
import { CertificateService } from '../../../../_services/certificate.service';
import { DeviceService } from '../../../../_services/device.service';
import { TranslateService } from '@ngx-translate/core';




@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./certificates_index.component.html",
  providers: [CertificateService, DeviceService],
  encapsulation: ViewEncapsulation.None,
})
export class CertificatesIndexComponent implements OnInit {

  certificates:any;
  limit=10;
  pagination:any;
  sortModel:any;
  sortOrder:any;
  devices:any;
  filters = {deviceId: '', status: '', subject: '', serial: '',
    createdAtStart: '', createdAtEnd: '', revokedAtStart: '', revokedAtEnd: '', validToStart: '', validToEnd: ''};

  constructor(
    private certificateService: CertificateService,
    private deviceService: DeviceService,
    private toastr: ToastrService,
    private translate: TranslateService
  )  {}

  ngOnInit()  {
    this.sortModel = 'createdAt';
    this.sortOrder = 'desc';
    this.loadRecords(1);
    this.deviceService.list().then(response => {
      this.devices = response;
    });
  }


  loadRecords(page=this.pagination.offset) {
    this.certificateService.index({
      offset: page, limit: this.limit, sortModel: this.sortModel, sortOrder: this.sortOrder, deviceId: this.filters.deviceId,
      status: this.filters.status, subject: this.filters.subject, serial: this.filters.serial,
      createdAtStart: this.filters.createdAtStart, createdAtEnd: this.filters.createdAtEnd, revokedAtStart: this.filters.revokedAtStart,
      revokedAtEnd: this.filters.revokedAtEnd, validToStart: this.filters.validToStart, validToEnd: this.filters.validToEnd
    }).then(response => {
      this.pagination = response.pagination;
      this.certificates = response.certificates;
    })
  }

  sort(sortModel) {
    if(this.sortModel != sortModel) {
      this.sortOrder = 'asc';
    } else {
      if(this.sortOrder == 'asc') {
        this.sortOrder = 'desc';
      } else {
        this.sortOrder = 'asc';
      }
    }
    this.sortModel = sortModel;
    this.loadRecords();
  }

  changeLimit(limit) {
    this.limit = limit;
    this.loadRecords(1);
  }


  activate(id) {
    this.certificateService.activate(id).then(response => {
      this.loadRecords();
      this.translate.get('admin_panel.certificates.successfully_activated').subscribe((res: string) => {
        this.toastr.success(res);
      });
    }, (err) => {
      this.translate.get('admin_panel.common.error').subscribe((res: string) => {
        this.toastr.error(res);
      });
    })
  }

  deactivate(id) {
    this.certificateService.deactivate(id).then(response => {
      this.loadRecords();
      this.translate.get('admin_panel.certificates.successfully_deactivated').subscribe((res: string) => {
        this.toastr.success(res);
      });
    }, (err) => {
      this.translate.get('admin_panel.common.error').subscribe((res: string) => {
        this.toastr.error(res);
      });
    })
  }



}
