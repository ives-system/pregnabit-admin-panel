var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var certificate_service_1 = require('../../../../_services/certificate.service');
var device_service_1 = require('../../../../_services/device.service');
var CertificatesIndexComponent = (function () {
    function CertificatesIndexComponent(certificateService, deviceService, toastr, translate) {
        this.certificateService = certificateService;
        this.deviceService = deviceService;
        this.toastr = toastr;
        this.translate = translate;
        this.limit = 10;
        this.filters = { deviceId: '', status: '', subject: '', serial: '',
            createdAtStart: '', createdAtEnd: '', revokedAtStart: '', revokedAtEnd: '', validToStart: '', validToEnd: '' };
    }
    CertificatesIndexComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadRecords(1);
        this.deviceService.list().then(function (response) {
            _this.devices = response;
        });
    };
    CertificatesIndexComponent.prototype.loadRecords = function (page) {
        var _this = this;
        if (page === void 0) { page = this.pagination.offset; }
        console.log(this.filters);
        this.certificateService.index({
            offset: page, limit: this.limit, sortModel: this.sortModel, sortOrder: this.sortOrder, deviceId: this.filters.deviceId,
            status: this.filters.status, subject: this.filters.subject, serial: this.filters.serial,
            createdAtStart: this.filters.createdAtStart, createdAtEnd: this.filters.createdAtEnd, revokedAtStart: this.filters.revokedAtStart,
            revokedAtEnd: this.filters.revokedAtEnd, validToStart: this.filters.validToStart, validToEnd: this.filters.validToEnd
        }).then(function (response) {
            _this.pagination = response.pagination;
            _this.certificates = response.certificates;
        });
    };
    CertificatesIndexComponent.prototype.sort = function (sortModel) {
        if (this.sortModel != sortModel) {
            this.sortOrder = 'asc';
        }
        else {
            if (this.sortOrder == 'asc') {
                this.sortOrder = 'desc';
            }
            else {
                this.sortOrder = 'asc';
            }
        }
        this.sortModel = sortModel;
        this.loadRecords();
    };
    CertificatesIndexComponent.prototype.changeLimit = function (limit) {
        this.limit = limit;
        this.loadRecords(1);
    };
    CertificatesIndexComponent.prototype.activate = function (id) {
        var _this = this;
        this.certificateService.activate(id).then(function (response) {
            _this.loadRecords();
            _this.translate.get('admin_panel.certificates.successfully_activated').subscribe(function (res) {
                _this.toastr.success(res);
            });
        }, function (err) {
            _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                _this.toastr.error(res);
            });
        });
    };
    CertificatesIndexComponent.prototype.deactivate = function (id) {
        var _this = this;
        this.certificateService.deactivate(id).then(function (response) {
            _this.loadRecords();
            _this.translate.get('admin_panel.certificates.successfully_deactivated').subscribe(function (res) {
                _this.toastr.success(res);
            });
        }, function (err) {
            _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                _this.toastr.error(res);
            });
        });
    };
    CertificatesIndexComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./certificates_index.component.html",
            providers: [certificate_service_1.CertificateService, device_service_1.DeviceService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], CertificatesIndexComponent);
    return CertificatesIndexComponent;
})();
exports.CertificatesIndexComponent = CertificatesIndexComponent;
//# sourceMappingURL=certificates_index.component.js.map