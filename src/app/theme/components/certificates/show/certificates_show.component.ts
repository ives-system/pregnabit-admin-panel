import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { CertificateService } from '../../../../_services/certificate.service';
import {DeviceService} from '../../../../_services/device.service';
import {D} from '@angular/core/src/render3';




@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./certificates_show.component.html",
  providers: [CertificateService, DeviceService],
  encapsulation: ViewEncapsulation.None,
})
export class CertificatesShowComponent implements OnInit {

  id: number;
  certificate: any;
  devices: any;
  constructor(
    private route: ActivatedRoute,
    public certificateService: CertificateService,
    public deviceService: DeviceService
  )  {

  }


  ngOnInit()  {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.certificateService.show(this.id).then(response => {
        this.certificate = response;
      })
      this.deviceService.index({}).then(devices => {
        this.devices = devices
      })
    });
  }

}
