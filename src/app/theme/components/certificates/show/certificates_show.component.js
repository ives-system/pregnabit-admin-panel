var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var certificate_service_1 = require('../../../../_services/certificate.service');
var CertificatesShowComponent = (function () {
    function CertificatesShowComponent(route, certificateService) {
        this.route = route;
        this.certificateService = certificateService;
    }
    CertificatesShowComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = +params['id'];
            _this.certificateService.show(_this.id).then(function (response) {
                _this.certificate = response.detailedCertificate;
            });
        });
    };
    CertificatesShowComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./certificates_show.component.html",
            providers: [certificate_service_1.CertificateService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], CertificatesShowComponent);
    return CertificatesShowComponent;
})();
exports.CertificatesShowComponent = CertificatesShowComponent;
//# sourceMappingURL=certificates_show.component.js.map