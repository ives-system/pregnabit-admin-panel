var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var unauthorized_examination_service_1 = require('../../../../_services/unauthorized_examination.service');
var UnauthorizedExaminationsShowComponent = (function () {
    function UnauthorizedExaminationsShowComponent(route, unauthorizedExaminationService, toastr, router, translate) {
        this.route = route;
        this.unauthorizedExaminationService = unauthorizedExaminationService;
        this.toastr = toastr;
        this.router = router;
        this.translate = translate;
    }
    UnauthorizedExaminationsShowComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = +params['id'];
            _this.unauthorizedExaminationService.show(_this.id).then(function (response) {
                _this.examination = response.detailedUnauthorizedExamination;
            });
        });
    };
    UnauthorizedExaminationsShowComponent.prototype.authorize = function (id) {
        var _this = this;
        this.unauthorizedExaminationService.authorize(id).then(function (response) {
            if (response.authorized) {
                _this.translate.get('admin_panel.examinations.authorized').subscribe(function (res) {
                    _this.toastr.success(res);
                });
                _this.router.navigateByUrl('/examinations/' + id);
            }
            else {
                _this.toastr.error(response.notice);
            }
        });
    };
    UnauthorizedExaminationsShowComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./unauthorized_examinations_show.component.html",
            providers: [unauthorized_examination_service_1.UnauthorizedExaminationService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], UnauthorizedExaminationsShowComponent);
    return UnauthorizedExaminationsShowComponent;
})();
exports.UnauthorizedExaminationsShowComponent = UnauthorizedExaminationsShowComponent;
//# sourceMappingURL=unauthorized_examinations_show.component.js.map