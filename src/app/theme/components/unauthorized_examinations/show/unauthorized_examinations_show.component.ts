import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { UnauthorizedExaminationService } from '../../../../_services/unauthorized_examination.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./unauthorized_examinations_show.component.html",
  providers: [UnauthorizedExaminationService],
  encapsulation: ViewEncapsulation.None,
})
export class UnauthorizedExaminationsShowComponent implements OnInit {

  id:number;
  examination:any;

  constructor(private route:ActivatedRoute,
              public unauthorizedExaminationService: UnauthorizedExaminationService,
              private toastr: ToastrService,
              private router: Router,
              private translate: TranslateService)
  {}


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.unauthorizedExaminationService.show(this.id).then(response => {
        this.examination = response;
      });
    })
  }


  authorize(id) {
    this.unauthorizedExaminationService.authorize(id).then(response => {
      if(response.authorized) {
        this.translate.get('admin_panel.examinations.authorized').subscribe((res: string) => {
          this.toastr.success(res);
        });
        this.router.navigateByUrl('/examinations/'+ id);
      } else {
        this.toastr.error(response.notice);
      }

    })
  }

}
