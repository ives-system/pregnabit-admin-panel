import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { UnauthorizedExaminationService } from '../../../../_services/unauthorized_examination.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';



@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./unauthorized_examinations_index.component.html",
  providers: [UnauthorizedExaminationService],
  encapsulation: ViewEncapsulation.None,
})
export class UnauthorizedExaminationsIndexComponent implements OnInit {

  limit=10;
  query='';
  pagination:any;
  examinations:any;
  sortModel:any;
  sortOrder:any;


  constructor(
    private unauthorizedExaminationService: UnauthorizedExaminationService,
    private toastr: ToastrService,
    private router: Router,
    private translate: TranslateService
  )  {}

  ngOnInit()  {
    this.loadRecords(1);
  }


  loadRecords(page=this.pagination.offset) {
    this.unauthorizedExaminationService.index({
      offset: page, limit: this.limit, query: this.query, sortModel: this.sortModel, sortOrder: this.sortOrder
    }).then(response => {
      this.pagination = response.pagination;
      this.examinations = response.unauthorizedExaminations;
    })
  }

  authorize(id) {
    this.unauthorizedExaminationService.authorize(id).then(response => {
      if(response.authorized) {
        this.translate.get('admin_panel.examinations.authorized').subscribe((res: string) => {
          this.toastr.success(res);
        });
        this.router.navigateByUrl('/examinations/'+ id);
      } else {
        this.toastr.error(response.notice);
      }

    })
  }

  sort(sortModel) {
    if(this.sortModel != sortModel) {
      this.sortOrder = 'asc';
    } else {
      if(this.sortOrder == 'asc') {
        this.sortOrder = 'desc';
      } else {
        this.sortOrder = 'asc';
      }
    }
    this.sortModel = sortModel;
    this.loadRecords();
  }

  changeLimit(limit) {
    this.limit = limit;
    this.loadRecords(1);
  }
}
