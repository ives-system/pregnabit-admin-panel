var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var unauthorized_examination_service_1 = require('../../../../_services/unauthorized_examination.service');
var UnauthorizedExaminationsIndexComponent = (function () {
    function UnauthorizedExaminationsIndexComponent(unauthorizedExaminationService, toastr, router, translate) {
        this.unauthorizedExaminationService = unauthorizedExaminationService;
        this.toastr = toastr;
        this.router = router;
        this.translate = translate;
        this.limit = 10;
        this.query = '';
    }
    UnauthorizedExaminationsIndexComponent.prototype.ngOnInit = function () {
        this.loadRecords(1);
    };
    UnauthorizedExaminationsIndexComponent.prototype.loadRecords = function (page) {
        var _this = this;
        if (page === void 0) { page = this.pagination.offset; }
        this.unauthorizedExaminationService.index({
            offset: page, limit: this.limit, query: this.query, sortModel: this.sortModel, sortOrder: this.sortOrder
        }).then(function (response) {
            _this.pagination = response.pagination;
            _this.examinations = response.unauthorizedExaminations;
        });
    };
    UnauthorizedExaminationsIndexComponent.prototype.authorize = function (id) {
        var _this = this;
        this.unauthorizedExaminationService.authorize(id).then(function (response) {
            if (response.authorized) {
                _this.translate.get('admin_panel.examinations.authorized').subscribe(function (res) {
                    _this.toastr.success(res);
                });
                _this.router.navigateByUrl('/examinations/' + id);
            }
            else {
                _this.toastr.error(response.notice);
            }
        });
    };
    UnauthorizedExaminationsIndexComponent.prototype.sort = function (sortModel) {
        if (this.sortModel != sortModel) {
            this.sortOrder = 'asc';
        }
        else {
            if (this.sortOrder == 'asc') {
                this.sortOrder = 'desc';
            }
            else {
                this.sortOrder = 'asc';
            }
        }
        this.sortModel = sortModel;
        this.loadRecords();
    };
    UnauthorizedExaminationsIndexComponent.prototype.changeLimit = function (limit) {
        this.limit = limit;
        this.loadRecords(1);
    };
    UnauthorizedExaminationsIndexComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./unauthorized_examinations_index.component.html",
            providers: [unauthorized_examination_service_1.UnauthorizedExaminationService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], UnauthorizedExaminationsIndexComponent);
    return UnauthorizedExaminationsIndexComponent;
})();
exports.UnauthorizedExaminationsIndexComponent = UnauthorizedExaminationsIndexComponent;
//# sourceMappingURL=unauthorized_examinations_index.component.js.map