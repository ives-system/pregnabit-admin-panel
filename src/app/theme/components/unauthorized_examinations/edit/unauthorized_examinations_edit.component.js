var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var unauthorized_examination_1 = require('../../../../_models/unauthorized_examination');
var unauthorized_examination_service_1 = require('../../../../_services/unauthorized_examination.service');
var doctor_service_1 = require('../../../../_services/doctor.service');
var patient_service_1 = require('../../../../_services/patient.service');
var UnauthorizedExaminationsEditComponent = (function () {
    function UnauthorizedExaminationsEditComponent(route, router, toastr, unauthorizedExaminationService, formBuilder, doctorService, patientService, translate) {
        this.route = route;
        this.router = router;
        this.toastr = toastr;
        this.unauthorizedExaminationService = unauthorizedExaminationService;
        this.formBuilder = formBuilder;
        this.doctorService = doctorService;
        this.patientService = patientService;
        this.translate = translate;
        this.submitted = false;
        this.examination = new unauthorized_examination_1.UnauthorizedExamination();
        this.editExaminationForm = formBuilder.group({
            doctorId: ['', forms_1.Validators.required],
            patientId: ['', forms_1.Validators.required]
        });
    }
    UnauthorizedExaminationsEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = +params['id'];
            _this.unauthorizedExaminationService.show(_this.id).then(function (response) {
                _this.examination = response.detailedUnauthorizedExamination;
            });
        });
        this.patientService.list().then(function (response) {
            _this.patients = response;
        });
        this.doctorService.list().then(function (response) {
            _this.doctors = response;
        });
    };
    UnauthorizedExaminationsEditComponent.prototype.updateExamination = function () {
        var _this = this;
        this.submitted = true;
        if (this.editExaminationForm.valid) {
            this.unauthorizedExaminationService.update(this.examination).then(function (result) {
                console.log(result);
                _this.translate.get('admin_panel.examinations.updated_unauthorize').subscribe(function (res) {
                    _this.toastr.success(res);
                });
                _this.router.navigateByUrl('/unauthorized_examinations/' + _this.id);
            }, function (err) {
                var error = JSON.parse(err._body).messages.examination.join(', ');
                _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                    _this.toastr.error(error, res);
                });
            });
        }
    };
    UnauthorizedExaminationsEditComponent.prototype.chosePatient = function (patientId) {
        var _this = this;
        this.chosenPatient = this.patients[Object.keys(this.patients).find(function (k) { return _this.patients[k].id === parseInt(patientId); })];
    };
    UnauthorizedExaminationsEditComponent.prototype.choseDoctor = function (doctorId) {
        var _this = this;
        this.chosenDoctor = this.doctors[Object.keys(this.doctors).find(function (k) { return _this.doctors[k].id === parseInt(doctorId); })];
    };
    UnauthorizedExaminationsEditComponent.prototype.patientBelongsToDoctor = function () {
        if (this.chosenPatient && this.chosenDoctor && this.chosenPatient.doctorId == this.chosenDoctor.id || !(this.chosenPatient && this.chosenDoctor)) {
            return true;
        }
        else {
            return false;
        }
    };
    UnauthorizedExaminationsEditComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./unauthorized_examinations_edit.component.html",
            providers: [unauthorized_examination_service_1.UnauthorizedExaminationService, doctor_service_1.DoctorService, patient_service_1.PatientService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], UnauthorizedExaminationsEditComponent);
    return UnauthorizedExaminationsEditComponent;
})();
exports.UnauthorizedExaminationsEditComponent = UnauthorizedExaminationsEditComponent;
//# sourceMappingURL=unauthorized_examinations_edit.component.js.map