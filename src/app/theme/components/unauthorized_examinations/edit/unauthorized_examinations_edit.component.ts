import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { UnauthorizedExamination } from '../../../../_models/unauthorized_examination';
import { UnauthorizedExaminationService } from '../../../../_services/unauthorized_examination.service';
import { DoctorService } from '../../../../_services/doctor.service';
import { PatientService } from '../../../../_services/patient.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./unauthorized_examinations_edit.component.html",
  providers: [UnauthorizedExaminationService, DoctorService, PatientService],
  encapsulation: ViewEncapsulation.None,
})
export class UnauthorizedExaminationsEditComponent implements OnInit {
  id: number;
  patients:any;
  chosenPatient:any;
  doctors:any;
  chosenDoctor:any;
  examination: UnauthorizedExamination;
  editExaminationForm: FormGroup;
  submitted = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    public unauthorizedExaminationService: UnauthorizedExaminationService,
    public formBuilder: FormBuilder,
    private doctorService: DoctorService,
    private patientService: PatientService,
    private translate: TranslateService
  ){
    this.examination = new UnauthorizedExamination();
    this.editExaminationForm = formBuilder.group({
      doctorId: ['', Validators.required],
      patientId: ['', Validators.required]
    })

  }


  ngOnInit()  {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.unauthorizedExaminationService.show(this.id).then(response => {
        this.examination = response;
      })
    });
    this.patientService.list().then(response => {
      this.patients = response;
    });
    this.doctorService.list().then(response => {
      this.doctors = response;
    });
  }

  updateExamination() {
    this.submitted = true;
    if(this.editExaminationForm.valid) {
      this.unauthorizedExaminationService.update(this.examination).then((result) => {
        console.log(result);
        this.translate.get('admin_panel.examinations.updated_unauthorize').subscribe((res: string) => {
          this.toastr.success(res);
        });
        this.router.navigateByUrl('/unauthorized_examinations/'+this.id);
      }, (err) => {
        var error = JSON.parse(err._body).messages.examination.join(', ');
        this.translate.get('admin_panel.common.error').subscribe((res: string) => {
          this.toastr.error(error, res);
        });
      });
    }
  }

  chosePatient(patientId) {
    this.chosenPatient = this.patients[Object.keys(this.patients).find(k => this.patients[k].id === parseInt(patientId))];
  }

  choseDoctor(doctorId) {
    this.chosenDoctor = this.doctors[Object.keys(this.doctors).find(k => this.doctors[k].id === parseInt(doctorId))];
  }

  patientBelongsToDoctor() {
    if(this.chosenPatient && this.chosenDoctor && this.chosenPatient.doctorId == this.chosenDoctor.id || !(this.chosenPatient && this.chosenDoctor)) {
      return true
    } else {
      return false
    }
  }


}
