import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AdminUsersIndexComponent } from './index/admin_users_index.component';
import { AdminUsersNewComponent } from './new/admin_users_new.component';
import { AdminUsersShowComponent } from './show/admin_users_show.component';
import { AdminUsersEditComponent } from './edit/admin_users_edit.component';
import { AdminUsersEditPasswordComponent } from './edit_password/admin_users_edit_password.component';
import { LayoutModule } from '../../layouts/layout.module';
import { DefaultComponent } from '../../pages/default/default.component';
import { ValidatorsService } from '../../../_services/validators.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {RoleGuard} from "../../../auth/_guards/role.guard";
import { TranslateModule } from '@ngx-translate/core';


const routes: Routes = [
  {
    "path": "",
    "component": DefaultComponent,
    "children": [
      {
        "path": "",
        "component": AdminUsersIndexComponent
      },
      {
        "path": "new",
        "component": AdminUsersNewComponent
      },
      {
        "path": ":id",
        "component": AdminUsersShowComponent
      },
      {
        "path": "edit\/:id",
        "component": AdminUsersEditComponent
      },
      {
        "path": "edit_password\/:id",
        "component": AdminUsersEditPasswordComponent,
        "canActivate": [RoleGuard],
        "data": {
          "expectedRoles": ['superadmin']
        }
      }
    ]
  }
];


@NgModule({imports: [
  CommonModule,
  RouterModule.forChild(routes),
  LayoutModule,
  FormsModule,
  ReactiveFormsModule,
  ToastrModule.forRoot({positionClass: 'toast-bottom-right'}),
  NgbModule,
  TranslateModule
  ],exports: [
    RouterModule
  ], providers: [
    ValidatorsService
  ],declarations: [
  AdminUsersIndexComponent,
  AdminUsersNewComponent,
  AdminUsersShowComponent,
  AdminUsersEditComponent,
  AdminUsersEditPasswordComponent
  ]})


export class AdminUsersModule  {

}
