import { Component, ViewEncapsulation } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AdminUser } from '../../../../_models/admin_user';
import { AdminUserService } from '../../../../_services/admin_user.service';
import { PasswordValidation } from '../../../../_validators/password_validator';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./admin_users_new.component.html",
  providers: [AdminUserService],
  encapsulation: ViewEncapsulation.None,
})
export class AdminUsersNewComponent {
  adminUser: AdminUser;
  newAdminUserForm: FormGroup;
  submitted = false;

  constructor(
    private router: Router,
    private toastr: ToastrService,
    public adminUserService: AdminUserService,
    public formBuilder: FormBuilder,
    private translate: TranslateService

  ){
    this.adminUser = new AdminUser();
    this.newAdminUserForm = formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.compose([Validators.required,Validators.email])],
      role: ['', Validators.required],
      password: ['', Validators.compose([Validators.required,Validators.minLength(8), Validators.pattern(/(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[\W]).{8}/)])],
      passwordConfirmation: ['', Validators.compose([Validators.required,Validators.minLength(8), Validators.pattern(/(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[\W]).{8}/)])],
    }, {
      validator: PasswordValidation.MatchPassword
    });
    this.adminUser['role'] = 'admin';
  }


  createAdminUser() {
    this.submitted = true;
    if(this.newAdminUserForm.valid) {
      this.adminUserService.create(this.adminUser).then((result) => {
        this.translate.get('admin_panel.admin_users.form.added').subscribe((res: string) => {
          this.toastr.success(res);
        });
        this.router.navigateByUrl('/admin_users/'+ result.id);
      }, (err) => {
        var error = JSON.parse(err._body).messages.admin_user.join(', ');
        this.translate.get('admin_panel.common.error').subscribe((res: string) => {
          this.toastr.error(error, res);
        });
      });
    } else {
      console.log('invalid');
    }
  }


}
