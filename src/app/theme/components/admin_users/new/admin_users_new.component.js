var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var admin_user_1 = require('../../../../_models/admin_user');
var admin_user_service_1 = require('../../../../_services/admin_user.service');
var password_validator_1 = require('../../../../_validators/password_validator');
var AdminUsersNewComponent = (function () {
    function AdminUsersNewComponent(router, toastr, adminUserService, formBuilder, translate) {
        this.router = router;
        this.toastr = toastr;
        this.adminUserService = adminUserService;
        this.formBuilder = formBuilder;
        this.translate = translate;
        this.submitted = false;
        this.adminUser = new admin_user_1.AdminUser();
        this.newAdminUserForm = formBuilder.group({
            firstName: ['', forms_1.Validators.required],
            lastName: ['', forms_1.Validators.required],
            email: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.email])],
            role: ['', forms_1.Validators.required],
            password: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(8)])],
            passwordConfirmation: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(8)])]
        }, {
            validator: password_validator_1.PasswordValidation.MatchPassword
        });
        this.adminUser['role'] = 'admin';
    }
    AdminUsersNewComponent.prototype.createAdminUser = function () {
        var _this = this;
        this.submitted = true;
        if (this.newAdminUserForm.valid) {
            this.adminUserService.create(this.adminUser).then(function (result) {
                _this.translate.get('admin_panel.admin_users.form.added').subscribe(function (res) {
                    _this.toastr.success(res);
                });
                _this.router.navigateByUrl('/admin_users/' + result.detailedAdminUser.id);
            }, function (err) {
                var error = JSON.parse(err._body).messages.admin_user.join(', ');
                _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                    _this.toastr.error(error, res);
                });
            });
        }
        else {
            console.log('invalid');
        }
    };
    AdminUsersNewComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./admin_users_new.component.html",
            providers: [admin_user_service_1.AdminUserService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], AdminUsersNewComponent);
    return AdminUsersNewComponent;
})();
exports.AdminUsersNewComponent = AdminUsersNewComponent;
//# sourceMappingURL=admin_users_new.component.js.map