import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { AdminUserService } from '../../../../_services/admin_user.service';



@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./admin_users_index.component.html",
  providers: [AdminUserService],
  encapsulation: ViewEncapsulation.None,
})
export class AdminUsersIndexComponent implements OnInit {

  limit=10;
  query='';
  pagination:any;
  adminUsers:any;
  sortModel:any;
  sortOrder:any;
  constructor(
    private adminUserService: AdminUserService
  )  {}

  ngOnInit()  {
    this.loadRecords(1);
  }


  loadRecords(page=this.pagination.offset) {
    this.adminUserService.index({offset: page, limit: this.limit, query: this.query, sortModel: this.sortModel, sortOrder: this.sortOrder}).then(response => {
      this.pagination = response.pagination;
      this.adminUsers = response.adminUsers;
    })
  }

  sort(sortModel) {
    if(this.sortModel != sortModel) {
      this.sortOrder = 'asc';
    } else {
      if(this.sortOrder == 'asc') {
        this.sortOrder = 'desc';
      } else {
        this.sortOrder = 'asc';
      }
    }
    this.sortModel = sortModel;
    this.loadRecords();
  }

  changeLimit(limit) {
    this.limit = limit;
    this.loadRecords(1);
  }


}
