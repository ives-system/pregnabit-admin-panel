var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var admin_user_service_1 = require('../../../../_services/admin_user.service');
var AdminUsersIndexComponent = (function () {
    function AdminUsersIndexComponent(adminUserService) {
        this.adminUserService = adminUserService;
        this.limit = 10;
        this.query = '';
    }
    AdminUsersIndexComponent.prototype.ngOnInit = function () {
        this.loadRecords(1);
    };
    AdminUsersIndexComponent.prototype.loadRecords = function (page) {
        var _this = this;
        if (page === void 0) { page = this.pagination.offset; }
        this.adminUserService.index({ offset: page, limit: this.limit, query: this.query, sortModel: this.sortModel, sortOrder: this.sortOrder }).then(function (response) {
            _this.pagination = response.pagination;
            _this.adminUsers = response.adminUsers;
        });
    };
    AdminUsersIndexComponent.prototype.sort = function (sortModel) {
        if (this.sortModel != sortModel) {
            this.sortOrder = 'asc';
        }
        else {
            if (this.sortOrder == 'asc') {
                this.sortOrder = 'desc';
            }
            else {
                this.sortOrder = 'asc';
            }
        }
        this.sortModel = sortModel;
        this.loadRecords();
    };
    AdminUsersIndexComponent.prototype.changeLimit = function (limit) {
        this.limit = limit;
        this.loadRecords(1);
    };
    AdminUsersIndexComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./admin_users_index.component.html",
            providers: [admin_user_service_1.AdminUserService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], AdminUsersIndexComponent);
    return AdminUsersIndexComponent;
})();
exports.AdminUsersIndexComponent = AdminUsersIndexComponent;
//# sourceMappingURL=admin_users_index.component.js.map