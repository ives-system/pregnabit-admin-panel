var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var admin_user_service_1 = require('../../../../_services/admin_user.service');
var AdminUsersShowComponent = (function () {
    function AdminUsersShowComponent(route, adminUserService) {
        this.route = route;
        this.adminUserService = adminUserService;
    }
    AdminUsersShowComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = +params['id'];
            _this.adminUserService.show(_this.id).then(function (response) {
                _this.adminUser = response.detailedAdminUser;
            });
        });
    };
    AdminUsersShowComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./admin_users_show.component.html",
            providers: [admin_user_service_1.AdminUserService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], AdminUsersShowComponent);
    return AdminUsersShowComponent;
})();
exports.AdminUsersShowComponent = AdminUsersShowComponent;
//# sourceMappingURL=admin_users_show.component.js.map