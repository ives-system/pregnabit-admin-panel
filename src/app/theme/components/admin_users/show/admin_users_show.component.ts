import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { AdminUserService } from '../../../../_services/admin_user.service';




@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./admin_users_show.component.html",
  providers: [AdminUserService],
  encapsulation: ViewEncapsulation.None,
})
export class AdminUsersShowComponent implements OnInit {

  id: number;
  adminUser: any;
  constructor(
    private route: ActivatedRoute,
    public adminUserService: AdminUserService
  )  {

  }


  ngOnInit()  {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.adminUserService.show(this.id).then(response => {
        console.log('res', response)
        this.adminUser = response;
      })
    })
  }

}
