var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var ngx_toastr_1 = require('ngx-toastr');
var common_1 = require('@angular/common');
var router_1 = require('@angular/router');
var admin_users_index_component_1 = require('./index/admin_users_index.component');
var admin_users_new_component_1 = require('./new/admin_users_new.component');
var admin_users_show_component_1 = require('./show/admin_users_show.component');
var admin_users_edit_component_1 = require('./edit/admin_users_edit.component');
var admin_users_edit_password_component_1 = require('./edit_password/admin_users_edit_password.component');
var layout_module_1 = require('../../layouts/layout.module');
var default_component_1 = require('../../pages/default/default.component');
var validators_service_1 = require('../../../_services/validators.service');
var ng_bootstrap_1 = require('@ng-bootstrap/ng-bootstrap');
var role_guard_1 = require("../../../auth/_guards/role.guard");
var core_2 = require('@ngx-translate/core');
var routes = [
    {
        "path": "",
        "component": default_component_1.DefaultComponent,
        "children": [
            {
                "path": "",
                "component": admin_users_index_component_1.AdminUsersIndexComponent
            },
            {
                "path": "new",
                "component": admin_users_new_component_1.AdminUsersNewComponent
            },
            {
                "path": ":id",
                "component": admin_users_show_component_1.AdminUsersShowComponent
            },
            {
                "path": "edit\/:id",
                "component": admin_users_edit_component_1.AdminUsersEditComponent
            },
            {
                "path": "edit_password\/:id",
                "component": admin_users_edit_password_component_1.AdminUsersEditPasswordComponent,
                "canActivate": [role_guard_1.RoleGuard],
                "data": {
                    "expectedRoles": ['superadmin']
                }
            }
        ]
    }
];
var AdminUsersModule = (function () {
    function AdminUsersModule() {
    }
    AdminUsersModule = __decorate([
        core_1.NgModule({ imports: [
                common_1.CommonModule,
                router_1.RouterModule.forChild(routes),
                layout_module_1.LayoutModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                ngx_toastr_1.ToastrModule.forRoot({ positionClass: 'toast-bottom-right' }),
                ng_bootstrap_1.NgbModule,
                core_2.TranslateModule
            ], exports: [
                router_1.RouterModule
            ], providers: [
                validators_service_1.ValidatorsService
            ], declarations: [
                admin_users_index_component_1.AdminUsersIndexComponent,
                admin_users_new_component_1.AdminUsersNewComponent,
                admin_users_show_component_1.AdminUsersShowComponent,
                admin_users_edit_component_1.AdminUsersEditComponent,
                admin_users_edit_password_component_1.AdminUsersEditPasswordComponent
            ] })
    ], AdminUsersModule);
    return AdminUsersModule;
})();
exports.AdminUsersModule = AdminUsersModule;
//# sourceMappingURL=admin_users.module.js.map