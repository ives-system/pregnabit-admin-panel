import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AdminUser } from '../../../../_models/admin_user';
import { AdminUserService } from '../../../../_services/admin_user.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./admin_users_edit.component.html",
  providers: [AdminUserService],
  encapsulation: ViewEncapsulation.None,
})
export class AdminUsersEditComponent implements OnInit {
  id: number;
  adminUserName: any;
  adminUser: AdminUser;
  editAdminUserForm: FormGroup;
  submitted = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    public adminUserService: AdminUserService,
    public formBuilder: FormBuilder,
    private translate: TranslateService
  ){
    this.adminUser = new AdminUser();
    this.editAdminUserForm = formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.compose([Validators.required,Validators.email])],
      role: ['', Validators.required]
    })

  }


  ngOnInit()  {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.adminUserService.show(this.id).then(response => {
        this.adminUser = response;
        this.adminUserName = this.adminUser['name'];
      })
    })
  }

  updateAdminUser() {
    this.submitted = true;
    if(this.editAdminUserForm.valid) {
      this.adminUserService.update(this.adminUser).then((result) => {
        this.translate.get('admin_panel.admin_users.form.updated').subscribe((res: string) => {
          this.toastr.success(res);
        });
        this.router.navigateByUrl('/admin_users/'+this.id);
      }, (err) => {
        var error = JSON.parse(err._body).messages.admin_user.join(', ');
        this.translate.get('admin_panel.common.error').subscribe((res: string) => {
          this.toastr.error(error, res);
        });
      });
    } else {
      console.log('invalid');
    }
  }


}
