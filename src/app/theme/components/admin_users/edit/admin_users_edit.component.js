var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var admin_user_1 = require('../../../../_models/admin_user');
var admin_user_service_1 = require('../../../../_services/admin_user.service');
var AdminUsersEditComponent = (function () {
    function AdminUsersEditComponent(route, router, toastr, adminUserService, formBuilder, translate) {
        this.route = route;
        this.router = router;
        this.toastr = toastr;
        this.adminUserService = adminUserService;
        this.formBuilder = formBuilder;
        this.translate = translate;
        this.submitted = false;
        this.adminUser = new admin_user_1.AdminUser();
        this.editAdminUserForm = formBuilder.group({
            firstName: ['', forms_1.Validators.required],
            lastName: ['', forms_1.Validators.required],
            email: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.email])],
            role: ['', forms_1.Validators.required]
        });
    }
    AdminUsersEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = +params['id'];
            _this.adminUserService.show(_this.id).then(function (response) {
                _this.adminUser = response.detailedAdminUser;
                _this.adminUserName = _this.adminUser['name'];
            });
        });
    };
    AdminUsersEditComponent.prototype.updateAdminUser = function () {
        var _this = this;
        this.submitted = true;
        if (this.editAdminUserForm.valid) {
            this.adminUserService.update(this.adminUser).then(function (result) {
                _this.translate.get('admin_panel.admin_users.form.updated').subscribe(function (res) {
                    _this.toastr.success(res);
                });
                _this.router.navigateByUrl('/admin_users/' + _this.id);
            }, function (err) {
                var error = JSON.parse(err._body).messages.admin_user.join(', ');
                _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                    _this.toastr.error(error, res);
                });
            });
        }
        else {
            console.log('invalid');
        }
    };
    AdminUsersEditComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./admin_users_edit.component.html",
            providers: [admin_user_service_1.AdminUserService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], AdminUsersEditComponent);
    return AdminUsersEditComponent;
})();
exports.AdminUsersEditComponent = AdminUsersEditComponent;
//# sourceMappingURL=admin_users_edit.component.js.map