var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var password_validator_1 = require('../../../../_validators/password_validator');
var admin_user_service_1 = require('../../../../_services/admin_user.service');
var password_data_1 = require('../../../../_models/password_data');
var AdminUsersEditPasswordComponent = (function () {
    function AdminUsersEditPasswordComponent(route, router, toastr, adminUserService, formBuilder, translate) {
        this.route = route;
        this.router = router;
        this.toastr = toastr;
        this.adminUserService = adminUserService;
        this.formBuilder = formBuilder;
        this.translate = translate;
        this.submitted = false;
        this.data = new password_data_1.PasswordData();
        this.changePasswordForm = formBuilder.group({
            password: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(8)])],
            passwordConfirmation: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(8)])]
        }, {
            validator: password_validator_1.PasswordValidation.MatchPassword
        });
    }
    AdminUsersEditPasswordComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = +params['id'];
            _this.data.id = _this.id;
            _this.adminUserService.show(_this.id).then(function (response) {
                _this.adminUser = response.detailedAdminUser;
            });
        });
    };
    AdminUsersEditPasswordComponent.prototype.changePassword = function () {
        var _this = this;
        this.submitted = true;
        if (this.changePasswordForm.valid) {
            this.adminUserService.updatePassword(this.data).then(function (result) {
                _this.translate.get('admin_panel.admin_users.form.updated_password').subscribe(function (res) {
                    _this.toastr.success(res);
                });
                _this.router.navigateByUrl('/admin_users/' + _this.id);
            }, function (err) {
                _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                    _this.toastr.error(res);
                });
            });
        }
        else {
            console.log('invalid');
        }
    };
    AdminUsersEditPasswordComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./admin_users_edit_password.component.html",
            providers: [admin_user_service_1.AdminUserService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], AdminUsersEditPasswordComponent);
    return AdminUsersEditPasswordComponent;
})();
exports.AdminUsersEditPasswordComponent = AdminUsersEditPasswordComponent;
//# sourceMappingURL=admin_users_edit_password.component.js.map