import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PasswordValidation } from '../../../../_validators/password_validator';
import { AdminUserService } from '../../../../_services/admin_user.service';
import { TranslateService } from '@ngx-translate/core';
import { PasswordData } from '../../../../_models/password_data';





@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./admin_users_edit_password.component.html",
  providers: [AdminUserService],
  encapsulation: ViewEncapsulation.None,
})
export class AdminUsersEditPasswordComponent implements OnInit {
  id: number;
  adminUser: any;
  changePasswordForm: FormGroup;
  data: PasswordData;
  submitted = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    public adminUserService: AdminUserService,
    public formBuilder: FormBuilder,
    private translate: TranslateService

  ){
    this.data = new PasswordData();
    this.changePasswordForm = formBuilder.group({
      password: ['', Validators.compose([Validators.required,Validators.minLength(8),Validators.pattern(/(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[\W]).{8}/)])],
      passwordConfirmation: ['', Validators.compose([Validators.required,Validators.minLength(8), Validators.pattern(/(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[\W]).{8}/)])],
    }, {
      validator: PasswordValidation.MatchPassword
    })

  }


  ngOnInit()  {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.data.id = this.id;
      this.adminUserService.show(this.id).then(response => {
        this.adminUser = response.detailedAdminUser;
      })
    })
  }

  changePassword() {
    this.submitted = true;
    if(this.changePasswordForm.valid) {
      this.adminUserService.updatePassword(this.data).then((result) => {
        this.translate.get('admin_panel.admin_users.form.updated_password').subscribe((res: string) => {
          this.toastr.success(res);
        });
        this.router.navigateByUrl('/admin_users/'+this.id);
      }, (err) => {
        this.translate.get('admin_panel.common.error').subscribe((res: string) => {
          this.toastr.error(res);
        });
      });
    } else {
      console.log('invalid');
    }
  }


}
