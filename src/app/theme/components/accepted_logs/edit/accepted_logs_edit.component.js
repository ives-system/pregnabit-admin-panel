var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var accepted_log_1 = require('../../../../_models/accepted_log');
var accepted_log_service_1 = require('../../../../_services/accepted_log.service');
var AcceptedLogsEditComponent = (function () {
    function AcceptedLogsEditComponent(route, router, toastr, acceptedLogService, formBuilder, translate) {
        this.route = route;
        this.router = router;
        this.toastr = toastr;
        this.acceptedLogService = acceptedLogService;
        this.formBuilder = formBuilder;
        this.translate = translate;
        this.submitted = false;
        this.acceptedLog = new accepted_log_1.AcceptedLog();
        this.editAcceptedLogForm = formBuilder.group({
            name: ['', forms_1.Validators.required]
        });
    }
    AcceptedLogsEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = +params['id'];
            _this.acceptedLogService.show(_this.id).then(function (response) {
                _this.acceptedLog = response.acceptedLog;
                _this.acceptedLogName = _this.acceptedLog['name'];
            });
        });
    };
    AcceptedLogsEditComponent.prototype.updateAcceptedLog = function () {
        var _this = this;
        this.submitted = true;
        if (this.editAcceptedLogForm.valid) {
            this.acceptedLogService.update(this.acceptedLog).then(function (result) {
                _this.translate.get('admin_panel.accepted_logs.form.updated').subscribe(function (res) {
                    _this.toastr.success(res);
                });
                _this.router.navigateByUrl('/accepted_logs/' + _this.id);
            }, function (err) {
                var error = JSON.parse(err._body).messages.accepted_log.join(', ');
                _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                    _this.toastr.error(error, res);
                });
            });
        }
        else {
            console.log('invalid');
        }
    };
    AcceptedLogsEditComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./accepted_logs_edit.component.html",
            providers: [accepted_log_service_1.AcceptedLogService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], AcceptedLogsEditComponent);
    return AcceptedLogsEditComponent;
})();
exports.AcceptedLogsEditComponent = AcceptedLogsEditComponent;
//# sourceMappingURL=accepted_logs_edit.component.js.map