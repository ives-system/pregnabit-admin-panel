import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AcceptedLog } from '../../../../_models/accepted_log';
import { AcceptedLogService } from '../../../../_services/accepted_log.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./accepted_logs_edit.component.html",
  providers: [AcceptedLogService],
  encapsulation: ViewEncapsulation.None,
})
export class AcceptedLogsEditComponent implements OnInit {
  id: number;
  acceptedLogName: any;
  acceptedLog: AcceptedLog;
  editAcceptedLogForm: FormGroup;
  submitted = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    public acceptedLogService: AcceptedLogService,
    public formBuilder: FormBuilder,
    private translate: TranslateService
  ){
    this.acceptedLog = new AcceptedLog();
    this.editAcceptedLogForm = formBuilder.group({
      name: ['', Validators.required]
    })

  }


  ngOnInit()  {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.acceptedLogService.show(this.id).then(response => {
        this.acceptedLog = response;
        this.acceptedLogName = this.acceptedLog['name'];
      })
    })
  }

  updateAcceptedLog() {
    this.submitted = true;
    if(this.editAcceptedLogForm.valid) {
      this.acceptedLogService.update(this.acceptedLog).then((result) => {
        this.translate.get('admin_panel.accepted_logs.form.updated').subscribe((res: string) => {
          this.toastr.success(res);
        });
        this.router.navigateByUrl('/accepted_logs/'+this.id);
      }, (err) => {
        var error = JSON.parse(err._body).messages.accepted_log.join(', ');
        this.translate.get('admin_panel.common.error').subscribe((res: string) => {
          this.toastr.error(error, res);
        });
      });
    } else {
      console.log('invalid');
    }
  }


}
