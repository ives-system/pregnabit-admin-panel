var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var accepted_log_service_1 = require('../../../../_services/accepted_log.service');
var AcceptedLogsShowComponent = (function () {
    function AcceptedLogsShowComponent(route, acceptedLogService, toastr, router, translate) {
        this.route = route;
        this.acceptedLogService = acceptedLogService;
        this.toastr = toastr;
        this.router = router;
        this.translate = translate;
    }
    AcceptedLogsShowComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = +params['id'];
            _this.acceptedLogService.show(_this.id).then(function (response) {
                _this.acceptedLog = response.acceptedLog;
            });
        });
    };
    AcceptedLogsShowComponent.prototype.deleteLog = function (log) {
        var _this = this;
        this.acceptedLogService.delete({ id: log.id })
            .then(function (response) {
            if (response.status == 'success') {
                _this.translate.get('admin_panel.accepted_logs.delete.success').subscribe(function (res) {
                    _this.toastr.success(res);
                });
                _this.router.navigateByUrl('/accepted_logs');
            }
            else {
                _this.translate.get('admin_panel.accepted_logs.delete.error').subscribe(function (res) {
                    _this.toastr.error(res);
                });
            }
        });
    };
    AcceptedLogsShowComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./accepted_logs_show.component.html",
            providers: [accepted_log_service_1.AcceptedLogService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], AcceptedLogsShowComponent);
    return AcceptedLogsShowComponent;
})();
exports.AcceptedLogsShowComponent = AcceptedLogsShowComponent;
//# sourceMappingURL=accepted_logs_show.component.js.map