import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { AcceptedLogService } from '../../../../_services/accepted_log.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';




@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./accepted_logs_show.component.html",
  providers: [AcceptedLogService],
  encapsulation: ViewEncapsulation.None,
})
export class AcceptedLogsShowComponent implements OnInit {

  id: number;
  acceptedLog: any;
  constructor(
    private route: ActivatedRoute,
    public acceptedLogService: AcceptedLogService,
    private toastr: ToastrService,
    private router: Router,
    private translate: TranslateService
  )  {

  }


  ngOnInit()  {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.acceptedLogService.show(this.id).then(response => {
        this.acceptedLog = response;
      })
    })
  }

  deleteLog(log) {
    this.acceptedLogService.delete({id: log.id})
      .then(response => {
        if(response.status == 'success') {
          this.translate.get('admin_panel.accepted_logs.delete.success').subscribe((res: string) => {
            this.toastr.success(res);
          });
          this.router.navigateByUrl('/accepted_logs');
        } else {
          this.translate.get('admin_panel.accepted_logs.delete.error').subscribe((res: string) => {
            this.toastr.error(res);
          });
        }
      })
  }

}
