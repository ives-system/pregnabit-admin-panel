import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AcceptedLogsIndexComponent } from './index/accepted_logs_index.component';
import { AcceptedLogsNewComponent } from './new/accepted_logs_new.component';
import { AcceptedLogsShowComponent } from './show/accepted_logs_show.component';
import { AcceptedLogsEditComponent } from './edit/accepted_logs_edit.component';
import { LayoutModule } from '../../layouts/layout.module';
import { DefaultComponent } from '../../pages/default/default.component';
import { ValidatorsService } from '../../../_services/validators.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';


const routes: Routes = [
  {
    "path": "",
    "component": DefaultComponent,
    "children": [
      {
        "path": "",
        "component": AcceptedLogsIndexComponent
      },
      {
        "path": "new",
        "component": AcceptedLogsNewComponent
      },
      {
        "path": ":id",
        "component": AcceptedLogsShowComponent
      },
      {
        "path": "edit\/:id",
        "component": AcceptedLogsEditComponent
      },
    ]
  }
];


@NgModule({imports: [
  CommonModule,
  RouterModule.forChild(routes),
  LayoutModule,
  FormsModule,
  ReactiveFormsModule,
  ToastrModule.forRoot({positionClass: 'toast-bottom-right'}),
  NgbModule,
  TranslateModule
],exports: [
  RouterModule
], providers: [
  ValidatorsService
],declarations: [
  AcceptedLogsIndexComponent,
  AcceptedLogsShowComponent,
  AcceptedLogsEditComponent,
  AcceptedLogsNewComponent
]})


export class AcceptedLogsModule  {

}
