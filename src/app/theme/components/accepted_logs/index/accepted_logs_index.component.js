var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var accepted_log_service_1 = require('../../../../_services/accepted_log.service');
var AcceptedLogsIndexComponent = (function () {
    function AcceptedLogsIndexComponent(acceptedLogService, toastr, translate) {
        this.acceptedLogService = acceptedLogService;
        this.toastr = toastr;
        this.translate = translate;
        this.limit = 10;
        this.query = '';
    }
    AcceptedLogsIndexComponent.prototype.ngOnInit = function () {
        this.loadRecords(1);
    };
    AcceptedLogsIndexComponent.prototype.loadRecords = function (page) {
        var _this = this;
        if (page === void 0) { page = this.pagination.offset; }
        this.acceptedLogService.index({ offset: page, limit: this.limit, query: this.query, sortModel: this.sortModel, sortOrder: this.sortOrder }).then(function (response) {
            _this.pagination = response.pagination;
            _this.acceptedLogs = response.acceptedLogs;
        });
    };
    AcceptedLogsIndexComponent.prototype.sort = function (sortModel) {
        if (this.sortModel != sortModel) {
            this.sortOrder = 'asc';
        }
        else {
            if (this.sortOrder == 'asc') {
                this.sortOrder = 'desc';
            }
            else {
                this.sortOrder = 'asc';
            }
        }
        this.sortModel = sortModel;
        this.loadRecords();
    };
    AcceptedLogsIndexComponent.prototype.changeLimit = function (limit) {
        this.limit = limit;
        this.loadRecords(1);
    };
    AcceptedLogsIndexComponent.prototype.deleteLog = function (log) {
        var _this = this;
        this.acceptedLogService.delete({ id: log.id })
            .then(function (response) {
            if (response.status == 'success') {
                _this.acceptedLogs.splice(_this.acceptedLogs.indexOf(log), 1);
                _this.translate.get('admin_panel.accepted_logs.delete.success').subscribe(function (res) {
                    _this.toastr.success(res);
                });
            }
            else {
                _this.translate.get('admin_panel.accepted_logs.delete.error').subscribe(function (res) {
                    _this.toastr.error(res);
                });
            }
        });
    };
    AcceptedLogsIndexComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./accepted_logs_index.component.html",
            providers: [accepted_log_service_1.AcceptedLogService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], AcceptedLogsIndexComponent);
    return AcceptedLogsIndexComponent;
})();
exports.AcceptedLogsIndexComponent = AcceptedLogsIndexComponent;
//# sourceMappingURL=accepted_logs_index.component.js.map