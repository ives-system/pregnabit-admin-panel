import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { AcceptedLogService } from '../../../../_services/accepted_log.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';



@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./accepted_logs_index.component.html",
  providers: [AcceptedLogService],
  encapsulation: ViewEncapsulation.None,
})
export class AcceptedLogsIndexComponent implements OnInit {

  limit=10;
  query='';
  pagination:any;
  acceptedLogs:any;
  sortModel:any;
  sortOrder:any;
  constructor(
    private acceptedLogService: AcceptedLogService,
    private toastr: ToastrService,
    private translate: TranslateService
  )  {}

  ngOnInit()  {
    this.loadRecords(1);
  }


  loadRecords(page=this.pagination.offset) {
    this.acceptedLogService.index({offset: page, limit: this.limit, query: this.query, sortModel: this.sortModel, sortOrder: this.sortOrder}).then(response => {
      this.pagination = response.pagination;
      this.acceptedLogs = response.acceptedLogs;
    })
  }

  sort(sortModel) {
    if(this.sortModel != sortModel) {
      this.sortOrder = 'asc';
    } else {
      if(this.sortOrder == 'asc') {
        this.sortOrder = 'desc';
      } else {
        this.sortOrder = 'asc';
      }
    }
    this.sortModel = sortModel;
    this.loadRecords();
  }

  changeLimit(limit) {
    this.limit = limit;
    this.loadRecords(1);
  }


  deleteLog(log) {
    this.acceptedLogService.delete({id: log.id})
      .then(response => {
        console.log(response.status)

        if(response.status == 'success') {
          this.acceptedLogs.splice(this.acceptedLogs.indexOf(log), 1);
          this.translate.get('admin_panel.accepted_logs.delete.success').subscribe((res: string) => {
            this.toastr.success(res);
          });
        } else {
          this.translate.get('admin_panel.accepted_logs.delete.error').subscribe((res: string) => {
            this.toastr.error(res);
          });
        }
      })
  }

}
