var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var accepted_log_1 = require('../../../../_models/accepted_log');
var accepted_log_service_1 = require('../../../../_services/accepted_log.service');
var AcceptedLogsNewComponent = (function () {
    function AcceptedLogsNewComponent(router, toastr, scceptedLogService, formBuilder, translate) {
        this.router = router;
        this.toastr = toastr;
        this.scceptedLogService = scceptedLogService;
        this.formBuilder = formBuilder;
        this.translate = translate;
        this.submitted = false;
        this.acceptedLog = new accepted_log_1.AcceptedLog();
        this.newAcceptedLogForm = formBuilder.group({
            name: ['', forms_1.Validators.required]
        });
    }
    AcceptedLogsNewComponent.prototype.createAcceptedLog = function () {
        var _this = this;
        this.submitted = true;
        if (this.newAcceptedLogForm.valid) {
            this.scceptedLogService.create(this.acceptedLog).then(function (result) {
                _this.translate.get('admin_panel.accepted_logs.form.added').subscribe(function (res) {
                    _this.toastr.success(res);
                });
                _this.router.navigateByUrl('/accepted_logs/' + result.acceptedLog.id);
            }, function (err) {
                var error = JSON.parse(err._body).messages.accepted_log.join(', ');
                _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                    _this.toastr.error(error, res);
                });
            });
        }
        else {
            console.log('invalid');
        }
    };
    AcceptedLogsNewComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./accepted_logs_new.component.html",
            providers: [accepted_log_service_1.AcceptedLogService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], AcceptedLogsNewComponent);
    return AcceptedLogsNewComponent;
})();
exports.AcceptedLogsNewComponent = AcceptedLogsNewComponent;
//# sourceMappingURL=accepted_logs_new.component.js.map