import { Component, ViewEncapsulation } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AcceptedLog } from '../../../../_models/accepted_log';
import { AcceptedLogService } from '../../../../_services/accepted_log.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./accepted_logs_new.component.html",
  providers: [AcceptedLogService],
  encapsulation: ViewEncapsulation.None,
})
export class AcceptedLogsNewComponent {
  acceptedLog: AcceptedLog;
  newAcceptedLogForm: FormGroup;
  submitted = false;

  constructor(
    private router: Router,
    private toastr: ToastrService,
    public scceptedLogService: AcceptedLogService,
    public formBuilder: FormBuilder,
    private translate: TranslateService

  ){
    this.acceptedLog = new AcceptedLog();
    this.newAcceptedLogForm = formBuilder.group({
      name: ['', Validators.required]
    });
  }


  createAcceptedLog() {
    this.submitted = true;
    if(this.newAcceptedLogForm.valid) {
      this.scceptedLogService.create(this.acceptedLog).then((result) => {
        this.translate.get('admin_panel.accepted_logs.form.added').subscribe((res: string) => {
          this.toastr.success(res);
        });
        this.router.navigateByUrl('/accepted_logs/'+ result.id);
      }, (err) => {
        var error = JSON.parse(err._body).messages.accepted_log.join(', ');
        this.translate.get('admin_panel.common.error').subscribe((res: string) => {
          this.toastr.error(error, res);
        });
      });
    } else {
      console.log('invalid');
    }
  }


}
