var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var doctor_service_1 = require('../../../../_services/doctor.service');
var DoctorsShowComponent = (function () {
    function DoctorsShowComponent(route, router, toastr, doctorService, translate) {
        var _this = this;
        this.route = route;
        this.router = router;
        this.toastr = toastr;
        this.doctorService = doctorService;
        this.translate = translate;
        this.limit = 10;
        translate.onLangChange.subscribe(function (lang) {
            _this.loadTranslates();
        });
    }
    DoctorsShowComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = +params['id'];
            _this.doctorService.show(_this.id).then(function (response) {
                _this.doctor = response.detailedDoctor;
            }, function (err) {
                _this.router.navigateByUrl('/404');
            });
        });
        this.loadRecords(1);
        this.loadTranslates();
    };
    DoctorsShowComponent.prototype.loadRecords = function (page) {
        var _this = this;
        if (page === void 0) { page = this.pagination.offset; }
        this.doctorService.getAudits({ id: this.id, limit: this.limit, offset: page }).then(function (response) {
            _this.audits = response.audits;
            _this.pagination = response.pagination;
        });
    };
    DoctorsShowComponent.prototype.resendConfirmationEmail = function () {
        var _this = this;
        this.doctorService.resendConfirmationEmail(this.id).then(function (result) {
            console.log(result);
            _this.translate.get('admin_panel.doctors.confirmation_email_sended_success').subscribe(function (res) {
                _this.toastr.success(res);
            });
            _this.router.navigateByUrl('/doctors/' + _this.doctor.id);
        }, function (err) {
            _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                _this.toastr.error(res);
            });
        });
    };
    DoctorsShowComponent.prototype.getCrmStatus = function () {
        var _this = this;
        this.doctorService.getCrmStatus(this.id).then(function (result) {
            console.log(result.state);
            _this.translate.get('admin_panel.doctors.crm_status_taken_success').subscribe(function (res) {
                _this.toastr.success(res + result.state);
            });
            _this.router.navigateByUrl('/doctors/' + _this.doctor.id);
        }, function (err) {
            _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                _this.toastr.error(res);
            });
        });
    };
    DoctorsShowComponent.prototype.loadTranslates = function () {
        var _this = this;
        this.translate.get([
            'admin_panel.doctors.types_of_activity.doctor',
            'admin_panel.doctors.types_of_activity.midwife',
            'admin_panel.doctors.types_of_activity.therapeutic',
            'admin_panel.doctors.types_of_activity.school_of_childbirth',
            'admin_panel.doctors.company_sizes.one',
            'admin_panel.doctors.company_sizes.up_to_ten',
            'admin_panel.doctors.company_sizes.up_to_fifty',
            'admin_panel.doctors.company_sizes.above_fifty',
            'admin_panel.doctors.voivodeships.dolnoslaskie',
            'admin_panel.doctors.voivodeships.kujawsko_pomorskie',
            'admin_panel.doctors.voivodeships.lubelskie',
            'admin_panel.doctors.voivodeships.lubuskie',
            'admin_panel.doctors.voivodeships.lodzkie',
            'admin_panel.doctors.voivodeships.malopolskie',
            'admin_panel.doctors.voivodeships.mazowieckie',
            'admin_panel.doctors.voivodeships.opolskie',
            'admin_panel.doctors.voivodeships.podkarpackie',
            'admin_panel.doctors.voivodeships.podlaskie',
            'admin_panel.doctors.voivodeships.pomorskie',
            'admin_panel.doctors.voivodeships.slaskie',
            'admin_panel.doctors.voivodeships.swietokrzyskie',
            'admin_panel.doctors.voivodeships.warminsko_mazurskie',
            'admin_panel.doctors.voivodeships.wielkopolskie',
            'admin_panel.doctors.voivodeships.zachodniopomorskie'])
            .subscribe(function (res) {
            _this.typesOfActivity = {
                'Doctor': res['admin_panel.doctors.types_of_activity.doctor'],
                'Midwife': res['admin_panel.doctors.types_of_activity.midwife'],
                'Therapeutic': res['admin_panel.doctors.types_of_activity.therapeutic'],
                'SchoolOfChildbirth': res['admin_panel.doctors.types_of_activity.school_of_childbirth'] };
            _this.companySizes = {
                'One': res['admin_panel.doctors.company_sizes.one'],
                'UpToTen': res['admin_panel.doctors.company_sizes.up_to_ten'],
                'UpToFifty': res['admin_panel.doctors.company_sizes.up_to_fifty'],
                'AboveFifty': res['admin_panel.doctors.company_sizes.above_fifty'] };
            _this.voivodeships = {
                'Dolnoslaskie': res['admin_panel.doctors.voivodeships.dolnoslaskie'],
                'KujawskoPomorskie': res['admin_panel.doctors.voivodeships.kujawsko_pomorskie'],
                'Lubelskie': res['admin_panel.doctors.voivodeships.lubelskie'],
                'Lubuskie': res['admin_panel.doctors.voivodeships.lubuskie'],
                'Lodzkie': res['admin_panel.doctors.voivodeships.lodzkie'],
                'Malopolskie': res['admin_panel.doctors.voivodeships.malopolskie'],
                'Mazowieckie': res['admin_panel.doctors.voivodeships.mazowieckie'],
                'Opolskie': res['admin_panel.doctors.voivodeships.opolskie'],
                'Podkarpackie': res['admin_panel.doctors.voivodeships.podkarpackie'],
                'Pomorskie': res['admin_panel.doctors.voivodeships.pomorskie'],
                'Podlaskie': res['admin_panel.doctors.voivodeships.podlaskie'],
                'Slaskie': res['admin_panel.doctors.voivodeships.slaskie'],
                'Swietokrzyskie': res['admin_panel.doctors.voivodeships.swietokrzyskie'],
                'WarminskoMazurskie': res['admin_panel.doctors.voivodeships.warminsko_mazurskie'],
                'Wielkopolskie': res['admin_panel.doctors.voivodeships.wielkopolskie'],
                'Zachodniopomorskie': res['admin_panel.doctors.voivodeships.zachodniopomorskie'] };
        });
    };
    DoctorsShowComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./doctors_show.component.html",
            providers: [doctor_service_1.DoctorService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], DoctorsShowComponent);
    return DoctorsShowComponent;
})();
exports.DoctorsShowComponent = DoctorsShowComponent;
//# sourceMappingURL=doctors_show.component.js.map