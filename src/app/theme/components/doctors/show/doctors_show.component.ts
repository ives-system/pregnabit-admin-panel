import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { DoctorService } from '../../../../_services/doctor.service';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { changedAuditValue, newAuditValue } from '../../../../_utils/audit_helper';





@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./doctors_show.component.html",
  providers: [DoctorService],
  encapsulation: ViewEncapsulation.None,
})
export class DoctorsShowComponent implements OnInit {

  id: number;
  doctor: any;
  limit=10;
  pagination:any;
  audits:any;
  typesOfActivity:any;
  companySizes:any;
  changeLimit:any;



  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    public doctorService: DoctorService,
    private translate: TranslateService
  )  {
    translate.onLangChange.subscribe((lang) => {
      this.loadTranslates();
    })
  }


  ngOnInit()  {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.doctorService.show(this.id).then(response => {
        this.doctor = response;
      }, (err) => {
        this.router.navigateByUrl('/404');
      })
    });
    this.loadRecords(1);
    this.loadTranslates();
  }

  changedParameter(change) {
    return changedAuditValue(change)
  }

  newParameter(change) {
    return newAuditValue(change)
  }

  loadRecords(page=this.pagination.offset) {
    this.doctorService.getAudits({id: this.id, limit: this.limit, offset: page}).then(response => {
      this.audits = response.audits;
      this.pagination = response.pagination;
    })
  }
  resendConfirmationEmail() {
    this.doctorService.resendConfirmationEmail(this.id).then((result) => {
      console.log(result)
      this.translate.get('admin_panel.doctors.confirmation_email_sended_success').subscribe((res: string) => {
        this.toastr.success(res);
      });
      this.router.navigateByUrl('/doctors/'+ this.doctor.id);
    }, (err) => {
      this.translate.get('admin_panel.common.error').subscribe((res: string) => {
        this.toastr.error(res);
      });
    });
  }

  getCrmStatus() {
    this.doctorService.getCrmStatus(this.id).then((result) => {
      console.log(result.state);
      this.translate.get('admin_panel.doctors.crm_status_taken_success').subscribe((res: string) => {
        this.toastr.success(res + result.state);
      });
      this.router.navigateByUrl('/doctors/'+ this.doctor.id);
    }, (err) => {
      this.translate.get('admin_panel.common.error').subscribe((res: string) => {
        this.toastr.error(res);
      });
    });

  }

  loadTranslates() {
    this.translate.get([
        'admin_panel.doctors.types_of_activity.doctor',
        'admin_panel.doctors.types_of_activity.midwife',
        'admin_panel.doctors.types_of_activity.therapeutic',
        'admin_panel.doctors.types_of_activity.school_of_childbirth',
        'admin_panel.doctors.company_sizes.one',
        'admin_panel.doctors.company_sizes.up_to_ten',
        'admin_panel.doctors.company_sizes.up_to_fifty',
        'admin_panel.doctors.company_sizes.above_fifty'])
      .subscribe((res: string) => {
        this.typesOfActivity = {
          'Doctor': res['admin_panel.doctors.types_of_activity.doctor'],
          'Midwife': res['admin_panel.doctors.types_of_activity.midwife'],
          'Therapeutic': res['admin_panel.doctors.types_of_activity.therapeutic'],
          'SchoolOfChildbirth': res['admin_panel.doctors.types_of_activity.school_of_childbirth']};
        this.companySizes = {
          'One': res['admin_panel.doctors.company_sizes.one'],
          'UpToTen': res['admin_panel.doctors.company_sizes.up_to_ten'],
          'UpToFifty': res['admin_panel.doctors.company_sizes.up_to_fifty'],
          'AboveFifty': res['admin_panel.doctors.company_sizes.above_fifty']};
      });
  }

}
