import {Component, OnInit, ViewChild, ElementRef, ViewEncapsulation} from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Doctor } from '../../../../_models/doctor';
import { DoctorService } from '../../../../_services/doctor.service';
import { MedicalUnitService } from '../../../../_services/medical_unit.service';
import { MerchantUserService } from '../../../../_services/merchant_user.service';
import { TranslateService } from '@ngx-translate/core';
import { Ng2TelInput } from "ng2-tel-input";

@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./doctors_edit.component.html",
  providers: [DoctorService, MedicalUnitService, MerchantUserService],
  encapsulation: ViewEncapsulation.None,
})
export class DoctorsEditComponent implements OnInit {
  id: number;
  doctor: Doctor;
  doctorName:any;
  editDoctorForm: FormGroup;
  submitted = false;
  typesOfActivity: any;
  companySizes: any;
  medicalUnits: any;
  merchants: any;
  objectKeys = Object.keys;

  @ViewChild(Ng2TelInput) phoneNumber: Ng2TelInput;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    public doctorService: DoctorService,
    public formBuilder: FormBuilder,
    private medicalUnitService: MedicalUnitService,
    private merchantUserService: MerchantUserService,
    private translate: TranslateService
  ){
    translate.onLangChange.subscribe((lang) => {
      this.loadTranslates();
    });
    this.doctor = new Doctor();
    this.editDoctorForm = formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.compose([Validators.required,Validators.email])],
      phoneNumber: [''],
      licence: [''],
      title: [''],
      specializations: [''],
      medicalUnitId: [''],
      merchantId: [''],
      // isBusinessOwner: [''],
      company: ['', Validators.required],
      nip: ['', Validators.compose([Validators.required, Validators.maxLength(30), Validators.pattern(/^[0-9a-zA-Z-_]+$/)])],
      street: ['', Validators.required],
      zipCode: ['', Validators.compose([Validators.required, Validators.maxLength(10) ,Validators.pattern(/^[0-9A-Za-z-]+$/)])],
      city: ['', Validators.required],
      streetNumber: ['', Validators.required],
      apartmentNumber: ['', null],
      typeOfActivity: [''],
      companySize: [''],
      canEditPatients: [''],
      canEditDevices: [''],
      canSeeAllPatients: [''],
      canSeeMedicalRecords: [''],
      visibleExaminations: [''],
      lang: ['', Validators.required],
      doctorCode: ['']
    });
  }

  ngOnInit()  {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.doctorService.show(this.id).then(response => {
        this.doctor = response;
        this.doctorName = this.doctor['fullName'];
        this.phoneNumber.ngTelInput.intlTelInput('setNumber', this.doctor['phoneNumber']);
      })
    });
    this.merchantUserService.list().then(response => {
      this.merchants = response;
    });
    this.medicalUnitService.list().then(response => {
      this.medicalUnits = response;
    });
    this.loadTranslates();
  }

  unitChange(unitId) {
    const chosenUnit = this.medicalUnits.filter(unit => unit.id === parseInt(unitId))[0]
    console.log('chosenUnit', chosenUnit)
    this.doctor.company = chosenUnit.company
    this.doctor.nip = chosenUnit.nip
    this.doctor.street = chosenUnit.street
    this.doctor.zipCode = chosenUnit.zipCode
    this.doctor.city = chosenUnit.city
    this.doctor.streetNumber = chosenUnit.streetNumber
    this.doctor.apartmentNumber = chosenUnit.apartmentNumber
    this.doctor.typeOfActivity = chosenUnit.typeOfActivity
    this.doctor.companySize = chosenUnit.companySize
  }

  updateDoctor() {
    this.submitted = true;
    if(this.editDoctorForm.valid) {
      this.doctorService.update(this.doctor).then((result) => {
        this.translate.get('admin_panel.doctors.form.updated').subscribe((res: string) => {
          this.toastr.success(res);
        });
        this.router.navigateByUrl('/doctors/'+this.id);
      }, (err) => {
        console.log('error =>', err._body)
        var error = JSON.parse(err._body).doctor.join(', ');
        this.translate.get('admin_panel.common.error').subscribe((res: string) => {
          this.toastr.error(error, res);
        });
      });
    } else {
      console.log('invalid');
    }
  }

  loadTranslates() {
    this.translate.get([
        'admin_panel.doctors.types_of_activity.doctor',
        'admin_panel.doctors.types_of_activity.midwife',
        'admin_panel.doctors.types_of_activity.therapeutic',
        'admin_panel.doctors.types_of_activity.school_of_childbirth',
        'admin_panel.doctors.company_sizes.one',
        'admin_panel.doctors.company_sizes.up_to_ten',
        'admin_panel.doctors.company_sizes.up_to_fifty',
        'admin_panel.doctors.company_sizes.above_fifty'])
      .subscribe((res: string) => {
        this.typesOfActivity = {
          'Doctor': res['admin_panel.doctors.types_of_activity.doctor'],
          'Midwife': res['admin_panel.doctors.types_of_activity.midwife'],
          'Therapeutic': res['admin_panel.doctors.types_of_activity.therapeutic'],
          'SchoolOfChildbirth': res['admin_panel.doctors.types_of_activity.school_of_childbirth']};
        this.companySizes = {
          'One': res['admin_panel.doctors.company_sizes.one'],
          'UpToTen': res['admin_panel.doctors.company_sizes.up_to_ten'],
          'UpToFifty': res['admin_panel.doctors.company_sizes.up_to_fifty'],
          'AboveFifty': res['admin_panel.doctors.company_sizes.above_fifty']};
      });
  }

  telInputObject(obj) {
  }

  hasError: boolean;
  onError(obj) {
    this.hasError = obj;
  }

  getNumber(obj) {
    this.doctor['phoneNumber'] = obj;
  }

 onCountryChange(obj) {
 }
}
