var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var doctor_service_1 = require('../../../../_services/doctor.service');
var medical_unit_service_1 = require('../../../../_services/medical_unit.service');
var merchant_user_service_1 = require('../../../../_services/merchant_user.service');
var DoctorsIndexComponent = (function () {
    function DoctorsIndexComponent(doctorService, merchantUserService, medicalUnitService) {
        this.doctorService = doctorService;
        this.merchantUserService = merchantUserService;
        this.medicalUnitService = medicalUnitService;
        this.limit = 10;
        this.query = '';
        this.filters = { medicalUnitId: '', merchantId: '' };
    }
    DoctorsIndexComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadRecords(1);
        this.merchantUserService.list().then(function (response) {
            _this.merchants = response;
        });
        this.medicalUnitService.list().then(function (response) {
            _this.medicalUnits = response;
        });
    };
    DoctorsIndexComponent.prototype.loadRecords = function (page) {
        var _this = this;
        if (page === void 0) { page = this.pagination.offset; }
        this.doctorService.index({
            offset: page, limit: this.limit, query: this.query, sortModel: this.sortModel,
            sortOrder: this.sortOrder, medicalUnitId: this.filters.medicalUnitId,
            merchantId: this.filters.merchantId
        }).then(function (response) {
            _this.pagination = response.pagination;
            _this.doctors = response.doctors;
        });
    };
    DoctorsIndexComponent.prototype.sort = function (sortModel) {
        if (this.sortModel != sortModel) {
            this.sortOrder = 'asc';
        }
        else {
            if (this.sortOrder == 'asc') {
                this.sortOrder = 'desc';
            }
            else {
                this.sortOrder = 'asc';
            }
        }
        this.sortModel = sortModel;
        this.loadRecords();
    };
    DoctorsIndexComponent.prototype.changeLimit = function (limit) {
        this.limit = limit;
        this.loadRecords(1);
    };
    DoctorsIndexComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./doctors_index.component.html",
            providers: [doctor_service_1.DoctorService, medical_unit_service_1.MedicalUnitService, merchant_user_service_1.MerchantUserService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], DoctorsIndexComponent);
    return DoctorsIndexComponent;
})();
exports.DoctorsIndexComponent = DoctorsIndexComponent;
//# sourceMappingURL=doctors_index.component.js.map