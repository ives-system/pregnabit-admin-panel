import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { DoctorService } from '../../../../_services/doctor.service';
import { MedicalUnitService } from '../../../../_services/medical_unit.service';
import { MerchantUserService } from '../../../../_services/merchant_user.service';


@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./doctors_index.component.html",
  providers: [DoctorService, MedicalUnitService, MerchantUserService],
  encapsulation: ViewEncapsulation.None,
})
export class DoctorsIndexComponent implements OnInit {

  limit=10;
  query='';
  pagination:any;
  doctors:any;
  sortModel:any;
  sortOrder:any;
  medicalUnits:any;
  merchants:any;
  filters = {medicalUnitId: '', merchantId: ''};

  constructor(
    private doctorService: DoctorService,
    private merchantUserService: MerchantUserService,
    private medicalUnitService: MedicalUnitService
  )  {}

  ngOnInit()  {
    this.loadRecords(1);
    this.merchantUserService.list().then(response => {
      this.merchants = response;
    });
    this.medicalUnitService.list().then(response => {
      this.medicalUnits = response;
    })
  }


  loadRecords(page=this.pagination.offset) {
    this.doctorService.index({
      offset: page, limit: this.limit, query: this.query, sortModel: this.sortModel,
      sortOrder: this.sortOrder, medicalUnitId: this.filters.medicalUnitId,
      merchantId: this.filters.merchantId
    }).then(response => {
      this.pagination = response.pagination;
      this.doctors = response.doctors;
      console.log(this.doctors)
    })
  }

  sort(sortModel) {
    if(this.sortModel != sortModel) {
      this.sortOrder = 'asc';
    } else {
      if(this.sortOrder == 'asc') {
        this.sortOrder = 'desc';
      } else {
        this.sortOrder = 'asc';
      }
    }
    this.sortModel = sortModel;
    this.loadRecords();
  }

  changeLimit(limit) {
    this.limit = limit;
    this.loadRecords(1);
  }
}
