import { Component, ViewEncapsulation } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Doctor } from '../../../../_models/doctor';
import { DoctorService } from '../../../../_services/doctor.service';
import { MedicalUnitService } from '../../../../_services/medical_unit.service';
import { MerchantUserService } from '../../../../_services/merchant_user.service';
import { TranslateService } from '@ngx-translate/core';
import CountrySelector from '../../../../_utils/country_selector'

@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./doctors_new.component.html",
  providers: [DoctorService, MedicalUnitService, MerchantUserService],
  encapsulation: ViewEncapsulation.None,
})
export class DoctorsNewComponent {
  doctor: Doctor;
  newDoctorForm: FormGroup;
  submitted = false;
  typesOfActivity: any;
  companySizes: any;
  medicalUnits:any;
  merchants:any;
  objectKeys = Object.keys;

  constructor(
    private router: Router,
    private toastr: ToastrService,
    public medicalUnitService: MedicalUnitService,
    public merchantUserService: MerchantUserService,
    public doctorService: DoctorService,
    public formBuilder: FormBuilder,
    private translate: TranslateService

  ){
    translate.onLangChange.subscribe((lang) => {
      this.loadTranslates();
    });
    this.doctor = new Doctor();
    this.doctor['canEditPatients'] = true;
    this.doctor['canEditDevices'] = true;
    this.doctor['canSeeMedicalRecords'] = true;
    this.doctor['typeOfActivity'] = '';
    this.doctor['companySize'] = '';
    this.newDoctorForm = formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.compose([Validators.required,Validators.email])],
      phoneNumber: [''],
      licence: [''],
      title: [''],
      specializations: [''],
      medicalUnitId: [''],
      merchantId: [''],
      // isBusinessOwner: [''],
      company: ['', Validators.required],
      nip: ['', Validators.compose([Validators.required, Validators.maxLength(30), Validators.pattern(/^[0-9a-zA-Z-_]+$/)])],
      street: ['', Validators.required],
      zipCode: ['', Validators.compose([Validators.required, Validators.maxLength(10) ,Validators.pattern(/^[0-9A-Za-z-]+$/)])],
      city: ['', Validators.required],
      streetNumber: ['', Validators.required],
      apartmentNumber: ['', null],
      typeOfActivity: [''],
      companySize: [''],
      canEditPatients: [''],
      canEditDevices: [''],
      canSeeAllPatients: [''],
      canSeeMedicalRecords: [''],
      visibleExaminations: [''],
      lang: ['', Validators.required],
      doctorCode: ['']
    });
  }

  ngOnInit()  {
    this.medicalUnitService.list().then(response => {
      this.medicalUnits = response;
    });
    this.merchantUserService.list().then(response => {
      this.merchants = response;
    });
    this.loadTranslates();
  }


  createDoctor() {
    this.submitted = true;
    if(this.newDoctorForm.valid) {
      this.doctorService.create(this.doctor).then((result) => {
        this.translate.get('admin_panel.doctors.form.added').subscribe((res: string) => {
          this.toastr.success(res);
        });
        this.router.navigateByUrl('/doctors/'+ result.id);
      }, (err) => {
        var error = JSON.parse(err._body).messages.doctor.join(', ');
        this.translate.get('admin_panel.common.error').subscribe((res: string) => {
          this.toastr.error(error, res);
        });
      });
    } else {
      console.log('invalid');
    }
  }

  chooseMedicalUnit() {
    if(this.doctor['medicalUnitId']) {
      this.medicalUnitService.show(this.doctor['medicalUnitId']).then((result) => {
        this.doctor['company'] = result['company'];
        this.doctor['nip'] = result['nip'];
        this.doctor['street'] = result['street'];
        this.doctor['zipCode'] = result['zipCode'];
        this.doctor['city'] = result['city'];
        this.doctor['streetNumber'] = result['streetNumber'];
        this.doctor['apartmentNumber'] = result['apartmentNumber'];
        this.doctor['typeOfActivity'] = result['typeOfActivity'];
        this.doctor['companySize'] = result['companySize'];
      })
    }
  }

  loadTranslates() {
    this.translate.get([
        'admin_panel.doctors.types_of_activity.doctor',
        'admin_panel.doctors.types_of_activity.midwife',
        'admin_panel.doctors.types_of_activity.therapeutic',
        'admin_panel.doctors.types_of_activity.school_of_childbirth',
        'admin_panel.doctors.company_sizes.one',
        'admin_panel.doctors.company_sizes.up_to_ten',
        'admin_panel.doctors.company_sizes.up_to_fifty',
        'admin_panel.doctors.company_sizes.above_fifty'])
      .subscribe((res: string) => {
        this.typesOfActivity = {
          'Doctor': res['admin_panel.doctors.types_of_activity.doctor'],
          'Midwife': res['admin_panel.doctors.types_of_activity.midwife'],
          'Therapeutic': res['admin_panel.doctors.types_of_activity.therapeutic'],
          'SchoolOfChildbirth': res['admin_panel.doctors.types_of_activity.school_of_childbirth']};
        this.companySizes = {
          'One': res['admin_panel.doctors.company_sizes.one'],
          'UpToTen': res['admin_panel.doctors.company_sizes.up_to_ten'],
          'UpToFifty': res['admin_panel.doctors.company_sizes.up_to_fifty'],
          'AboveFifty': res['admin_panel.doctors.company_sizes.above_fifty']};
      });
  }

  telInputObject(obj) {
    CountrySelector.select(obj);
  }

  hasError: boolean;
  onError(obj) {
    this.hasError = obj;
    console.log('hasError: ', obj);
  }

  getNumber(obj) {
    this.doctor['phoneNumber'] = obj;
  }
}
