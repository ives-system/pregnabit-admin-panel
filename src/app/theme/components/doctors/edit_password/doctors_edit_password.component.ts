import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PasswordValidation } from '../../../../_validators/password_validator';
import { PasswordData } from '../../../../_models/password_data';
import { DoctorService } from '../../../../_services/doctor.service';
import { TranslateService } from '@ngx-translate/core';





@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./doctors_edit_password.component.html",
  providers: [DoctorService],
  encapsulation: ViewEncapsulation.None,
})
export class DoctorsEditPasswordComponent implements OnInit {
  id: number;
  doctor: any;
  changePasswordForm: FormGroup;
  data:PasswordData;
  submitted = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    public doctorService: DoctorService,
    public formBuilder: FormBuilder,
    private translate: TranslateService

  ){
    this.data = new PasswordData();
    this.changePasswordForm = formBuilder.group({
      password: ['', Validators.compose([Validators.required,Validators.minLength(8), Validators.pattern(/(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[\W]).{8}/)])],
      passwordConfirmation: ['', Validators.compose([Validators.required,Validators.minLength(8), Validators.pattern(/(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[\W]).{8}/)])],
    }, {
      validator: PasswordValidation.MatchPassword
    })

  }


  ngOnInit()  {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.data.id = this.id;
      this.doctorService.show(this.id).then(response => {
        this.doctor = response.detailedDoctor;
      })
    })
  }

  changePassword() {
    this.submitted = true;
    if(this.changePasswordForm.valid) {
      this.doctorService.updatePassword(this.data).then((result) => {
        this.translate.get('admin_panel.admin_users.form.updated_password').subscribe((res: string) => {
          this.toastr.success(res);
        });
        this.router.navigateByUrl('/doctors/'+this.id);
      }, (err) => {
        this.translate.get('admin_panel.common.error').subscribe((res: string) => {
          this.toastr.error(res);
        });
      });
    } else {
      console.log('invalid');
    }
  }


}
