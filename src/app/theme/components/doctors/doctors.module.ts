import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DoctorsIndexComponent } from './index/doctors_index.component';
import { DoctorsShowComponent } from './show/doctors_show.component';
import { DoctorsNewComponent } from './new/doctors_new.component';
import { DoctorsEditComponent } from './edit/doctors_edit.component';
import { DoctorsEditPasswordComponent } from './edit_password/doctors_edit_password.component';
import { LayoutModule } from '../../layouts/layout.module';
import { DefaultComponent } from '../../pages/default/default.component';
import { ValidatorsService } from '../../../_services/validators.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { Ng2TelInputModule } from 'ng2-tel-input';

const routes: Routes = [
  {
    "path": "",
    "component": DefaultComponent,
    "children": [
      {
        "path": "",
        "component": DoctorsIndexComponent
      },
      {
        "path": "new",
        "component": DoctorsNewComponent
      },
      {
        "path": ":id",
        "component": DoctorsShowComponent
      },
      {
        "path": "edit\/:id",
        "component": DoctorsEditComponent
      },
      {
        "path": "edit_password\/:id",
        "component": DoctorsEditPasswordComponent
      }
    ]
  }
];


@NgModule({imports: [
  CommonModule,
  RouterModule.forChild(routes),
  LayoutModule,
  FormsModule,
  ReactiveFormsModule,
  ToastrModule.forRoot({positionClass: 'toast-bottom-right'}),
  NgbModule,
  TranslateModule,
  Ng2TelInputModule.forRoot()
],exports: [
  RouterModule
], providers: [
  ValidatorsService
],declarations: [
  DoctorsIndexComponent,
  DoctorsShowComponent,
  DoctorsEditPasswordComponent,
  DoctorsEditComponent,
  DoctorsNewComponent
]})


export class DoctorsModule  {

}
