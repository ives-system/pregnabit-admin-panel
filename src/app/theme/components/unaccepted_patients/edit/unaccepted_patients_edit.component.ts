import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Patient } from '../../../../_models/patient';
import { DoctorService } from '../../../../_services/doctor.service';
import { UnacceptedPatientService } from '../../../../_services/unaccepted_patient.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./unaccepted_patients_edit.component.html",
  providers: [UnacceptedPatientService, DoctorService],
  encapsulation: ViewEncapsulation.None,
})
export class UnacceptedPatientsEditComponent implements OnInit {
  id: number;
  doctors: any;
  patient: Patient;
  editPatientForm: FormGroup;
  submitted = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    private doctorService: DoctorService,
    public unacceptedPatientService: UnacceptedPatientService,
    public formBuilder: FormBuilder,
    private translate: TranslateService
  ){
    this.patient = new Patient();
    this.editPatientForm = formBuilder.group({
      doctorId: ['', Validators.compose([Validators.required, Validators.pattern('^[1-9][0-9]*$')])]
    })

  }


  ngOnInit()  {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.unacceptedPatientService.show(this.id).then(response => {
        this.patient.id = response.id;
        this.patient.firstName = response.firstName;
        this.patient.lastName = response.lastName;
        this.patient.doctorId = response.matchingDoctorId;
      })
    });
    this.doctorService.list().then(response => {
      this.doctors = response;
    });
  }

  updatePatient() {
    this.submitted = true;
    if(this.editPatientForm.valid) {
      this.unacceptedPatientService.update(this.patient).then((result) => {
        this.translate.get('admin_panel.patients.form.updated').subscribe((res: string) => {
          this.toastr.success(res);
        });
        this.router.navigateByUrl('/patients/'+this.id);
      }, (err) => {
        var error = JSON.parse(err._body).messages.patient.join(', ');
        this.translate.get('admin_panel.common.error').subscribe((res: string) => {
          this.toastr.error(error, res);
        });
      });
    } else {
      console.log('invalid');
    }
  }


}
