var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var patient_1 = require('../../../../_models/patient');
var doctor_service_1 = require('../../../../_services/doctor.service');
var unaccepted_patient_service_1 = require('../../../../_services/unaccepted_patient.service');
var UnacceptedPatientsEditComponent = (function () {
    function UnacceptedPatientsEditComponent(route, router, toastr, doctorService, unacceptedPatientService, formBuilder, translate) {
        this.route = route;
        this.router = router;
        this.toastr = toastr;
        this.doctorService = doctorService;
        this.unacceptedPatientService = unacceptedPatientService;
        this.formBuilder = formBuilder;
        this.translate = translate;
        this.submitted = false;
        this.patient = new patient_1.Patient();
        this.editPatientForm = formBuilder.group({
            doctorId: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.pattern('^[1-9][0-9]*$')])]
        });
    }
    UnacceptedPatientsEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = +params['id'];
            _this.unacceptedPatientService.show(_this.id).then(function (response) {
                _this.patient.id = response.detailedPatient.id;
                _this.patient.firstName = response.detailedPatient.firstName;
                _this.patient.lastName = response.detailedPatient.lastName;
                _this.patient.doctorId = response.detailedPatient.matchingDoctorId;
            });
        });
        this.doctorService.list().then(function (response) {
            _this.doctors = response;
        });
    };
    UnacceptedPatientsEditComponent.prototype.updatePatient = function () {
        var _this = this;
        this.submitted = true;
        if (this.editPatientForm.valid) {
            this.unacceptedPatientService.update(this.patient).then(function (result) {
                _this.translate.get('admin_panel.patients.form.updated').subscribe(function (res) {
                    _this.toastr.success(res);
                });
                _this.router.navigateByUrl('/patients/' + _this.id);
            }, function (err) {
                var error = JSON.parse(err._body).messages.patient.join(', ');
                _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                    _this.toastr.error(error, res);
                });
            });
        }
        else {
            console.log('invalid');
        }
    };
    UnacceptedPatientsEditComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./unaccepted_patients_edit.component.html",
            providers: [unaccepted_patient_service_1.UnacceptedPatientService, doctor_service_1.DoctorService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], UnacceptedPatientsEditComponent);
    return UnacceptedPatientsEditComponent;
})();
exports.UnacceptedPatientsEditComponent = UnacceptedPatientsEditComponent;
//# sourceMappingURL=unaccepted_patients_edit.component.js.map