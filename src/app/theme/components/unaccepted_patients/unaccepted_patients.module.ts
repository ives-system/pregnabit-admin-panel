import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { UnacceptedPatientsIndexComponent } from './index/unaccepted_patients_index.component';
import { UnacceptedPatientsShowComponent } from './show/unaccepted_patients_show.component';
import { UnacceptedPatientsEditComponent } from './edit/unaccepted_patients_edit.component';
import { LayoutModule } from '../../layouts/layout.module';
import { DefaultComponent } from '../../pages/default/default.component';
import { ValidatorsService } from '../../../_services/validators.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';


const routes: Routes = [
  {
    "path": "",
    "component": DefaultComponent,
    "children": [
      {
        "path": "",
        "component": UnacceptedPatientsIndexComponent
      },
      {
        "path": ":id",
        "component": UnacceptedPatientsShowComponent
      },
      {
        "path": "edit\/:id",
        "component": UnacceptedPatientsEditComponent
      }
    ]
  }
];


@NgModule({imports: [
  CommonModule,
  RouterModule.forChild(routes),
  LayoutModule,
  FormsModule,
  ReactiveFormsModule,
  ToastrModule.forRoot({positionClass: 'toast-bottom-right'}),
  NgbModule,
  TranslateModule
],exports: [
  RouterModule
], providers: [
  ValidatorsService
],declarations: [
  UnacceptedPatientsIndexComponent,
  UnacceptedPatientsShowComponent,
  UnacceptedPatientsEditComponent
]})


export class UnacceptedPatientsModule  {

}
