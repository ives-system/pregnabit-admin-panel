var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var unaccepted_patient_service_1 = require('../../../../_services/unaccepted_patient.service');
var UnacceptedPatientsShowComponent = (function () {
    function UnacceptedPatientsShowComponent(route, unacceptedPatientService, toastr, router, translate) {
        this.route = route;
        this.unacceptedPatientService = unacceptedPatientService;
        this.toastr = toastr;
        this.router = router;
        this.translate = translate;
    }
    UnacceptedPatientsShowComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = +params['id'];
            _this.unacceptedPatientService.show(_this.id).then(function (response) {
                _this.patient = response.detailedPatient;
            });
        });
    };
    UnacceptedPatientsShowComponent.prototype.resendConfirmationEmail = function () {
        var _this = this;
        this.unacceptedPatientService.resendConfirmationEmail(this.id).then(function (result) {
            console.log(result);
            _this.translate.get('admin_panel.doctors.confirmation_email_sended_success').subscribe(function (res) {
                _this.toastr.success(res);
            });
            _this.router.navigateByUrl('/unaccepted_patients/' + _this.id);
        }, function (err) {
            _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                _this.toastr.error(res);
            });
        });
    };
    UnacceptedPatientsShowComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./unaccepted_patients_show.component.html",
            providers: [unaccepted_patient_service_1.UnacceptedPatientService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], UnacceptedPatientsShowComponent);
    return UnacceptedPatientsShowComponent;
})();
exports.UnacceptedPatientsShowComponent = UnacceptedPatientsShowComponent;
//# sourceMappingURL=unaccepted_patients_show.component.js.map