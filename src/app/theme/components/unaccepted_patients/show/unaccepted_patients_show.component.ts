import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { UnacceptedPatientService } from '../../../../_services/unaccepted_patient.service';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';



@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./unaccepted_patients_show.component.html",
  providers: [UnacceptedPatientService],
  encapsulation: ViewEncapsulation.None,
})
export class UnacceptedPatientsShowComponent implements OnInit {

  id: number;
  patient: any;
  constructor(
    private route: ActivatedRoute,
    public unacceptedPatientService: UnacceptedPatientService,
    private toastr: ToastrService,
    private router: Router,
    private translate: TranslateService
  )  {

  }


  ngOnInit()  {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.unacceptedPatientService.show(this.id).then(response => {
        this.patient = response;
      })
    })
  }

  resendConfirmationEmail() {
    this.unacceptedPatientService.resendConfirmationEmail(this.id).then((result) => {
      console.log(result)
      this.translate.get('admin_panel.doctors.confirmation_email_sended_success').subscribe((res: string) => {
        this.toastr.success(res);
      });
      this.router.navigateByUrl('/unaccepted_patients/'+ this.id);
    }, (err) => {
      this.translate.get('admin_panel.common.error').subscribe((res: string) => {
        this.toastr.error(res);
      });
    });
  }

}
