var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var unaccepted_patient_service_1 = require('../../../../_services/unaccepted_patient.service');
var UnacceptedPatientsIndexComponent = (function () {
    function UnacceptedPatientsIndexComponent(unacceptedPatientService) {
        this.unacceptedPatientService = unacceptedPatientService;
        this.limit = 10;
        this.query = '';
    }
    UnacceptedPatientsIndexComponent.prototype.ngOnInit = function () {
        this.loadRecords(1);
    };
    UnacceptedPatientsIndexComponent.prototype.loadRecords = function (page) {
        var _this = this;
        if (page === void 0) { page = this.pagination.offset; }
        this.unacceptedPatientService.index({
            offset: page, limit: this.limit, query: this.query, sortModel: this.sortModel,
            sortOrder: this.sortOrder
        }).then(function (response) {
            _this.pagination = response.pagination;
            _this.unacceptedPatients = response.patients;
        });
    };
    UnacceptedPatientsIndexComponent.prototype.sort = function (sortModel) {
        if (this.sortModel != sortModel) {
            this.sortOrder = 'asc';
        }
        else {
            if (this.sortOrder == 'asc') {
                this.sortOrder = 'desc';
            }
            else {
                this.sortOrder = 'asc';
            }
        }
        this.sortModel = sortModel;
        this.loadRecords();
    };
    UnacceptedPatientsIndexComponent.prototype.changeLimit = function (limit) {
        this.limit = limit;
        this.loadRecords(1);
    };
    UnacceptedPatientsIndexComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./unaccepted_patients_index.component.html",
            providers: [unaccepted_patient_service_1.UnacceptedPatientService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], UnacceptedPatientsIndexComponent);
    return UnacceptedPatientsIndexComponent;
})();
exports.UnacceptedPatientsIndexComponent = UnacceptedPatientsIndexComponent;
//# sourceMappingURL=unaccepted_patients_index.component.js.map