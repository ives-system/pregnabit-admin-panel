import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { UnacceptedPatientService } from '../../../../_services/unaccepted_patient.service';

@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./unaccepted_patients_index.component.html",
  providers: [UnacceptedPatientService],
  encapsulation: ViewEncapsulation.None,
})
export class UnacceptedPatientsIndexComponent implements OnInit {

  limit=10;
  query='';
  pagination:any;
  unacceptedPatients:any;
  sortModel:any;
  sortOrder:any;

  constructor(
    private unacceptedPatientService: UnacceptedPatientService
  )  {}

  ngOnInit()  {
    this.loadRecords(1);
  }


  loadRecords(page=this.pagination.offset) {
    this.unacceptedPatientService.index({
      offset: page, limit: this.limit, query: this.query, sortModel: this.sortModel,
      sortOrder: this.sortOrder
    }).then(response => {
      this.pagination = response.pagination;
      this.unacceptedPatients = response.patients;
    })
  }

  sort(sortModel) {
    if(this.sortModel != sortModel) {
      this.sortOrder = 'asc';
    } else {
      if(this.sortOrder == 'asc') {
        this.sortOrder = 'desc';
      } else {
        this.sortOrder = 'asc';
      }
    }
    this.sortModel = sortModel;
    this.loadRecords();
  }

  changeLimit(limit) {
    this.limit = limit;
    this.loadRecords(1);
  }
}
