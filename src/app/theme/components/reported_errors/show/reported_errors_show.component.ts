import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { ReportedErrorService } from '../../../../_services/reported_error.service';
import { TranslateService } from '@ngx-translate/core';




@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./reported_errors_show.component.html",
  providers: [ReportedErrorService],
  encapsulation: ViewEncapsulation.None,
})
export class ReportedErrorsShowComponent implements OnInit {

  id: number;
  reportedError: any;
  typesOfError = {};
  statuses = {};
  constructor(
    private route: ActivatedRoute,
    public reportedErrorService: ReportedErrorService,
    private translate: TranslateService
  )  {
    translate.onLangChange.subscribe((lang) => {
      this.loadTranslates();
    })
  }


  ngOnInit()  {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.reportedErrorService.show(this.id).then(response => {
        this.reportedError = response;
      })
    });
    this.loadTranslates();
  }

  loadTranslates() {
    this.translate.get([
        'admin_panel.reported_errors.types.device',
        'admin_panel.reported_errors.types.platform',
        'admin_panel.reported_errors.types.consultation',
        'admin_panel.reported_errors.types.other',
        'admin_panel.reported_errors.statuses.reported',
        'admin_panel.reported_errors.statuses.in_progress',
        'admin_panel.reported_errors.statuses.resolved'])
      .subscribe((res: string) => {
        this.typesOfError = {
          'device': res['admin_panel.reported_errors.types.device'],
          'platform': res['admin_panel.reported_errors.types.platform'],
          'consultation': res['admin_panel.reported_errors.types.consultation'],
          'other': res['admin_panel.reported_errors.types.other']};
        this.statuses = {
          'reported': res['admin_panel.reported_errors.statuses.reported'],
          'in_progress': res['admin_panel.reported_errors.statuses.in_progress'],
          'resolved': res['admin_panel.reported_errors.statuses.resolved']}
      });
  }

}
