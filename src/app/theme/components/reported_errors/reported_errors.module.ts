import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ReportedErrorsIndexComponent } from './index/reported_errors_index.component';
import { ReportedErrorsShowComponent } from './show/reported_errors_show.component';
import { ReportedErrorsEditComponent } from './edit/reported_errors_edit.component';
import { LayoutModule } from '../../layouts/layout.module';
import { DefaultComponent } from '../../pages/default/default.component';
import { ValidatorsService } from '../../../_services/validators.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {InputMaskModule} from 'primeng/inputmask';
import { TranslateModule } from '@ngx-translate/core';


const routes: Routes = [
  {
    "path": "",
    "component": DefaultComponent,
    "children": [
      {
        "path": "",
        "component": ReportedErrorsIndexComponent
      },
      {
        "path": ":id",
        "component": ReportedErrorsShowComponent
      },
      {
        "path": "edit\/:id",
        "component": ReportedErrorsEditComponent
      }
    ]
  }
];


@NgModule({imports: [
  CommonModule,
  RouterModule.forChild(routes),
  LayoutModule,
  FormsModule,
  ReactiveFormsModule,
  ToastrModule.forRoot({positionClass: 'toast-bottom-right'}),
  NgbModule,
  InputMaskModule,
  TranslateModule
],exports: [
  RouterModule
], providers: [
  ValidatorsService
],declarations: [
  ReportedErrorsIndexComponent,
  ReportedErrorsShowComponent,
  ReportedErrorsEditComponent
]})


export class ReportedErrorsModule  {

}
