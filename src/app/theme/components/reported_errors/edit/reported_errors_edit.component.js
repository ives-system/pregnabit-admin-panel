var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var reported_error_1 = require('../../../../_models/reported_error');
var reported_error_service_1 = require('../../../../_services/reported_error.service');
var ReportedErrorsEditComponent = (function () {
    function ReportedErrorsEditComponent(route, router, toastr, reportedErrorService, formBuilder, translate) {
        this.route = route;
        this.router = router;
        this.toastr = toastr;
        this.reportedErrorService = reportedErrorService;
        this.formBuilder = formBuilder;
        this.translate = translate;
        this.submitted = false;
        this.reportedError = new reported_error_1.ReportedError();
        this.editReportedErrorForm = formBuilder.group({
            email: [''],
            text: [''],
            dateOfIssue: [''],
            typeOfError: ['', forms_1.Validators.required],
            status: ['', forms_1.Validators.required],
            comment: [''],
            dateOfSolution: ['']
        });
    }
    ReportedErrorsEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = +params['id'];
            _this.reportedErrorService.show(_this.id).then(function (response) {
                _this.reportedError = response.detailedReportedError;
            });
        });
    };
    ReportedErrorsEditComponent.prototype.updateReportedError = function () {
        var _this = this;
        this.submitted = true;
        if (this.editReportedErrorForm.valid) {
            this.reportedErrorService.update(this.reportedError).then(function (result) {
                _this.translate.get('admin_panel.reported_errors.form.updated').subscribe(function (res) {
                    _this.toastr.success(res);
                });
                _this.router.navigateByUrl('/reported_errors/' + _this.id);
            }, function (err) {
                console.log(err);
                var error = JSON.parse(err._body).messages.reported_error.join(', ');
                _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                    _this.toastr.error(error, res);
                });
            });
        }
        else {
            console.log('invalid');
        }
    };
    ReportedErrorsEditComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./reported_errors_edit.component.html",
            providers: [reported_error_service_1.ReportedErrorService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], ReportedErrorsEditComponent);
    return ReportedErrorsEditComponent;
})();
exports.ReportedErrorsEditComponent = ReportedErrorsEditComponent;
//# sourceMappingURL=reported_errors_edit.component.js.map