import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ReportedError } from '../../../../_models/reported_error';
import { ReportedErrorService } from '../../../../_services/reported_error.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./reported_errors_edit.component.html",
  providers: [ReportedErrorService],
  encapsulation: ViewEncapsulation.None,
})
export class ReportedErrorsEditComponent implements OnInit {
  id: number;
  reportedError: ReportedError;
  editReportedErrorForm: FormGroup;
  submitted = false;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    public reportedErrorService: ReportedErrorService,
    public formBuilder: FormBuilder,
    private translate: TranslateService
  ){

    this.reportedError = new ReportedError();
    this.editReportedErrorForm = formBuilder.group({
      email: [''],
      text: [''],
      dateOfIssue: [''],
      typeOfError: ['', Validators.required],
      status: ['', Validators.required],
      comment: [''],
      dateOfSolution: ['']
    })

  }


  ngOnInit()  {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.reportedErrorService.show(this.id).then(response => {
        this.reportedError = response;
      })
    })
  }


  updateReportedError() {
    this.submitted = true;
    if(this.editReportedErrorForm.valid) {
      this.reportedErrorService.update(this.reportedError).then((result) => {
        this.translate.get('admin_panel.reported_errors.form.updated').subscribe((res: string) => {
          this.toastr.success(res);
        });
        this.router.navigateByUrl('/reported_errors/'+this.id);
      }, (err) => {
        console.log(err);
        var error = JSON.parse(err._body).messages.reported_error.join(', ');
        this.translate.get('admin_panel.common.error').subscribe((res: string) => {
          this.toastr.error(error, res);
        });
      });
    } else {
      console.log('invalid');
    }
  }


}
