import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { ReportedErrorService } from '../../../../_services/reported_error.service';
import { TranslateService } from '@ngx-translate/core';



@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./reported_errors_index.component.html",
  providers: [ReportedErrorService],
  encapsulation: ViewEncapsulation.None,
})
export class ReportedErrorsIndexComponent implements OnInit {

  limit=10;
  query='';
  pagination:any;
  reportedErrors:any;
  sortModel:any;
  sortOrder:any;
  filters = {status: '', typeOfError: ''};
  typesOfError = {};
  statuses = {};

  constructor(
    private reportedErrorService: ReportedErrorService,
    private translate: TranslateService
  )  {
    translate.onLangChange.subscribe((lang) => {
      this.loadTranslates();
    })
  }

  ngOnInit()  {
    this.loadRecords(1);
    this.loadTranslates();
  }


  loadRecords(page=this.pagination.offset) {
    this.reportedErrorService.index({
      offset: page, limit: this.limit, query: this.query,
      sortModel: this.sortModel, sortOrder: this.sortOrder,
      status: this.filters.status, typeOfError: this.filters.typeOfError
    }).then(response => {
      this.pagination = response.pagination;
      this.reportedErrors = response.reportedErrors;
    })
  }

  sort(sortModel) {
    if(this.sortModel != sortModel) {
      this.sortOrder = 'asc';
    } else {
      if(this.sortOrder == 'asc') {
        this.sortOrder = 'desc';
      } else {
        this.sortOrder = 'asc';
      }
    }
    this.sortModel = sortModel;
    this.loadRecords();
  }

  changeLimit(limit) {
    this.limit = limit;
    this.loadRecords(1);
  }

  loadTranslates() {
    this.translate.get([
        'admin_panel.reported_errors.types.device',
        'admin_panel.reported_errors.types.platform',
        'admin_panel.reported_errors.types.consultation',
        'admin_panel.reported_errors.types.other',
        'admin_panel.reported_errors.statuses.reported',
        'admin_panel.reported_errors.statuses.in_progress',
        'admin_panel.reported_errors.statuses.resolved'])
      .subscribe((res: string) => {
        this.typesOfError = {
          'device': res['admin_panel.reported_errors.types.device'],
          'platform': res['admin_panel.reported_errors.types.platform'],
          'consultation': res['admin_panel.reported_errors.types.consultation'],
          'other': res['admin_panel.reported_errors.types.other']};
        this.statuses = {
          'reported': res['admin_panel.reported_errors.statuses.reported'],
          'in_progress': res['admin_panel.reported_errors.statuses.in_progress'],
          'resolved': res['admin_panel.reported_errors.statuses.resolved']}
      });
  }


}
