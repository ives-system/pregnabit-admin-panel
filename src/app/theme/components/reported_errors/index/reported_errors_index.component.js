var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var reported_error_service_1 = require('../../../../_services/reported_error.service');
var ReportedErrorsIndexComponent = (function () {
    function ReportedErrorsIndexComponent(reportedErrorService, translate) {
        var _this = this;
        this.reportedErrorService = reportedErrorService;
        this.translate = translate;
        this.limit = 10;
        this.query = '';
        this.filters = { status: '', typeOfError: '' };
        this.typesOfError = {};
        this.statuses = {};
        translate.onLangChange.subscribe(function (lang) {
            _this.loadTranslates();
        });
    }
    ReportedErrorsIndexComponent.prototype.ngOnInit = function () {
        this.loadRecords(1);
        this.loadTranslates();
    };
    ReportedErrorsIndexComponent.prototype.loadRecords = function (page) {
        var _this = this;
        if (page === void 0) { page = this.pagination.offset; }
        this.reportedErrorService.index({
            offset: page, limit: this.limit, query: this.query,
            sortModel: this.sortModel, sortOrder: this.sortOrder,
            status: this.filters.status, typeOfError: this.filters.typeOfError
        }).then(function (response) {
            _this.pagination = response.pagination;
            _this.reportedErrors = response.reportedErrors;
        });
    };
    ReportedErrorsIndexComponent.prototype.sort = function (sortModel) {
        if (this.sortModel != sortModel) {
            this.sortOrder = 'asc';
        }
        else {
            if (this.sortOrder == 'asc') {
                this.sortOrder = 'desc';
            }
            else {
                this.sortOrder = 'asc';
            }
        }
        this.sortModel = sortModel;
        this.loadRecords();
    };
    ReportedErrorsIndexComponent.prototype.changeLimit = function (limit) {
        this.limit = limit;
        this.loadRecords(1);
    };
    ReportedErrorsIndexComponent.prototype.loadTranslates = function () {
        var _this = this;
        this.translate.get([
            'admin_panel.reported_errors.types.device',
            'admin_panel.reported_errors.types.platform',
            'admin_panel.reported_errors.types.consultation',
            'admin_panel.reported_errors.types.other',
            'admin_panel.reported_errors.statuses.reported',
            'admin_panel.reported_errors.statuses.in_progress',
            'admin_panel.reported_errors.statuses.resolved'])
            .subscribe(function (res) {
            _this.typesOfError = {
                'device': res['admin_panel.reported_errors.types.device'],
                'platform': res['admin_panel.reported_errors.types.platform'],
                'consultation': res['admin_panel.reported_errors.types.consultation'],
                'other': res['admin_panel.reported_errors.types.other'] };
            _this.statuses = {
                'reported': res['admin_panel.reported_errors.statuses.reported'],
                'in_progress': res['admin_panel.reported_errors.statuses.in_progress'],
                'resolved': res['admin_panel.reported_errors.statuses.resolved'] };
        });
    };
    ReportedErrorsIndexComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./reported_errors_index.component.html",
            providers: [reported_error_service_1.ReportedErrorService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], ReportedErrorsIndexComponent);
    return ReportedErrorsIndexComponent;
})();
exports.ReportedErrorsIndexComponent = ReportedErrorsIndexComponent;
//# sourceMappingURL=reported_errors_index.component.js.map