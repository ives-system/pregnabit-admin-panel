import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { DeviceService } from '../../../../_services/device.service';
import { changedAuditValue, newAuditValue } from '../../../../_utils/audit_helper';




@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./devices_show.component.html",
  providers: [DeviceService],
  encapsulation: ViewEncapsulation.None,
})
export class DevicesShowComponent implements OnInit {

  id: number;
  device: any;
  limit=10;
  pagination:any;
  audits:any;
  changeLimit:any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public deviceService: DeviceService
  )  {

  }


  ngOnInit()  {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.deviceService.show(this.id).then(response => {
        this.device = response;
      }, (err) => {
        this.router.navigateByUrl('/404');
      })
    });
    this.loadRecords(1);
  }

  loadRecords(page=this.pagination.offset) {
    this.deviceService.getAudits({id: this.id, limit: this.limit, offset: page}).then(response => {
      this.audits = response.audits;
      this.pagination = response.pagination;
    })
  }

  changedParameter(change) {
    return changedAuditValue(change)
  }

  newParameter(change) {
    return newAuditValue(change)
  }

}
