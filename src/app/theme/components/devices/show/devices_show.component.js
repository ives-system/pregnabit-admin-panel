var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var device_service_1 = require('../../../../_services/device.service');
var DevicesShowComponent = (function () {
    function DevicesShowComponent(route, router, deviceService) {
        this.route = route;
        this.router = router;
        this.deviceService = deviceService;
        this.limit = 10;
    }
    DevicesShowComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = +params['id'];
            _this.deviceService.show(_this.id).then(function (response) {
                _this.device = response.detailedDevice;
            }, function (err) {
                _this.router.navigateByUrl('/404');
            });
        });
        this.loadRecords(1);
    };
    DevicesShowComponent.prototype.loadRecords = function (page) {
        var _this = this;
        if (page === void 0) { page = this.pagination.offset; }
        this.deviceService.getAudits({ id: this.id, limit: this.limit, offset: page }).then(function (response) {
            _this.audits = response.audits;
            console.log(response);
            _this.pagination = response.pagination;
        });
    };
    DevicesShowComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./devices_show.component.html",
            providers: [device_service_1.DeviceService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], DevicesShowComponent);
    return DevicesShowComponent;
})();
exports.DevicesShowComponent = DevicesShowComponent;
//# sourceMappingURL=devices_show.component.js.map