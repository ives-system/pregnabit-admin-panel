import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DevicesIndexComponent } from './index/devices_index.component';
import { DevicesShowComponent } from './show/devices_show.component';
import { DevicesEditComponent } from './edit/devices_edit.component';
import { DevicesNewComponent } from './new/devices_new.component';
import { LayoutModule } from '../../layouts/layout.module';
import { DefaultComponent } from '../../pages/default/default.component';
import { ValidatorsService } from '../../../_services/validators.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';


const routes: Routes = [
  {
    "path": "",
    "component": DefaultComponent,
    "children": [
      {
        "path": "",
        "component": DevicesIndexComponent
      },
      {
        "path": "new",
        "component": DevicesNewComponent
      },
      {
        "path": ":id",
        "component": DevicesShowComponent
      },
      {
        "path": "edit\/:id",
        "component": DevicesEditComponent
      }
    ]
  }
];


@NgModule({imports: [
  CommonModule,
  RouterModule.forChild(routes),
  LayoutModule,
  FormsModule,
  ReactiveFormsModule,
  ToastrModule.forRoot({positionClass: 'toast-bottom-right'}),
  NgbModule,
  TranslateModule
],exports: [
  RouterModule
], providers: [
  ValidatorsService
],declarations: [
  DevicesIndexComponent,
  DevicesShowComponent,
  DevicesNewComponent,
  DevicesEditComponent
]})


export class DevicesModule  {

}
