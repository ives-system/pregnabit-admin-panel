var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var device_1 = require('../../../../_models/device');
var device_service_1 = require('../../../../_services/device.service');
var doctor_service_1 = require('../../../../_services/doctor.service');
var patient_service_1 = require('../../../../_services/patient.service');
var medical_unit_service_1 = require('../../../../_services/medical_unit.service');
var merchant_user_service_1 = require('../../../../_services/merchant_user.service');
var DevicesNewComponent = (function () {
    function DevicesNewComponent(router, toastr, deviceService, formBuilder, doctorService, patientService, medicalUnitService, merchantUserService, translate) {
        this.router = router;
        this.toastr = toastr;
        this.deviceService = deviceService;
        this.formBuilder = formBuilder;
        this.doctorService = doctorService;
        this.patientService = patientService;
        this.medicalUnitService = medicalUnitService;
        this.merchantUserService = merchantUserService;
        this.translate = translate;
        this.submitted = false;
        this.device = new device_1.Device();
        this.newDeviceForm = formBuilder.group({
            identifier: ['', forms_1.Validators.required],
            pin: [''],
            comments: [''],
            doctorId: [''],
            patientId: [''],
            medicalUnitId: [''],
            merchantUserId: [''],
            dateOfDeviceRelease: ['', forms_1.Validators.pattern(/^(0[1-9]|[12][0-9]|3[01])([-\/.])(0[1-9]|1[012])\2(19|20)\d\d$/)]
        });
    }
    DevicesNewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.patientService.list().then(function (response) {
            _this.patients = response;
        });
        this.doctorService.list().then(function (response) {
            _this.doctors = response;
        });
        this.medicalUnitService.list().then(function (response) {
            _this.medicalUnits = response;
        });
        this.merchantUserService.list().then(function (response) {
            _this.merchants = response;
        });
    };
    DevicesNewComponent.prototype.createDevice = function () {
        var _this = this;
        console.log(this.device);
        this.submitted = true;
        if (this.newDeviceForm.valid) {
            this.deviceService.create(this.device).then(function (result) {
                _this.translate.get('admin_panel.devices.form.added').subscribe(function (res) {
                    _this.toastr.success(res);
                });
                _this.router.navigateByUrl('/devices/' + result.detailedDevice.id);
            }, function (err) {
                var error = JSON.parse(err._body).messages.device.join(', ');
                _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                    _this.toastr.error(error, res);
                });
            });
        }
        else {
            console.log('invalid');
        }
    };
    DevicesNewComponent.prototype.chosePatient = function (patientId) {
        var _this = this;
        this.chosenPatient = this.patients[Object.keys(this.patients).find(function (k) { return _this.patients[k].id === parseInt(patientId); })];
    };
    DevicesNewComponent.prototype.choseDoctor = function (doctorId) {
        var _this = this;
        this.chosenDoctor = this.doctors[Object.keys(this.doctors).find(function (k) { return _this.doctors[k].id === parseInt(doctorId); })];
    };
    DevicesNewComponent.prototype.choseMedicalUnit = function (medicalUnitId) {
        var _this = this;
        this.chosenMedicalUnit = this.medicalUnits[Object.keys(this.medicalUnits).find(function (k) { return _this.medicalUnits[k].id === parseInt(medicalUnitId); })];
    };
    DevicesNewComponent.prototype.patientBelongsToDoctor = function () {
        if (this.chosenPatient && this.chosenDoctor && this.chosenPatient.doctorId == this.chosenDoctor.id || !(this.chosenPatient && this.chosenDoctor)) {
            return true;
        }
        else {
            return false;
        }
    };
    DevicesNewComponent.prototype.doctorBelongsToMedicalUnit = function () {
        if (this.chosenMedicalUnit && this.chosenDoctor && this.chosenDoctor.medicalUnitId == this.chosenMedicalUnit.id || !(this.chosenMedicalUnit && this.chosenDoctor)) {
            return true;
        }
        else {
            return false;
        }
    };
    DevicesNewComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./devices_new.component.html",
            providers: [device_service_1.DeviceService, doctor_service_1.DoctorService, patient_service_1.PatientService, medical_unit_service_1.MedicalUnitService, merchant_user_service_1.MerchantUserService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], DevicesNewComponent);
    return DevicesNewComponent;
})();
exports.DevicesNewComponent = DevicesNewComponent;
//# sourceMappingURL=devices_new.component.js.map