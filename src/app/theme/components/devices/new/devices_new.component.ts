import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Device } from '../../../../_models/device';
import { DeviceService } from '../../../../_services/device.service';
import { DoctorService } from '../../../../_services/doctor.service';
import { PatientService } from '../../../../_services/patient.service';
import { MedicalUnitService } from '../../../../_services/medical_unit.service';
import { MerchantUserService } from '../../../../_services/merchant_user.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./devices_new.component.html",
  providers: [DeviceService, DoctorService, PatientService, MedicalUnitService, MerchantUserService],
  encapsulation: ViewEncapsulation.None,
})

export class DevicesNewComponent implements OnInit {
  device: Device;
  patients:any;
  doctors:any;
  medicalUnits:any;
  chosenPatient:any;
  chosenDoctor:any;
  chosenMedicalUnit:any;
  newDeviceForm: FormGroup;
  submitted = false;
  merchants:any;

  constructor(
    private router: Router,
    private toastr: ToastrService,
    public deviceService: DeviceService,
    public formBuilder: FormBuilder,
    private doctorService: DoctorService,
    private patientService: PatientService,
    private medicalUnitService: MedicalUnitService,
    private merchantUserService: MerchantUserService,
    private translate: TranslateService

  ){
    this.device = new Device();
    this.newDeviceForm = formBuilder.group({
      identifier: ['', Validators.required],
      pin: [''],
      comments: [''],
      doctorId: [''],
      patientId: [''],
      medicalUnitId: [''],
      merchantUserId: [''],
      dateOfDeviceRelease: ['', Validators.pattern(/^(0[1-9]|[12][0-9]|3[01])([-\/.])(0[1-9]|1[012])\2(19|20)\d\d$/)],
    });
  }

  ngOnInit()  {
    this.patientService.list().then(response => {
      this.patients = response;
    });
    this.doctorService.list().then(response => {
      this.doctors = response;


    });
    this.medicalUnitService.list().then(response => {
      this.medicalUnits = response;
    });
    this.merchantUserService.list().then(response => {
      this.merchants = response;
    });
  }


  createDevice() {
    this.submitted = true;
    if(this.newDeviceForm.valid) {
      this.deviceService.create(this.device).then((result) => {
        this.translate.get('admin_panel.devices.form.added').subscribe((res: string) => {
          this.toastr.success(res);
        });
        this.router.navigateByUrl('/devices/'+ result.id);
      }, (err) => {
        var error = JSON.parse(err._body).messages.device.join(', ');
        this.translate.get('admin_panel.common.error').subscribe((res: string) => {
          this.toastr.error(error, res);
        });
      });
    } else {
      console.log('invalid');
    }
  }

  chosePatient(patientId) {
    this.chosenPatient = this.patients[Object.keys(this.patients).find(k => this.patients[k].id === parseInt(patientId))];
  }

  choseDoctor(doctorId) {
    this.chosenDoctor = this.doctors[Object.keys(this.doctors).find(k => this.doctors[k].id === parseInt(doctorId))];
  }

  choseMedicalUnit(medicalUnitId) {
    this.chosenMedicalUnit = this.medicalUnits[Object.keys(this.medicalUnits).find(k => this.medicalUnits[k].id === parseInt(medicalUnitId))];
  }

  patientBelongsToDoctor() {
    if(this.chosenPatient && this.chosenDoctor && this.chosenPatient.doctorId == this.chosenDoctor.id || !(this.chosenPatient && this.chosenDoctor)) {
      return true
    } else {
      return false
    }
  }

  doctorBelongsToMedicalUnit() {
    if(this.chosenMedicalUnit && this.chosenDoctor && this.chosenDoctor.medicalUnitId == this.chosenMedicalUnit.id || !(this.chosenMedicalUnit && this.chosenDoctor)) {
      return true
    } else {
      return false
    }
  }


}


