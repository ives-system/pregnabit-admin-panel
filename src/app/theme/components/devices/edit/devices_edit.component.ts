import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Device } from '../../../../_models/device';
import { DeviceService } from '../../../../_services/device.service';
import { DoctorService } from '../../../../_services/doctor.service';
import { PatientService } from '../../../../_services/patient.service';
import { MedicalUnitService } from '../../../../_services/medical_unit.service';
import { MerchantUserService } from '../../../../_services/merchant_user.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./devices_edit.component.html",
  providers: [DeviceService, DoctorService, PatientService, MedicalUnitService, MerchantUserService],
  encapsulation: ViewEncapsulation.None,
})
export class DevicesEditComponent implements OnInit {
  id: number;
  device: Device;
  deviceName:any;
  editDeviceForm: FormGroup;
  submitted = false;
  patients:any;
  patientsCollection: any;
  medicalUnitsCollection: any;
  doctorsCollection: any;
  doctors:any;
  medicalUnits:any;
  chosenPatient:any;
  chosenDoctor:any;
  chosenMedicalUnit:any;
  merchants:any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    public formBuilder: FormBuilder,
    public deviceService: DeviceService,
    private doctorService: DoctorService,
    private patientService: PatientService,
    private medicalUnitService: MedicalUnitService,
    private merchantUserService: MerchantUserService,
    private translate: TranslateService
  ) {
    this.device = new Device();
    this.editDeviceForm = formBuilder.group({
      identifier: ['', Validators.required],
      pin: [''],
      comments: [''],
      doctorId: [''],
      patientId: [''],
      medicalUnitId: [''],
      merchantUserId: [''],
      dateOfDeviceRelease: ['', Validators.pattern(/^(0[1-9]|[12][0-9]|3[01])([-\/.])(0[1-9]|1[012])\2(19|20)\d\d$/)],
    });
  }



  ngOnInit()  {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.deviceService.show(this.id).then(response => {
        this.device = response;
        this.deviceName = this.device['identifier'];
      })
    });
    this.patientService.list().then(response => {
      this.patients = response;
      this.patientsCollection = this.patients
      this.chosePatient(this.device['patientId']);
    });
    this.doctorService.list().then(response => {
      this.doctors = response;
      this.doctorsCollection = this.doctors;
      this.choseDoctor(this.device['doctorId']);
    });
    this.medicalUnitService.list().then(response => {
      this.medicalUnits = response;
      this.medicalUnitsCollection = this.medicalUnits
      this.choseMedicalUnit(this.device['medicalUnitId']);
    });
    this.merchantUserService.list().then(response => {
      this.merchants = response;
    });
  }

  updateDevice() {
    this.submitted = true;
    if(this.editDeviceForm.valid) {
      this.deviceService.update(this.device).then((result) => {
        this.translate.get('admin_panel.devices.form.updated').subscribe((res: string) => {
          this.toastr.success(res);
        });
        this.router.navigateByUrl('/devices/'+this.id);
      }, (err) => {
        console.log('Error =>', err._body)
        var error = JSON.parse(err._body).messages.device.join(', ');
        this.translate.get('admin_panel.common.error').subscribe((res: string) => {
          this.toastr.error(error, res);
        });
      });
    } else {
      console.log('invalid');
    }
  }


  chosePatient(patientId) {
    this.chosenPatient = this.patients[Object.keys(this.patients).find(k => this.patients[k].id === parseInt(patientId))];
  }

  choseDoctor(doctorId) {
    this.chosenDoctor = this.doctors[Object.keys(this.doctors).find(k => this.doctors[k].id === parseInt(doctorId))];
    this.patients = this.patientsCollection.filter(patient => patient.doctorId === this.chosenDoctor.id)

  }

  choseMedicalUnit(medicalUnitId) {
    this.chosenMedicalUnit = this.medicalUnits[Object.keys(this.medicalUnits).find(k => this.medicalUnits[k].id === parseInt(medicalUnitId))];
    this.doctors = this.doctorsCollection.filter(doctor => doctor.medicalUnitId === this.chosenMedicalUnit.id)
  }

  patientBelongsToDoctor() {
    if(this.chosenPatient && this.chosenDoctor && this.chosenPatient.doctorId == this.chosenDoctor.id || !(this.chosenPatient && this.chosenDoctor)) {
      return true
    } else {
      return false
    }
  }

  doctorBelongsToMedicalUnit() {
    if(this.chosenMedicalUnit && this.chosenDoctor && this.chosenDoctor.medicalUnitId == this.chosenMedicalUnit.id || !(this.chosenMedicalUnit && this.chosenDoctor)) {
      return true
    } else {
      return false
    }
  }
}
