var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var device_service_1 = require('../../../../_services/device.service');
var doctor_service_1 = require('../../../../_services/doctor.service');
var patient_service_1 = require('../../../../_services/patient.service');
var medical_unit_service_1 = require('../../../../_services/medical_unit.service');
var merchant_user_service_1 = require('../../../../_services/merchant_user.service');
var DevicesIndexComponent = (function () {
    function DevicesIndexComponent(deviceService, doctorService, patientService, medicalUnitService, merchantUserService) {
        this.deviceService = deviceService;
        this.doctorService = doctorService;
        this.patientService = patientService;
        this.medicalUnitService = medicalUnitService;
        this.merchantUserService = merchantUserService;
        this.limit = 10;
        this.filters = { patientId: '', doctorId: '', medicalUnitId: '', merchantUserId: '', identifier: '', pin: '' };
    }
    DevicesIndexComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadRecords(1);
        this.patientService.list().then(function (response) {
            _this.patients = response;
        });
        this.doctorService.list().then(function (response) {
            _this.doctors = response;
        });
        this.medicalUnitService.list().then(function (response) {
            _this.medicalUnits = response;
        });
        this.merchantUserService.list().then(function (response) {
            _this.merchants = response;
        });
    };
    DevicesIndexComponent.prototype.loadRecords = function (page) {
        var _this = this;
        if (page === void 0) { page = this.pagination.offset; }
        this.deviceService.index({
            offset: page, limit: this.limit, identifier: this.filters.identifier, pin: this.filters.pin,
            sortModel: this.sortModel, sortOrder: this.sortOrder, doctorId: this.filters.doctorId,
            patientId: this.filters.patientId, medicalUnitId: this.filters.medicalUnitId,
            merchantUserId: this.filters.merchantUserId
        }).then(function (response) {
            _this.pagination = response.pagination;
            _this.devices = response.devices;
        });
    };
    DevicesIndexComponent.prototype.sort = function (sortModel) {
        if (this.sortModel != sortModel) {
            this.sortOrder = 'asc';
        }
        else {
            if (this.sortOrder == 'asc') {
                this.sortOrder = 'desc';
            }
            else {
                this.sortOrder = 'asc';
            }
        }
        this.sortModel = sortModel;
        this.loadRecords();
    };
    DevicesIndexComponent.prototype.changeLimit = function (limit) {
        this.limit = limit;
        this.loadRecords(1);
    };
    DevicesIndexComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./devices_index.component.html",
            providers: [device_service_1.DeviceService, doctor_service_1.DoctorService, patient_service_1.PatientService, medical_unit_service_1.MedicalUnitService, merchant_user_service_1.MerchantUserService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], DevicesIndexComponent);
    return DevicesIndexComponent;
})();
exports.DevicesIndexComponent = DevicesIndexComponent;
//# sourceMappingURL=devices_index.component.js.map