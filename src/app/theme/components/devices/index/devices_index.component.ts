import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { DeviceService } from '../../../../_services/device.service';
import { DoctorService } from '../../../../_services/doctor.service';
import { PatientService } from '../../../../_services/patient.service';
import { MedicalUnitService } from '../../../../_services/medical_unit.service';
import { MerchantUserService } from '../../../../_services/merchant_user.service';


@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./devices_index.component.html",
  providers: [DeviceService, DoctorService, PatientService, MedicalUnitService, MerchantUserService],
  encapsulation: ViewEncapsulation.None,
})
export class DevicesIndexComponent implements OnInit {

  limit=10;
  pagination:any;
  devices:any;
  sortModel:any;
  sortOrder:any;
  patients:any;
  doctors:any;
  medicalUnits:any;
  merchants:any;
  filters = {patientId: '', doctorId: '', medicalUnitId: '', merchantUserId: '', identifier: '', pin: ''};

  constructor(
    private deviceService: DeviceService,
    private doctorService: DoctorService,
    private patientService: PatientService,
    private medicalUnitService: MedicalUnitService,
    private merchantUserService: MerchantUserService
  )  {}

  ngOnInit()  {
    this.loadRecords(1);
    this.patientService.list().then(response => {
      this.patients = response;
    });
    this.doctorService.list().then(response => {
      this.doctors = response;
    });
    this.medicalUnitService.list().then(response => {
      this.medicalUnits = response;
    });
    this.merchantUserService.list().then(response => {
      this.merchants = response;
    });
  }


  loadRecords(page=this.pagination.offset) {
    this.deviceService.index({
      offset: page, limit: this.limit, identifier: this.filters.identifier, pin: this.filters.pin,
      sortModel: this.sortModel, sortOrder: this.sortOrder, doctorId: this.filters.doctorId,
      patientId: this.filters.patientId, medicalUnitId: this.filters.medicalUnitId,
      merchantUserId: this.filters.merchantUserId
    }).then(response => {
      this.pagination = response.pagination;
      this.devices = response.devices;
      console.log(this.devices)
    })
  }

  sort(sortModel) {
    if(this.sortModel != sortModel) {
      this.sortOrder = 'asc';
    } else {
      if(this.sortOrder == 'asc') {
        this.sortOrder = 'desc';
      } else {
        this.sortOrder = 'asc';
      }
    }
    this.sortModel = sortModel;
    this.loadRecords();
  }

  changeLimit(limit) {
    this.limit = limit;
    this.loadRecords(1);
  }
}
