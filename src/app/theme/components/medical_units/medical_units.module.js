var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var ngx_toastr_1 = require('ngx-toastr');
var common_1 = require('@angular/common');
var router_1 = require('@angular/router');
var medical_units_index_component_1 = require('./index/medical_units_index.component');
var medical_units_show_component_1 = require('./show/medical_units_show.component');
var medical_units_new_component_1 = require('./new/medical_units_new.component');
var medical_units_edit_component_1 = require('./edit/medical_units_edit.component');
var medical_units_edit_password_component_1 = require('./edit_password/medical_units_edit_password.component');
var layout_module_1 = require('../../layouts/layout.module');
var default_component_1 = require('../../pages/default/default.component');
var validators_service_1 = require('../../../_services/validators.service');
var ng_bootstrap_1 = require('@ng-bootstrap/ng-bootstrap');
var core_2 = require('@ngx-translate/core');
var routes = [
    {
        "path": "",
        "component": default_component_1.DefaultComponent,
        "children": [
            {
                "path": "",
                "component": medical_units_index_component_1.MedicalUnitsIndexComponent
            },
            {
                "path": "new",
                "component": medical_units_new_component_1.MedicalUnitsNewComponent
            },
            {
                "path": ":id",
                "component": medical_units_show_component_1.MedicalUnitsShowComponent
            },
            {
                "path": "edit\/:id",
                "component": medical_units_edit_component_1.MedicalUnitsEditComponent
            },
            {
                "path": "edit_password\/:id",
                "component": medical_units_edit_password_component_1.MedicalUnitsEditPasswordComponent
            }
        ]
    }
];
var MedicalUnitsModule = (function () {
    function MedicalUnitsModule() {
    }
    MedicalUnitsModule = __decorate([
        core_1.NgModule({ imports: [
                common_1.CommonModule,
                router_1.RouterModule.forChild(routes),
                layout_module_1.LayoutModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                ngx_toastr_1.ToastrModule.forRoot({ positionClass: 'toast-bottom-right' }),
                ng_bootstrap_1.NgbModule,
                core_2.TranslateModule
            ], exports: [
                router_1.RouterModule
            ], providers: [
                validators_service_1.ValidatorsService
            ], declarations: [
                medical_units_index_component_1.MedicalUnitsIndexComponent,
                medical_units_show_component_1.MedicalUnitsShowComponent,
                medical_units_edit_component_1.MedicalUnitsEditComponent,
                medical_units_edit_password_component_1.MedicalUnitsEditPasswordComponent,
                medical_units_new_component_1.MedicalUnitsNewComponent
            ] })
    ], MedicalUnitsModule);
    return MedicalUnitsModule;
})();
exports.MedicalUnitsModule = MedicalUnitsModule;
//# sourceMappingURL=medical_units.module.js.map