var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var medical_unit_1 = require('../../../../_models/medical_unit');
var medical_unit_service_1 = require('../../../../_services/medical_unit.service');
var merchant_user_service_1 = require('../../../../_services/merchant_user.service');
var MedicalUnitsNewComponent = (function () {
    function MedicalUnitsNewComponent(router, toastr, medicalUnitService, merchantUserService, formBuilder, translate) {
        var _this = this;
        this.router = router;
        this.toastr = toastr;
        this.medicalUnitService = medicalUnitService;
        this.merchantUserService = merchantUserService;
        this.formBuilder = formBuilder;
        this.translate = translate;
        this.submitted = false;
        this.objectKeys = Object.keys;
        translate.onLangChange.subscribe(function (lang) {
            _this.loadTranslates();
        });
        this.medicalUnit = new medical_unit_1.MedicalUnit();
        this.medicalUnit['canEditPatients'] = true;
        this.medicalUnit['canEditDevices'] = true;
        this.medicalUnit['canSeeMedicalRecords'] = true;
        this.newMedicalUnitForm = formBuilder.group({
            firstName: [''],
            lastName: [''],
            email: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.email])],
            phoneNumber: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.pattern(/^[0-9]+$/)])],
            company: ['', forms_1.Validators.required],
            nip: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.pattern(/^[0-9]+$/)])],
            street: ['', forms_1.Validators.required],
            zipCode: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.maxLength(10), forms_1.Validators.pattern(/^[0-9-]+$/)])],
            city: ['', forms_1.Validators.required],
            streetNumber: ['', forms_1.Validators.required],
            apartmentNumber: ['', null],
            voivodeship: ['', forms_1.Validators.required],
            typeOfActivity: ['', forms_1.Validators.required],
            companySize: ['', forms_1.Validators.required],
            canEditPatients: [false],
            canEditDevices: [false],
            canSeeAllPatients: [false],
            canSeeMedicalRecords: [false],
            merchantId: [''],
            lang: ['', forms_1.Validators.required],
            doctorCode: ['']
        });
    }
    MedicalUnitsNewComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.merchantUserService.list().then(function (response) {
            _this.merchants = response;
        });
        this.loadTranslates();
    };
    MedicalUnitsNewComponent.prototype.createMedicalUnit = function () {
        var _this = this;
        this.submitted = true;
        if (this.newMedicalUnitForm.valid) {
            this.medicalUnitService.create(this.medicalUnit).then(function (result) {
                _this.translate.get('admin_panel.medical_units.form.added').subscribe(function (res) {
                    _this.toastr.success(res);
                });
                _this.router.navigateByUrl('/medical_units/' + result.detailedMedicalUnit.id);
            }, function (err) {
                var error = JSON.parse(err._body).messages.medical_unit.join(', ');
                _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                    _this.toastr.error(error, res);
                });
            });
        }
        else {
            console.log('invalid');
        }
    };
    MedicalUnitsNewComponent.prototype.loadTranslates = function () {
        var _this = this;
        this.translate.get([
            'admin_panel.doctors.types_of_activity.doctor',
            'admin_panel.doctors.types_of_activity.midwife',
            'admin_panel.doctors.types_of_activity.therapeutic',
            'admin_panel.doctors.types_of_activity.school_of_childbirth',
            'admin_panel.doctors.company_sizes.one',
            'admin_panel.doctors.company_sizes.up_to_ten',
            'admin_panel.doctors.company_sizes.up_to_fifty',
            'admin_panel.doctors.company_sizes.above_fifty',
            'admin_panel.doctors.voivodeships.dolnoslaskie',
            'admin_panel.doctors.voivodeships.kujawsko_pomorskie',
            'admin_panel.doctors.voivodeships.lubelskie',
            'admin_panel.doctors.voivodeships.lubuskie',
            'admin_panel.doctors.voivodeships.lodzkie',
            'admin_panel.doctors.voivodeships.malopolskie',
            'admin_panel.doctors.voivodeships.mazowieckie',
            'admin_panel.doctors.voivodeships.opolskie',
            'admin_panel.doctors.voivodeships.podkarpackie',
            'admin_panel.doctors.voivodeships.podlaskie',
            'admin_panel.doctors.voivodeships.pomorskie',
            'admin_panel.doctors.voivodeships.slaskie',
            'admin_panel.doctors.voivodeships.swietokrzyskie',
            'admin_panel.doctors.voivodeships.warminsko_mazurskie',
            'admin_panel.doctors.voivodeships.wielkopolskie',
            'admin_panel.doctors.voivodeships.zachodniopomorskie'])
            .subscribe(function (res) {
            _this.typesOfActivity = {
                'Doctor': res['admin_panel.doctors.types_of_activity.doctor'],
                'Midwife': res['admin_panel.doctors.types_of_activity.midwife'],
                'Therapeutic': res['admin_panel.doctors.types_of_activity.therapeutic'],
                'SchoolOfChildbirth': res['admin_panel.doctors.types_of_activity.school_of_childbirth'] };
            _this.companySizes = {
                'One': res['admin_panel.doctors.company_sizes.one'],
                'UpToTen': res['admin_panel.doctors.company_sizes.up_to_ten'],
                'UpToFifty': res['admin_panel.doctors.company_sizes.up_to_fifty'],
                'AboveFifty': res['admin_panel.doctors.company_sizes.above_fifty'] };
            _this.voivodeships = {
                'Dolnoslaskie': res['admin_panel.doctors.voivodeships.dolnoslaskie'],
                'KujawskoPomorskie': res['admin_panel.doctors.voivodeships.kujawsko_pomorskie'],
                'Lubelskie': res['admin_panel.doctors.voivodeships.lubelskie'],
                'Lubuskie': res['admin_panel.doctors.voivodeships.lubuskie'],
                'Lodzkie': res['admin_panel.doctors.voivodeships.lodzkie'],
                'Malopolskie': res['admin_panel.doctors.voivodeships.malopolskie'],
                'Mazowieckie': res['admin_panel.doctors.voivodeships.mazowieckie'],
                'Opolskie': res['admin_panel.doctors.voivodeships.opolskie'],
                'Podkarpackie': res['admin_panel.doctors.voivodeships.podkarpackie'],
                'Pomorskie': res['admin_panel.doctors.voivodeships.pomorskie'],
                'Podlaskie': res['admin_panel.doctors.voivodeships.podlaskie'],
                'Slaskie': res['admin_panel.doctors.voivodeships.slaskie'],
                'Swietokrzyskie': res['admin_panel.doctors.voivodeships.swietokrzyskie'],
                'WarminskoMazurskie': res['admin_panel.doctors.voivodeships.warminsko_mazurskie'],
                'Wielkopolskie': res['admin_panel.doctors.voivodeships.wielkopolskie'],
                'Zachodniopomorskie': res['admin_panel.doctors.voivodeships.zachodniopomorskie'] };
        });
    };
    MedicalUnitsNewComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./medical_units_new.component.html",
            providers: [medical_unit_service_1.MedicalUnitService, merchant_user_service_1.MerchantUserService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], MedicalUnitsNewComponent);
    return MedicalUnitsNewComponent;
})();
exports.MedicalUnitsNewComponent = MedicalUnitsNewComponent;
//# sourceMappingURL=medical_units_new.component.js.map