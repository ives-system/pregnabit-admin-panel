import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { MedicalUnitService } from '../../../../_services/medical_unit.service';
import { MerchantUserService } from '../../../../_services/merchant_user.service';
import { DeviceService } from '../../../../_services/device.service';
import { DoctorService } from '../../../../_services/doctor.service';
import { PatientService } from '../../../../_services/patient.service';


@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./medical_units_index.component.html",
  providers: [MedicalUnitService, DeviceService, DoctorService, PatientService, MerchantUserService],
  encapsulation: ViewEncapsulation.None,
})
export class MedicalUnitsIndexComponent implements OnInit {

  limit=10;
  query='';
  pagination:any;
  medicalUnits:any;
  sortModel:any;
  sortOrder:any;
  merchants:any;
  devices:any;
  doctors:any;
  patients:any;
  filters = {merchantId: '', doctorId: '', deviceId: '', patientId: ''};
  constructor(
    private medicalUnitService: MedicalUnitService,
    private deviceService: DeviceService,
    private doctorService: DoctorService,
    private patientService: PatientService,
    private merchantUserService: MerchantUserService
  )  {}

  ngOnInit()  {
    this.loadRecords(1);
    this.merchantUserService.list().then(response => {
      this.merchants = response;
    });
    this.doctorService.list().then(response => {
      this.doctors = response;
    });
    this.deviceService.list().then(response => {
      this.devices = response;
    });
    this.patientService.list().then(response => {
      this.patients = response;
    });
  }


  loadRecords(page=this.pagination.offset) {
    this.medicalUnitService.index({offset: page, limit: this.limit,
      query: this.query, sortModel: this.sortModel, sortOrder: this.sortOrder,
      merchantId: this.filters.merchantId, doctorId: this.filters.doctorId,
      deviceId: this.filters.deviceId, patientId: this.filters.patientId}).then(response => {
      this.pagination = response.pagination;
      this.medicalUnits = response.medicalUnits;
      console.log(this.medicalUnits)
    })
  }

  sort(sortModel) {
    if(this.sortModel != sortModel) {
      this.sortOrder = 'asc';
    } else {
      if(this.sortOrder == 'asc') {
        this.sortOrder = 'desc';
      } else {
        this.sortOrder = 'asc';
      }
    }
    this.sortModel = sortModel;
    this.loadRecords();
  }

  changeLimit(limit) {
    this.limit = limit;
    this.loadRecords(1);
  }


}
