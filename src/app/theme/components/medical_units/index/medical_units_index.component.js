var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var medical_unit_service_1 = require('../../../../_services/medical_unit.service');
var merchant_user_service_1 = require('../../../../_services/merchant_user.service');
var device_service_1 = require('../../../../_services/device.service');
var doctor_service_1 = require('../../../../_services/doctor.service');
var patient_service_1 = require('../../../../_services/patient.service');
var MedicalUnitsIndexComponent = (function () {
    function MedicalUnitsIndexComponent(medicalUnitService, deviceService, doctorService, patientService, merchantUserService) {
        this.medicalUnitService = medicalUnitService;
        this.deviceService = deviceService;
        this.doctorService = doctorService;
        this.patientService = patientService;
        this.merchantUserService = merchantUserService;
        this.limit = 10;
        this.query = '';
        this.filters = { merchantId: '', doctorId: '', deviceId: '', patientId: '' };
    }
    MedicalUnitsIndexComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadRecords(1);
        this.merchantUserService.list().then(function (response) {
            _this.merchants = response;
        });
        this.doctorService.list().then(function (response) {
            _this.doctors = response;
        });
        this.deviceService.list().then(function (response) {
            _this.devices = response;
        });
        this.patientService.list().then(function (response) {
            _this.patients = response;
        });
    };
    MedicalUnitsIndexComponent.prototype.loadRecords = function (page) {
        var _this = this;
        if (page === void 0) { page = this.pagination.offset; }
        this.medicalUnitService.index({ offset: page, limit: this.limit,
            query: this.query, sortModel: this.sortModel, sortOrder: this.sortOrder,
            merchantId: this.filters.merchantId, doctorId: this.filters.doctorId,
            deviceId: this.filters.deviceId, patientId: this.filters.patientId }).then(function (response) {
            _this.pagination = response.pagination;
            _this.medicalUnits = response.medicalUnits;
        });
    };
    MedicalUnitsIndexComponent.prototype.sort = function (sortModel) {
        if (this.sortModel != sortModel) {
            this.sortOrder = 'asc';
        }
        else {
            if (this.sortOrder == 'asc') {
                this.sortOrder = 'desc';
            }
            else {
                this.sortOrder = 'asc';
            }
        }
        this.sortModel = sortModel;
        this.loadRecords();
    };
    MedicalUnitsIndexComponent.prototype.changeLimit = function (limit) {
        this.limit = limit;
        this.loadRecords(1);
    };
    MedicalUnitsIndexComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./medical_units_index.component.html",
            providers: [medical_unit_service_1.MedicalUnitService, device_service_1.DeviceService, doctor_service_1.DoctorService, patient_service_1.PatientService, merchant_user_service_1.MerchantUserService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], MedicalUnitsIndexComponent);
    return MedicalUnitsIndexComponent;
})();
exports.MedicalUnitsIndexComponent = MedicalUnitsIndexComponent;
//# sourceMappingURL=medical_units_index.component.js.map