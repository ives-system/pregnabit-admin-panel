import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { MedicalUnitsIndexComponent } from './index/medical_units_index.component';
import { MedicalUnitsShowComponent } from './show/medical_units_show.component';
import { MedicalUnitsNewComponent } from './new/medical_units_new.component';
import { MedicalUnitsEditComponent } from './edit/medical_units_edit.component';
import { MedicalUnitsEditPasswordComponent } from './edit_password/medical_units_edit_password.component';
import { LayoutModule } from '../../layouts/layout.module';
import { DefaultComponent } from '../../pages/default/default.component';
import { ValidatorsService } from '../../../_services/validators.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { Ng2TelInputModule } from 'ng2-tel-input';

const routes: Routes = [
  {
    "path": "",
    "component": DefaultComponent,
    "children": [
      {
        "path": "",
        "component": MedicalUnitsIndexComponent
      },
      {
        "path": "new",
        "component": MedicalUnitsNewComponent
      },
      {
        "path": ":id",
        "component": MedicalUnitsShowComponent
      },
      {
        "path": "edit\/:id",
        "component": MedicalUnitsEditComponent
      },
      {
        "path": "edit_password\/:id",
        "component": MedicalUnitsEditPasswordComponent
      }
    ]
  }
];


@NgModule({imports: [
  CommonModule,
  RouterModule.forChild(routes),
  LayoutModule,
  FormsModule,
  ReactiveFormsModule,
  ToastrModule.forRoot({positionClass: 'toast-bottom-right'}),
  NgbModule,
  TranslateModule,
  Ng2TelInputModule.forRoot()
],exports: [
  RouterModule
], providers: [
  ValidatorsService
],declarations: [
  MedicalUnitsIndexComponent,
  MedicalUnitsShowComponent,
  MedicalUnitsEditComponent,
  MedicalUnitsEditPasswordComponent,
  MedicalUnitsNewComponent
]})


export class MedicalUnitsModule  {

}
