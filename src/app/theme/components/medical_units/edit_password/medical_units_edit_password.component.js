var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var password_validator_1 = require('../../../../_validators/password_validator');
var password_data_1 = require('../../../../_models/password_data');
var medical_unit_service_1 = require('../../../../_services/medical_unit.service');
var MedicalUnitsEditPasswordComponent = (function () {
    function MedicalUnitsEditPasswordComponent(route, router, toastr, medicalUnitService, formBuilder, translate) {
        this.route = route;
        this.router = router;
        this.toastr = toastr;
        this.medicalUnitService = medicalUnitService;
        this.formBuilder = formBuilder;
        this.translate = translate;
        this.submitted = false;
        this.data = new password_data_1.PasswordData();
        this.changePasswordForm = formBuilder.group({
            password: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(8)])],
            passwordConfirmation: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.minLength(8)])]
        }, {
            validator: password_validator_1.PasswordValidation.MatchPassword
        });
    }
    MedicalUnitsEditPasswordComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = +params['id'];
            _this.data.id = _this.id;
            _this.medicalUnitService.show(_this.id).then(function (response) {
                _this.medicalUnit = response.detailedMedicalUnit;
            });
        });
    };
    MedicalUnitsEditPasswordComponent.prototype.changePassword = function () {
        var _this = this;
        this.submitted = true;
        if (this.changePasswordForm.valid) {
            this.medicalUnitService.updatePassword(this.data).then(function (result) {
                _this.translate.get('admin_panel.admin_users.form.updated_password').subscribe(function (res) {
                    _this.toastr.success(res);
                });
                _this.router.navigateByUrl('/medical_units/' + _this.id);
            }, function (err) {
                _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                    _this.toastr.error(res);
                });
            });
        }
        else {
            console.log('invalid');
        }
    };
    MedicalUnitsEditPasswordComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./medical_units_edit_password.component.html",
            providers: [medical_unit_service_1.MedicalUnitService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], MedicalUnitsEditPasswordComponent);
    return MedicalUnitsEditPasswordComponent;
})();
exports.MedicalUnitsEditPasswordComponent = MedicalUnitsEditPasswordComponent;
//# sourceMappingURL=medical_units_edit_password.component.js.map