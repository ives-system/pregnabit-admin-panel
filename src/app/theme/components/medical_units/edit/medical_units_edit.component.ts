import {Component, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MedicalUnit } from '../../../../_models/medical_unit';
import { MedicalUnitService } from '../../../../_services/medical_unit.service';
import { MerchantUserService } from '../../../../_services/merchant_user.service';
import { TranslateService } from '@ngx-translate/core';
import { Ng2TelInput } from "ng2-tel-input";

@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./medical_units_edit.component.html",
  providers: [MedicalUnitService, MerchantUserService],
  encapsulation: ViewEncapsulation.None,
})
export class MedicalUnitsEditComponent implements OnInit {
  id: number;
  medicalUnit: MedicalUnit;
  medicalUnitName:any;
  editMedicalUnitForm: FormGroup;
  submitted = false;
  typesOfActivity: any;
  companySizes: any;
  merchants: any;
  objectKeys = Object.keys;

  @ViewChild(Ng2TelInput) phoneNumber: Ng2TelInput;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    public medicalUnitService: MedicalUnitService,
    public merchantUserService: MerchantUserService,
    public formBuilder: FormBuilder,
    private translate: TranslateService
  ){
    translate.onLangChange.subscribe((lang) => {
      this.loadTranslates();
    });
    this.medicalUnit = new MedicalUnit();
    this.editMedicalUnitForm = formBuilder.group({
      firstName: [''],
      lastName: [''],
      email: ['', Validators.compose([Validators.required,Validators.email])],
      phoneNumber: [''],
      company: ['', Validators.required],
      nip: ['', Validators.compose([Validators.required, Validators.maxLength(30), Validators.pattern(/^[0-9a-zA-Z-_]+$/)])],
      street: ['', Validators.required],
      zipCode: ['', Validators.compose([Validators.required, Validators.maxLength(10) ,Validators.pattern(/^[0-9A-Za-z-]+$/)])],
      city: ['', Validators.required],
      streetNumber: ['', Validators.required],
      apartmentNumber: ['', null],
      typeOfActivity: [''],
      companySize: [''],
      canEditPatients: [''],
      canEditDevices: [''],
      canSeeAllPatients: [''],
      canSeeMedicalRecords: [''],
      merchantId: [''],
      lang: ['', Validators.required],
      doctorCode: ['']
    });
  }



  ngOnInit()  {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.medicalUnitService.show(this.id).then(response => {
        this.medicalUnit = response;
        this.medicalUnitName = this.medicalUnit['company'];
        this.phoneNumber.ngTelInput.intlTelInput('setNumber', this.medicalUnit['phoneNumber']);
      })
    });
    this.merchantUserService.list().then(response => {
      this.merchants = response;
    });
    this.loadTranslates();
  }

  updateMedicalUnit() {
    this.submitted = true;
    if(this.editMedicalUnitForm.valid) {
      this.medicalUnitService.update(this.medicalUnit).then((result) => {
        this.translate.get('admin_panel.medical_units.form.updated').subscribe((res: string) => {
          this.toastr.success(res);
        });
        this.router.navigateByUrl('/medical_units/'+this.id);
      }, (err) => {
        var error = JSON.parse(err._body).messages.medical_unit.join(', ');
        this.translate.get('admin_panel.common.error').subscribe((res: string) => {
          this.toastr.error(error, res);
        });
      });
    } else {
      console.log('invalid');
    }
  }

  loadTranslates() {
    this.translate.get([
        'admin_panel.doctors.types_of_activity.doctor',
        'admin_panel.doctors.types_of_activity.midwife',
        'admin_panel.doctors.types_of_activity.therapeutic',
        'admin_panel.doctors.types_of_activity.school_of_childbirth',
        'admin_panel.doctors.company_sizes.one',
        'admin_panel.doctors.company_sizes.up_to_ten',
        'admin_panel.doctors.company_sizes.up_to_fifty',
        'admin_panel.doctors.company_sizes.above_fifty'])
      .subscribe((res: string) => {
        this.typesOfActivity = {
          'Doctor': res['admin_panel.doctors.types_of_activity.doctor'],
          'Midwife': res['admin_panel.doctors.types_of_activity.midwife'],
          'Therapeutic': res['admin_panel.doctors.types_of_activity.therapeutic'],
          'SchoolOfChildbirth': res['admin_panel.doctors.types_of_activity.school_of_childbirth']};
        this.companySizes = {
          'One': res['admin_panel.doctors.company_sizes.one'],
          'UpToTen': res['admin_panel.doctors.company_sizes.up_to_ten'],
          'UpToFifty': res['admin_panel.doctors.company_sizes.up_to_fifty'],
          'AboveFifty': res['admin_panel.doctors.company_sizes.above_fifty']};
      });
  }

  telInputObject(obj) {
  }

  hasError: boolean;
  onError(obj) {
    this.hasError = obj;
  }

  getNumber(obj) {
    this.medicalUnit['phoneNumber'] = obj;
  }

  onCountryChange(obj) {
  }
}
