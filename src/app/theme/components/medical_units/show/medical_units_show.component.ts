import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { MedicalUnitService } from '../../../../_services/medical_unit.service';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { changedAuditValue, newAuditValue } from '../../../../_utils/audit_helper';




@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./medical_units_show.component.html",
  providers: [MedicalUnitService],
  encapsulation: ViewEncapsulation.None,
})
export class MedicalUnitsShowComponent implements OnInit {

  id: number;
  medicalUnit: any;
  limit=10;
  pagination:any;
  audits:any;
  typesOfActivity:any;
  companySizes:any;
  changeLimit:any;


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    public medicalUnitService: MedicalUnitService,
    private translate: TranslateService,
    private toastr: ToastrService
  )  {
    translate.onLangChange.subscribe((lang) => {
      this.loadTranslates();
    })
  }


  ngOnInit()  {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.medicalUnitService.show(this.id).then(response => {
        this.medicalUnit = response;
        console.log('medicalunit', response)
      }, (err) => {
        this.router.navigateByUrl('/404');
      });
      this.loadRecords(1);
      this.loadTranslates();
    })
  }

  changedParameter(change) {
    return changedAuditValue(change)
  }

  newParameter(change) {
    return newAuditValue(change)
  }

  resendConfirmationEmail() {
    this.medicalUnitService.resendConfirmationEmail(this.id).then((result) => {
      console.log(result)
      this.translate.get('admin_panel.doctors.confirmation_email_sended_success').subscribe((res: string) => {
        this.toastr.success(res);
      });
      this.router.navigateByUrl('/medical_units/'+ this.id);
    }, (err) => {
      this.translate.get('admin_panel.common.error').subscribe((res: string) => {
        this.toastr.error(res);
      });
    });
  }

  getCrmStatus() {
    this.medicalUnitService.getCrmStatus(this.id).then((result) => {
      console.log(result.state);
      this.translate.get('admin_panel.doctors.crm_status_taken_success').subscribe((res: string) => {
        this.toastr.success(res + result.state);
      });
      this.router.navigateByUrl('/medical_units/'+ this.id);
    }, (err) => {
      this.translate.get('admin_panel.common.error').subscribe((res: string) => {
        this.toastr.error(res);
      });
    });

  }

  loadRecords(page=this.pagination.offset) {
    this.medicalUnitService.getAudits({id: this.id, limit: this.limit, offset: page}).then(response => {
      this.audits = response.audits;
      this.pagination = response.pagination;
    })
  }

  loadTranslates() {
    this.translate.get([
        'admin_panel.doctors.types_of_activity.doctor',
        'admin_panel.doctors.types_of_activity.midwife',
        'admin_panel.doctors.types_of_activity.therapeutic',
        'admin_panel.doctors.types_of_activity.school_of_childbirth',
        'admin_panel.doctors.company_sizes.one',
        'admin_panel.doctors.company_sizes.up_to_ten',
        'admin_panel.doctors.company_sizes.up_to_fifty',
        'admin_panel.doctors.company_sizes.above_fifty'])
      .subscribe((res: string) => {
        this.typesOfActivity = {
          'Doctor': res['admin_panel.doctors.types_of_activity.doctor'],
          'Midwife': res['admin_panel.doctors.types_of_activity.midwife'],
          'Therapeutic': res['admin_panel.doctors.types_of_activity.therapeutic'],
          'SchoolOfChildbirth': res['admin_panel.doctors.types_of_activity.school_of_childbirth']};
        this.companySizes = {
          'One': res['admin_panel.doctors.company_sizes.one'],
          'UpToTen': res['admin_panel.doctors.company_sizes.up_to_ten'],
          'UpToFifty': res['admin_panel.doctors.company_sizes.up_to_fifty'],
          'AboveFifty': res['admin_panel.doctors.company_sizes.above_fifty']};
      });
  }

}
