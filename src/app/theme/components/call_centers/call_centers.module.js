var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var ngx_toastr_1 = require('ngx-toastr');
var common_1 = require('@angular/common');
var router_1 = require('@angular/router');
var call_centers_index_component_1 = require('./index/call_centers_index.component');
var call_centers_new_component_1 = require('./new/call_centers_new.component');
var call_centers_show_component_1 = require('./show/call_centers_show.component');
var call_centers_edit_component_1 = require('./edit/call_centers_edit.component');
var call_centers_edit_password_component_1 = require('./edit_password/call_centers_edit_password.component');
var layout_module_1 = require('../../layouts/layout.module');
var default_component_1 = require('../../pages/default/default.component');
var validators_service_1 = require('../../../_services/validators.service');
var ng_bootstrap_1 = require('@ng-bootstrap/ng-bootstrap');
var core_2 = require('@ngx-translate/core');
var routes = [
    {
        "path": "",
        "component": default_component_1.DefaultComponent,
        "children": [
            {
                "path": "",
                "component": call_centers_index_component_1.CallCentersIndexComponent
            },
            {
                "path": "new",
                "component": call_centers_new_component_1.CallCentersNewComponent
            },
            {
                "path": ":id",
                "component": call_centers_show_component_1.CallCentersShowComponent
            },
            {
                "path": "edit\/:id",
                "component": call_centers_edit_component_1.CallCentersEditComponent
            },
            {
                "path": "edit_password\/:id",
                "component": call_centers_edit_password_component_1.CallCentersEditPasswordComponent
            }
        ]
    }
];
var CallCentersModule = (function () {
    function CallCentersModule() {
    }
    CallCentersModule = __decorate([
        core_1.NgModule({ imports: [
                common_1.CommonModule,
                router_1.RouterModule.forChild(routes),
                layout_module_1.LayoutModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                ngx_toastr_1.ToastrModule.forRoot({ positionClass: 'toast-bottom-right' }),
                ng_bootstrap_1.NgbModule,
                core_2.TranslateModule
            ], exports: [
                router_1.RouterModule
            ], providers: [
                validators_service_1.ValidatorsService
            ], declarations: [
                call_centers_index_component_1.CallCentersIndexComponent,
                call_centers_show_component_1.CallCentersShowComponent,
                call_centers_edit_component_1.CallCentersEditComponent,
                call_centers_new_component_1.CallCentersNewComponent,
                call_centers_edit_password_component_1.CallCentersEditPasswordComponent
            ] })
    ], CallCentersModule);
    return CallCentersModule;
})();
exports.CallCentersModule = CallCentersModule;
//# sourceMappingURL=call_centers.module.js.map