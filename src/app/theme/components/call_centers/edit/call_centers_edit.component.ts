import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CallCenter } from '../../../../_models/call_center';
import { CallCenterService } from '../../../../_services/call_center.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./call_centers_edit.component.html",
  providers: [CallCenterService],
  encapsulation: ViewEncapsulation.None,
})
export class CallCentersEditComponent implements OnInit {
  id: number;
  callCenter: CallCenter;
  callCenterName:any;
  editCallCenterForm: FormGroup;
  submitted = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    public callCenterService: CallCenterService,
    public formBuilder: FormBuilder,
    private translate: TranslateService
  ){
    this.callCenter = new CallCenter();
    this.editCallCenterForm = formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.compose([Validators.required,Validators.email])],
      phoneNumber: ['', Validators.required],
      licence: [''],
      lang: ['', Validators.required]
    })

  }


  ngOnInit()  {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.callCenterService.show(this.id).then(response => {
        this.callCenter = response;
        this.callCenterName = this.callCenter['name'];
      })
    })
  }

  updateCallCenter() {
    this.submitted = true;
    if(this.editCallCenterForm.valid) {
      this.callCenterService.update(this.callCenter).then((result) => {
        this.translate.get('admin_panel.call_centers.form.updated').subscribe((res: string) => {
          this.toastr.success(res);
        });
        this.router.navigateByUrl('/call_centers/'+this.id);
      }, (err) => {
        var error = JSON.parse(err._body).messages.call_center.join(', ');
        this.translate.get('admin_panel.common.error').subscribe((res: string) => {
          this.toastr.error(error, res);
        });
      });
    } else {
      console.log('invalid');
    }
  }


}
