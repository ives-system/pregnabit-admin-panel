import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { CallCenterService } from '../../../../_services/call_center.service';



@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./call_centers_index.component.html",
  providers: [CallCenterService],
  encapsulation: ViewEncapsulation.None,
})
export class CallCentersIndexComponent implements OnInit {

  limit=10;
  query='';
  pagination:any;
  callCenters:any;
  sortModel:any;
  sortOrder:any;
  constructor(
    private callCenterService: CallCenterService
  )  {}

  ngOnInit()  {
    this.loadRecords(1);
  }


  loadRecords(page=this.pagination.offset) {
    this.callCenterService.index({offset: page, limit: this.limit, query: this.query, sortModel: this.sortModel, sortOrder: this.sortOrder}).then(response => {
      this.pagination = response.pagination;
      this.callCenters = response.callCenters;
    })
  }

  sort(sortModel) {
    if(this.sortModel != sortModel) {
      this.sortOrder = 'asc';
    } else {
      if(this.sortOrder == 'asc') {
        this.sortOrder = 'desc';
      } else {
        this.sortOrder = 'asc';
      }
    }
    this.sortModel = sortModel;
    this.loadRecords();
  }

  changeLimit(limit) {
    this.limit = limit;
    this.loadRecords(1);
  }


}
