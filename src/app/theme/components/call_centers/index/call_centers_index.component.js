var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var call_center_service_1 = require('../../../../_services/call_center.service');
var CallCentersIndexComponent = (function () {
    function CallCentersIndexComponent(callCenterService) {
        this.callCenterService = callCenterService;
        this.limit = 10;
        this.query = '';
    }
    CallCentersIndexComponent.prototype.ngOnInit = function () {
        this.loadRecords(1);
    };
    CallCentersIndexComponent.prototype.loadRecords = function (page) {
        var _this = this;
        if (page === void 0) { page = this.pagination.offset; }
        this.callCenterService.index({ offset: page, limit: this.limit, query: this.query, sortModel: this.sortModel, sortOrder: this.sortOrder }).then(function (response) {
            _this.pagination = response.pagination;
            _this.callCenters = response.callCenters;
        });
    };
    CallCentersIndexComponent.prototype.sort = function (sortModel) {
        if (this.sortModel != sortModel) {
            this.sortOrder = 'asc';
        }
        else {
            if (this.sortOrder == 'asc') {
                this.sortOrder = 'desc';
            }
            else {
                this.sortOrder = 'asc';
            }
        }
        this.sortModel = sortModel;
        this.loadRecords();
    };
    CallCentersIndexComponent.prototype.changeLimit = function (limit) {
        this.limit = limit;
        this.loadRecords(1);
    };
    CallCentersIndexComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./call_centers_index.component.html",
            providers: [call_center_service_1.CallCenterService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], CallCentersIndexComponent);
    return CallCentersIndexComponent;
})();
exports.CallCentersIndexComponent = CallCentersIndexComponent;
//# sourceMappingURL=call_centers_index.component.js.map