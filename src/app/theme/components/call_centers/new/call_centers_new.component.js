var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var call_center_1 = require('../../../../_models/call_center');
var call_center_service_1 = require('../../../../_services/call_center.service');
var CallCentersNewComponent = (function () {
    function CallCentersNewComponent(router, toastr, callCenterService, formBuilder, translate) {
        this.router = router;
        this.toastr = toastr;
        this.callCenterService = callCenterService;
        this.formBuilder = formBuilder;
        this.translate = translate;
        this.submitted = false;
        this.callCenter = new call_center_1.CallCenter();
        this.newCallCenterForm = formBuilder.group({
            firstName: ['', forms_1.Validators.required],
            lastName: ['', forms_1.Validators.required],
            email: ['', forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.email])],
            phoneNumber: ['', forms_1.Validators.required],
            licence: ['', forms_1.Validators.required],
            lang: ['', forms_1.Validators.required]
        });
    }
    CallCentersNewComponent.prototype.createCallCenter = function () {
        var _this = this;
        this.submitted = true;
        if (this.newCallCenterForm.valid) {
            this.callCenterService.create(this.callCenter).then(function (result) {
                _this.translate.get('admin_panel.call_centers.form.added').subscribe(function (res) {
                    _this.toastr.success(res);
                });
                _this.router.navigateByUrl('/call_centers/' + result.detailedCallCenter.id);
            }, function (err) {
                var error = JSON.parse(err._body).messages.call_center.join(', ');
                _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                    _this.toastr.error(error, res);
                });
            });
        }
        else {
            console.log('invalid');
        }
    };
    CallCentersNewComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./call_centers_new.component.html",
            providers: [call_center_service_1.CallCenterService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], CallCentersNewComponent);
    return CallCentersNewComponent;
})();
exports.CallCentersNewComponent = CallCentersNewComponent;
//# sourceMappingURL=call_centers_new.component.js.map