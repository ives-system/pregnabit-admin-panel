import { Component, ViewEncapsulation } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CallCenter } from '../../../../_models/call_center';
import { CallCenterService } from '../../../../_services/call_center.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./call_centers_new.component.html",
  providers: [CallCenterService],
  encapsulation: ViewEncapsulation.None,
})
export class CallCentersNewComponent {
  callCenter: CallCenter;
  newCallCenterForm: FormGroup;
  submitted = false;

  constructor(
    private router: Router,
    private toastr: ToastrService,
    public callCenterService: CallCenterService,
    public formBuilder: FormBuilder,
    private translate: TranslateService

  ){
    this.callCenter = new CallCenter();
    this.newCallCenterForm = formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.compose([Validators.required,Validators.email])],
      phoneNumber: ['', Validators.required],
      licence: [''],
      lang: ['', Validators.required]
    });
  }


  createCallCenter() {
    this.submitted = true;
    if(this.newCallCenterForm.valid) {
      this.callCenterService.create(this.callCenter).then((result) => {
        this.translate.get('admin_panel.call_centers.form.added').subscribe((res: string) => {
          this.toastr.success(res);
        });
        this.router.navigateByUrl('/call_centers/'+ result.id);
      }, (err) => {
        var error = JSON.parse(err._body).messages.call_center.join(', ');
        this.translate.get('admin_panel.common.error').subscribe((res: string) => {
          this.toastr.error(error, res);
        });
      });
    } else {
      console.log('invalid');
    }
  }


}
