import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CallCentersIndexComponent } from './index/call_centers_index.component';
import { CallCentersNewComponent } from './new/call_centers_new.component';
import { CallCentersShowComponent } from './show/call_centers_show.component';
import { CallCentersEditComponent } from './edit/call_centers_edit.component';
import { CallCentersEditPasswordComponent } from './edit_password/call_centers_edit_password.component';
import { LayoutModule } from '../../layouts/layout.module';
import { DefaultComponent } from '../../pages/default/default.component';
import { ValidatorsService } from '../../../_services/validators.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';


const routes: Routes = [
  {
    "path": "",
    "component": DefaultComponent,
    "children": [
      {
        "path": "",
        "component": CallCentersIndexComponent
      },
      {
        "path": "new",
        "component": CallCentersNewComponent
      },
      {
        "path": ":id",
        "component": CallCentersShowComponent
      },
      {
        "path": "edit\/:id",
        "component": CallCentersEditComponent
      },
      {
        "path": "edit_password\/:id",
        "component": CallCentersEditPasswordComponent
      }
    ]
  }
];


@NgModule({imports: [
  CommonModule,
  RouterModule.forChild(routes),
  LayoutModule,
  FormsModule,
  ReactiveFormsModule,
  ToastrModule.forRoot({positionClass: 'toast-bottom-right'}),
  NgbModule,
  TranslateModule
],exports: [
  RouterModule
], providers: [
  ValidatorsService
],declarations: [
  CallCentersIndexComponent,
  CallCentersShowComponent,
  CallCentersEditComponent,
  CallCentersNewComponent,
  CallCentersEditPasswordComponent
]})


export class CallCentersModule  {

}
