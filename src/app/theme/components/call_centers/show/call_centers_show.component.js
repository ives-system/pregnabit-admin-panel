var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var call_center_service_1 = require('../../../../_services/call_center.service');
var CallCentersShowComponent = (function () {
    function CallCentersShowComponent(route, callCenterService, translate, router, toastr) {
        this.route = route;
        this.callCenterService = callCenterService;
        this.translate = translate;
        this.router = router;
        this.toastr = toastr;
        this.limit = 10;
    }
    CallCentersShowComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = +params['id'];
            _this.callCenterService.show(_this.id).then(function (response) {
                _this.callCenter = response.detailedCallCenter;
            });
            _this.loadRecords(1);
        });
    };
    CallCentersShowComponent.prototype.resendConfirmationEmail = function () {
        var _this = this;
        this.callCenterService.resendConfirmationEmail(this.id).then(function (result) {
            console.log(result);
            _this.translate.get('admin_panel.doctors.confirmation_email_sended_success').subscribe(function (res) {
                _this.toastr.success(res);
            });
            _this.router.navigateByUrl('/call_centers/' + _this.id);
        }, function (err) {
            _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                _this.toastr.error(res);
            });
        });
    };
    CallCentersShowComponent.prototype.loadRecords = function (page) {
        var _this = this;
        if (page === void 0) { page = this.pagination.offset; }
        this.callCenterService.getAudits({ id: this.id, limit: this.limit, offset: page }).then(function (response) {
            _this.audits = response.audits;
            _this.pagination = response.pagination;
        });
    };
    CallCentersShowComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./call_centers_show.component.html",
            providers: [call_center_service_1.CallCenterService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], CallCentersShowComponent);
    return CallCentersShowComponent;
})();
exports.CallCentersShowComponent = CallCentersShowComponent;
//# sourceMappingURL=call_centers_show.component.js.map