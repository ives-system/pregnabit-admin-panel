import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { CallCenterService } from '../../../../_services/call_center.service';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';




@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./call_centers_show.component.html",
  providers: [CallCenterService],
  encapsulation: ViewEncapsulation.None,
})
export class CallCentersShowComponent implements OnInit {

  id: number;
  callCenter: any;
  limit=10;
  pagination:any;
  audits:any;
  changeLimit:any;

  constructor(
    private route: ActivatedRoute,
    public callCenterService: CallCenterService,
    private translate: TranslateService,
    private router: Router,
    private toastr: ToastrService
  )  {

  }


  ngOnInit()  {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.callCenterService.show(this.id).then(response => {
        this.callCenter = response;
      });
      this.loadRecords(1);
    })
  }

  changedParameter(change) {
    if (Array.isArray(change[1]) && change[1].length > 1) {
      return change[1][0]
    } else {
      return change[1]
    }
  }

  newParameter(change) {
    if (Array.isArray(change[1]) && change[1].length > 1) {
      return change[1][1]
    } else {
      return change[1]
    }
  }

  resendConfirmationEmail() {
    this.callCenterService.resendConfirmationEmail(this.id).then((result) => {
      console.log(result)
      this.translate.get('admin_panel.doctors.confirmation_email_sended_success').subscribe((res: string) => {
        this.toastr.success(res);
      });
      this.router.navigateByUrl('/call_centers/'+ this.id);
    }, (err) => {
      this.translate.get('admin_panel.common.error').subscribe((res: string) => {
        this.toastr.error(res);
      });
    });
  }

  loadRecords(page=this.pagination.offset) {
    this.callCenterService.getAudits({id: this.id, limit: this.limit, offset: page}).then(response => {
      this.audits = response.audits;
      console.log('audits', this.audits)
      this.pagination = response.pagination;
    })
  }

}
