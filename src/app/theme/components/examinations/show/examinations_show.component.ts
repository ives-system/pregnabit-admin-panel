import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { ExaminationService } from '../../../../_services/examination.service';
import { Chart } from 'chart.js';
import { PregnabitChart } from '../../../../_services/pregnabit_chart';
import 'rxjs/Rx' ;
import { saveAs } from 'file-saver';
import  {FetalMovementsChartDirective } from '../../../../_directives/fetal_movements_chart.directive'
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';
import {Md5} from 'ts-md5/dist/md5'
import { environment } from '../../../../../environments/environment';
import {changedAuditValue, newAuditValue} from '../../../../_utils/audit_helper';


let rootUrl = environment.url;



@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./examinations_show.component.html",
  styleUrls: ['./styles.scss'],
  providers: [ExaminationService, PregnabitChart],
  encapsulation: ViewEncapsulation.None,
})
export class ExaminationsShowComponent implements OnInit {

  id: number;
  examination: any;
  comments:any;
  descriptions:any;
  chartImg = {};
  chartHR: any;
  chartCramps: any;
  extended = false;
  fma:any;
  marginLeft:any;
  marginRight:any;
  hrLineOn = true;
  chartHrOn = true;
  hrArray:any;
  states = {};
  currentUser: any
  limit=10;
  pagination:any;
  audits:any;
  changeLimit:any;

  constructor(
    private route: ActivatedRoute,
    public examinationService: ExaminationService,
    private toastr: ToastrService,
    private translate: TranslateService
  )  {
    translate.onLangChange.subscribe((lang) => {
      this.loadTranslates();
    })
  }


  ngOnInit()  {
    this.loadTranslates()
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'))
    console.log(this.currentUser)
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.examinationService.show(this.id).then(response => {
        this.examination = response;

        this.hrArray = response.heartRatesArray.hr;
        this.renderCharts();
      });
      this.examinationService.getDescriptions(this.id).then(response => {
        this.descriptions = response;
      });
      this.examinationService.getComments(this.id).then(response => {
        this.comments = response;
      })
    })
    this.loadRecords(1)
  }


  renderCharts() {
    let chartHr : any = document.getElementById('chart-hr');
    let chartCr : any = document.getElementById('chart-cramps');
    chartHr.width = 1000;
    chartCr.width = 1000;
    chartHr.height = 396;
    chartCr.height = 288;
    this.chartCramps =
      new PregnabitChart().chartCramps(this.examination.crampArray, this.chartImg);
    this.chartHR =
      new PregnabitChart().chartHeartRate(this.examination.heartRatesArray, this.chartImg, 1,this.hrLineOn);
    this.completeChartInfo()

  }

  completeChartInfo() {
    this.marginLeft = parseInt(this.chartHR.scales['y-axis-1'].width);
    this.marginRight = parseInt(this.chartHR.scales['x-axis-1'].margins.right);
  }


  rerenderCharts() {
    this.chartHR.render(0,true);
    this.chartCramps.render(0,true);
  }

  chartsReady() {
    return !!(this.chartImg['hr'] && this.chartImg['cramps'])
  }

  fetalMovementsCallback(img) {
    this.chartImg['fetal'] = img;

  }

  switchHrChart() {
    if(this.chartHrOn) {
      if(this.hrLineOn) {
        this.hrLineOn = false;
      } else {
        this.chartHrOn = false;
        this.examination.heartRatesArray.hr = []
      }
    } else {
      this.chartHrOn = true;
      this.hrLineOn = true;
      this.examination.heartRatesArray.hr = this.hrArray
    }
    this.renderCharts();
  }

  downloadFile(file) {
    let fileExtension = '.csv';
    if(file == 'ctg_input') {
      fileExtension = '.raw';
    };
    this.examinationService.downloadFile({id: this.id, file: file}).then(response => {
      saveAs(new Blob([response], { type: "text/csv" }), file+fileExtension);
    })
  }

  downloadRaw() {
    const md5 = new Md5();
    var hash = md5.appendStr(this.examination.createdAt).end();
    window.open(rootUrl+'/download_raw_data?id='+ this.id +'&hash=' +hash)
  }

  deleteComment(comment) {
    this.examinationService.deleteComment({id: this.id, comment_id: comment.id})
      .then(response => {
        if(response.status == 'success') {
          this.comments.splice(this.comments.indexOf(comment), 1);
          this.translate.get('admin_panel.examinations.destroyed_comment').subscribe((res: string) => {
            this.toastr.success(res);
          });
        } else {
          this.translate.get('admin_panel.common.error').subscribe((res: string) => {
            this.toastr.error(res);
          });
        }
    })
  }

  deleteDescription(description) {
    this.examinationService.deleteDescription({id: this.id, description_id: description.id})
      .then(response => {
        console.log(response)
        if(response.status == 'success') {
          this.descriptions.splice(this.descriptions.indexOf(description), 1);
          this.translate.get('admin_panel.examinations.destroyed_description').subscribe((res: string) => {
            this.toastr.success(res);
          });
        } else {
          this.translate.get('admin_panel.common.error').subscribe((res: string) => {
            this.toastr.error(res);
          });
        }
      })
  }

  loadTranslates() {
    this.translate.get([
        'admin_panel.examinations.states.good',
        'admin_panel.examinations.states.repeat',
        'admin_panel.examinations.states.not_checked'])
      .subscribe((res: string) => {
        this.states = {
          'good': res['admin_panel.examinations.states.good'],
          'repeat': res['admin_panel.examinations.states.repeat'],
          'not_checked': res['admin_panel.examinations.states.not_checked']}
      });
  }

  loadRecords(page=this.pagination.offset) {
    this.examinationService.getAudits({id: this.id, limit: this.limit, offset: page}).then(response => {
      console.log(response)
      this.audits = response.audits;
      this.pagination = response.pagination;
    })
  }

  changedParameter(change) {
    return changedAuditValue(change)
  }

  newParameter(change) {
    return newAuditValue(change)
  }

}
