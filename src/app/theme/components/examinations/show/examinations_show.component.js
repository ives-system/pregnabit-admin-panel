var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var examination_service_1 = require('../../../../_services/examination.service');
var pregnabit_chart_1 = require('../../../../_services/pregnabit_chart');
require('rxjs/Rx');
var file_saver_1 = require('file-saver');
var md5_1 = require('ts-md5/dist/md5');
var environment_1 = require('../../../../../environments/environment');
var rootUrl = environment_1.environment.url;
var ExaminationsShowComponent = (function () {
    function ExaminationsShowComponent(route, examinationService, toastr, translate) {
        var _this = this;
        this.route = route;
        this.examinationService = examinationService;
        this.toastr = toastr;
        this.translate = translate;
        this.chartImg = {};
        this.extended = false;
        this.hrLineOn = true;
        this.chartHrOn = true;
        this.states = {};
        translate.onLangChange.subscribe(function (lang) {
            _this.loadTranslates();
        });
    }
    ExaminationsShowComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadTranslates();
        this.route.params.subscribe(function (params) {
            _this.id = +params['id'];
            _this.examinationService.show(_this.id).then(function (response) {
                _this.examination = response.detailedExamination;
                _this.hrArray = response.detailedExamination.heartRatesArray.hr;
                _this.renderCharts();
            });
            _this.examinationService.getDescriptions(_this.id).then(function (response) {
                _this.descriptions = response;
            });
            _this.examinationService.getComments(_this.id).then(function (response) {
                _this.comments = response;
            });
        });
    };
    ExaminationsShowComponent.prototype.renderCharts = function () {
        var chartHr = document.getElementById('chart-hr');
        var chartCr = document.getElementById('chart-cramps');
        chartHr.width = 1000;
        chartCr.width = 1000;
        chartHr.height = 396;
        chartCr.height = 288;
        this.chartCramps =
            new pregnabit_chart_1.PregnabitChart().chartCramps(this.examination.crampArray, this.chartImg);
        this.chartHR =
            new pregnabit_chart_1.PregnabitChart().chartHeartRate(this.examination.heartRatesArray, this.chartImg, 1, this.hrLineOn);
        this.completeChartInfo();
    };
    ExaminationsShowComponent.prototype.completeChartInfo = function () {
        this.marginLeft = parseInt(this.chartHR.scales['y-axis-1'].width);
        this.marginRight = parseInt(this.chartHR.scales['x-axis-1'].margins.right);
    };
    ExaminationsShowComponent.prototype.rerenderCharts = function () {
        this.chartHR.render(0, true);
        this.chartCramps.render(0, true);
    };
    ExaminationsShowComponent.prototype.chartsReady = function () {
        return !!(this.chartImg['hr'] && this.chartImg['cramps']);
    };
    ExaminationsShowComponent.prototype.fetalMovementsCallback = function (img) {
        this.chartImg['fetal'] = img;
    };
    ExaminationsShowComponent.prototype.switchHrChart = function () {
        if (this.chartHrOn) {
            if (this.hrLineOn) {
                this.hrLineOn = false;
            }
            else {
                this.chartHrOn = false;
                this.examination.heartRatesArray.hr = [];
            }
        }
        else {
            this.chartHrOn = true;
            this.hrLineOn = true;
            this.examination.heartRatesArray.hr = this.hrArray;
        }
        this.renderCharts();
    };
    ExaminationsShowComponent.prototype.downloadFile = function (file) {
        var fileExtension = '.csv';
        if (file == 'ctg_input') {
            fileExtension = '.raw';
        }
        ;
        this.examinationService.downloadFile({ id: this.id, file: file }).then(function (response) {
            file_saver_1.saveAs(new Blob([response], { type: "text/csv" }), file + fileExtension);
        });
    };
    ExaminationsShowComponent.prototype.downloadRaw = function () {
        var md5 = new md5_1.Md5();
        var hash = md5.appendStr(this.examination.createdAt).end();
        window.open(rootUrl + '/download_raw_data?id=' + this.id + '&hash=' + hash);
    };
    ExaminationsShowComponent.prototype.deleteComment = function (comment) {
        var _this = this;
        this.examinationService.deleteComment({ id: this.id, comment_id: comment.id })
            .then(function (response) {
            if (response.status == 'success') {
                _this.comments.splice(_this.comments.indexOf(comment), 1);
                _this.translate.get('admin_panel.examinations.destroyed_comment').subscribe(function (res) {
                    _this.toastr.success(res);
                });
            }
            else {
                _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                    _this.toastr.error(res);
                });
            }
        });
    };
    ExaminationsShowComponent.prototype.deleteDescription = function (description) {
        var _this = this;
        this.examinationService.deleteDescription({ id: this.id, description_id: description.id })
            .then(function (response) {
            if (response.status == 'success') {
                _this.descriptions.splice(_this.descriptions.indexOf(description), 1);
                _this.translate.get('admin_panel.examinations.destroyed_description').subscribe(function (res) {
                    _this.toastr.success(res);
                });
            }
            else {
                _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                    _this.toastr.error(res);
                });
            }
        });
    };
    ExaminationsShowComponent.prototype.loadTranslates = function () {
        var _this = this;
        this.translate.get([
            'admin_panel.examinations.states.good',
            'admin_panel.examinations.states.repeat',
            'admin_panel.examinations.states.not_checked'])
            .subscribe(function (res) {
            _this.states = {
                'good': res['admin_panel.examinations.states.good'],
                'repeat': res['admin_panel.examinations.states.repeat'],
                'not_checked': res['admin_panel.examinations.states.not_checked'] };
        });
    };
    ExaminationsShowComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./examinations_show.component.html",
            providers: [examination_service_1.ExaminationService, pregnabit_chart_1.PregnabitChart],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], ExaminationsShowComponent);
    return ExaminationsShowComponent;
})();
exports.ExaminationsShowComponent = ExaminationsShowComponent;
//# sourceMappingURL=examinations_show.component.js.map