import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { Helpers } from '../../../../helpers';
import { ExaminationService } from '../../../../_services/examination.service';
import { DeviceService } from '../../../../_services/device.service';
import { DoctorService } from '../../../../_services/doctor.service';
import { PatientService } from '../../../../_services/patient.service';
import { MedicalUnitService } from '../../../../_services/medical_unit.service';


@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./examinations_index.component.html",
  providers: [ExaminationService, DeviceService, DoctorService, PatientService, MedicalUnitService],
  encapsulation: ViewEncapsulation.None,
})
export class ExaminationsIndexComponent implements OnInit {

  limit=10;
  query='';
  pagination:any;
  examinations:any;
  sortModel:any;
  sortOrder:any;
  patients:any;
  doctors:any;
  medicalUnits:any;
  devices:any;
  filters = {patientId: '', doctorId: '', medicalUnitId: '', deviceId: '', state: ''};

  constructor(
    private examinationService: ExaminationService,
    private deviceService: DeviceService,
    private doctorService: DoctorService,
    private patientService: PatientService,
    private medicalUnitService: MedicalUnitService
  )  {}

  ngOnInit()  {
    this.loadRecords(1);
    this.patientService.list().then(response => {
      this.patients = response;
    });
    this.doctorService.list().then(response => {
      this.doctors = response;
    });
    this.medicalUnitService.list().then(response => {
      this.medicalUnits = response;
    });
    this.deviceService.list().then(response => {
      this.devices = response;
    });

  }


  loadRecords(page=this.pagination.offset) {
    this.examinationService.index({
      offset: page, limit: this.limit, query: this.query, sortModel: this.sortModel,
      sortOrder: this.sortOrder, patientId: this.filters.patientId, doctorId: this.filters.doctorId,
      medicalUnitId: this.filters.medicalUnitId, deviceId: this.filters.deviceId,
      state: this.filters.state
    }).then(response => {
      this.pagination = response.pagination;
      this.examinations = response.examinations;
    })
  }

  sort(sortModel) {
    if(this.sortModel != sortModel) {
      this.sortOrder = 'asc';
    } else {
      if(this.sortOrder == 'asc') {
        this.sortOrder = 'desc';
      } else {
        this.sortOrder = 'asc';
      }
    }
    this.sortModel = sortModel;
    this.loadRecords();
  }

  changeLimit(limit) {
    this.limit = limit;
    this.loadRecords(1);
  }
}
