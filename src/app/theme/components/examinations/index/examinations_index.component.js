var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var examination_service_1 = require('../../../../_services/examination.service');
var device_service_1 = require('../../../../_services/device.service');
var doctor_service_1 = require('../../../../_services/doctor.service');
var patient_service_1 = require('../../../../_services/patient.service');
var medical_unit_service_1 = require('../../../../_services/medical_unit.service');
var ExaminationsIndexComponent = (function () {
    function ExaminationsIndexComponent(examinationService, deviceService, doctorService, patientService, medicalUnitService) {
        this.examinationService = examinationService;
        this.deviceService = deviceService;
        this.doctorService = doctorService;
        this.patientService = patientService;
        this.medicalUnitService = medicalUnitService;
        this.limit = 10;
        this.query = '';
        this.filters = { patientId: '', doctorId: '', medicalUnitId: '', deviceId: '', state: '' };
    }
    ExaminationsIndexComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.loadRecords(1);
        this.patientService.list().then(function (response) {
            _this.patients = response;
        });
        this.doctorService.list().then(function (response) {
            _this.doctors = response;
        });
        this.medicalUnitService.list().then(function (response) {
            _this.medicalUnits = response;
        });
        this.deviceService.list().then(function (response) {
            _this.devices = response;
        });
    };
    ExaminationsIndexComponent.prototype.loadRecords = function (page) {
        var _this = this;
        if (page === void 0) { page = this.pagination.offset; }
        this.examinationService.index({
            offset: page, limit: this.limit, query: this.query, sortModel: this.sortModel,
            sortOrder: this.sortOrder, patientId: this.filters.patientId, doctorId: this.filters.doctorId,
            medicalUnitId: this.filters.medicalUnitId, deviceId: this.filters.deviceId,
            state: this.filters.state
        }).then(function (response) {
            _this.pagination = response.pagination;
            _this.examinations = response.examinations;
        });
    };
    ExaminationsIndexComponent.prototype.sort = function (sortModel) {
        if (this.sortModel != sortModel) {
            this.sortOrder = 'asc';
        }
        else {
            if (this.sortOrder == 'asc') {
                this.sortOrder = 'desc';
            }
            else {
                this.sortOrder = 'asc';
            }
        }
        this.sortModel = sortModel;
        this.loadRecords();
    };
    ExaminationsIndexComponent.prototype.changeLimit = function (limit) {
        this.limit = limit;
        this.loadRecords(1);
    };
    ExaminationsIndexComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./examinations_index.component.html",
            providers: [examination_service_1.ExaminationService, device_service_1.DeviceService, doctor_service_1.DoctorService, patient_service_1.PatientService, medical_unit_service_1.MedicalUnitService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], ExaminationsIndexComponent);
    return ExaminationsIndexComponent;
})();
exports.ExaminationsIndexComponent = ExaminationsIndexComponent;
//# sourceMappingURL=examinations_index.component.js.map