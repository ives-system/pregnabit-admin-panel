import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { Helpers } from '../../../../helpers';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Examination } from '../../../../_models/examination';
import { PatientService } from '../../../../_services/patient.service';
import { ExaminationService } from '../../../../_services/examination.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
  templateUrl: "./examinations_edit.component.html",
  providers: [PatientService, ExaminationService],
  encapsulation: ViewEncapsulation.None,
})
export class ExaminationsEditComponent implements OnInit {
  id: number;
  examination: Examination;
  editExaminationForm: FormGroup;
  submitted = false;
  patients:any;
  newPregnancyWeek:any;
  doctorPatients: any

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService,
    public formBuilder: FormBuilder,
    private patientService: PatientService,
    private examinationService: ExaminationService,
    private translate: TranslateService
  ) {
    this.examination = new Examination();
    this.editExaminationForm = formBuilder.group({
      patientId: ['', Validators.required],
      pregnancyWeek: ['', Validators.required]
    });
  }



  ngOnInit()  {
    this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.patientService.list().then(patients => {
        this.getExaminations(patients)
      });

    });

  }

  getExaminations(patients) {
    this.examinationService.show(this.id).then(response => {
      this.examination = response
      console.log('patients', patients)
      console.log('examination', this.examination)
      console.log('doctorId', this.examination.doctorId)
      console.log('patients size', patients.length)
      console.log('doctors', patients.map(p => {return { id: p.doctorId, archived: p.isArchived } }))
      if (this.examination.doctorId) {
        this.patients = patients.filter(patient => (patient.doctorId === this.examination.doctorId && patient.isArchived === false))
        console.log('this patients', this.patients)
      }
    })
  }

  updateExamination() {
    this.submitted = true;
    if(this.editExaminationForm.valid) {
      this.examinationService.update(this.examination).then((result) => {
        this.translate.get('admin_panel.examinations.updated').subscribe((res: string) => {
          this.toastr.success(res);
        });
        this.router.navigateByUrl('/examinations/'+this.id);
      }, (err) => {
        console.log()
        var error = JSON.parse(err._body).join(', ');
        console.log('body', JSON.parse(err._body), 'error', error)
        this.translate.get('admin_panel.common.error').subscribe((res: string) => {
          this.toastr.error(error, res);
        });
      });
    } else {
      console.log('invalid');
    }
  }

  chosePatient(patientId) {
    this.newPregnancyWeek = null;
    this.patientService.pregnancyWeek({id: parseInt(patientId), examinationId: this.id}).then(response => {
      console.log(response);
      this.newPregnancyWeek = response;
    });
  }

}
