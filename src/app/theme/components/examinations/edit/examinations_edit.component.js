var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var examination_1 = require('../../../../_models/examination');
var patient_service_1 = require('../../../../_services/patient.service');
var examination_service_1 = require('../../../../_services/examination.service');
var ExaminationsEditComponent = (function () {
    function ExaminationsEditComponent(route, router, toastr, formBuilder, patientService, examinationService, translate) {
        this.route = route;
        this.router = router;
        this.toastr = toastr;
        this.formBuilder = formBuilder;
        this.patientService = patientService;
        this.examinationService = examinationService;
        this.translate = translate;
        this.submitted = false;
        this.examination = new examination_1.Examination();
        this.editExaminationForm = formBuilder.group({
            patientId: ['', forms_1.Validators.required],
            pregnancyWeek: ['', forms_1.Validators.required]
        });
    }
    ExaminationsEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            _this.id = +params['id'];
            _this.examinationService.show(_this.id).then(function (response) {
                _this.examination = response.detailedExamination;
                _this.patientService.list({ doctorId: _this.examination['doctorId'] }).then(function (response) {
                    _this.patients = response;
                });
            });
        });
    };
    ExaminationsEditComponent.prototype.updateExamination = function () {
        var _this = this;
        this.submitted = true;
        if (this.editExaminationForm.valid) {
            this.examinationService.update(this.examination).then(function (result) {
                _this.translate.get('admin_panel.examinations.updated').subscribe(function (res) {
                    _this.toastr.success(res);
                });
                _this.router.navigateByUrl('/examinations/' + _this.id);
            }, function (err) {
                var error = JSON.parse(err._body).messages.examination.join(', ');
                _this.translate.get('admin_panel.common.error').subscribe(function (res) {
                    _this.toastr.error(error, res);
                });
            });
        }
        else {
            console.log('invalid');
        }
    };
    ExaminationsEditComponent.prototype.chosePatient = function (patientId) {
        var _this = this;
        this.newPregnancyWeek = null;
        this.patientService.pregnancyWeek({ id: parseInt(patientId), examinationId: this.id }).then(function (response) {
            console.log(response);
            _this.newPregnancyWeek = response;
        });
    };
    ExaminationsEditComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./examinations_edit.component.html",
            providers: [patient_service_1.PatientService, examination_service_1.ExaminationService],
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], ExaminationsEditComponent);
    return ExaminationsEditComponent;
})();
exports.ExaminationsEditComponent = ExaminationsEditComponent;
//# sourceMappingURL=examinations_edit.component.js.map