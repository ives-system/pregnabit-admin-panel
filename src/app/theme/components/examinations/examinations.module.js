var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var forms_1 = require('@angular/forms');
var ngx_toastr_1 = require('ngx-toastr');
var common_1 = require('@angular/common');
var router_1 = require('@angular/router');
var examinations_index_component_1 = require('./index/examinations_index.component');
var examinations_show_component_1 = require('./show/examinations_show.component');
var examinations_edit_component_1 = require('./edit/examinations_edit.component');
var layout_module_1 = require('../../layouts/layout.module');
var default_component_1 = require('../../pages/default/default.component');
var validators_service_1 = require('../../../_services/validators.service');
var ng_bootstrap_1 = require('@ng-bootstrap/ng-bootstrap');
var fetal_movements_chart_directive_1 = require('../../../_directives/fetal_movements_chart.directive');
var role_guard_1 = require("../../../auth/_guards/role.guard");
var core_2 = require('@ngx-translate/core');
var routes = [
    {
        "path": "",
        "component": default_component_1.DefaultComponent,
        "children": [
            {
                "path": "",
                "component": examinations_index_component_1.ExaminationsIndexComponent
            },
            {
                "path": ":id",
                "component": examinations_show_component_1.ExaminationsShowComponent
            },
            {
                "path": "edit\/:id",
                "component": examinations_edit_component_1.ExaminationsEditComponent,
                "canActivate": [role_guard_1.RoleGuard],
                "data": {
                    "expectedRoles": ['superadmin']
                }
            }
        ]
    }
];
var ExaminationsModule = (function () {
    function ExaminationsModule() {
    }
    ExaminationsModule = __decorate([
        core_1.NgModule({ imports: [
                common_1.CommonModule,
                router_1.RouterModule.forChild(routes),
                layout_module_1.LayoutModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                ngx_toastr_1.ToastrModule.forRoot({ positionClass: 'toast-bottom-right' }),
                ng_bootstrap_1.NgbModule,
                core_2.TranslateModule
            ], exports: [
                router_1.RouterModule
            ], providers: [
                validators_service_1.ValidatorsService
            ], declarations: [
                examinations_index_component_1.ExaminationsIndexComponent,
                examinations_show_component_1.ExaminationsShowComponent,
                examinations_edit_component_1.ExaminationsEditComponent,
                fetal_movements_chart_directive_1.FetalMovementsChartDirective
            ] })
    ], ExaminationsModule);
    return ExaminationsModule;
})();
exports.ExaminationsModule = ExaminationsModule;
//# sourceMappingURL=examinations.module.js.map