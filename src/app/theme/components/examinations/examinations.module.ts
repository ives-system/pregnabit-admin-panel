import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ExaminationsIndexComponent } from './index/examinations_index.component';
import { ExaminationsShowComponent } from './show/examinations_show.component';
import { ExaminationsEditComponent } from './edit/examinations_edit.component';
import { LayoutModule } from '../../layouts/layout.module';
import { DefaultComponent } from '../../pages/default/default.component';
import { ValidatorsService } from '../../../_services/validators.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import  {FetalMovementsChartDirective } from '../../../_directives/fetal_movements_chart.directive'
import {RoleGuard} from "../../../auth/_guards/role.guard";
import { TranslateModule } from '@ngx-translate/core';


const routes: Routes = [
  {
    "path": "",
    "component": DefaultComponent,
    "children": [
      {
        "path": "",
        "component": ExaminationsIndexComponent
      },
      {
        "path": ":id",
        "component": ExaminationsShowComponent
      },
      {
        "path": "edit\/:id",
        "component": ExaminationsEditComponent,
        "canActivate": [RoleGuard],
        "data": {
          "expectedRoles": ['superadmin']
        }
      }
    ]
  }
];


@NgModule({imports: [
  CommonModule,
  RouterModule.forChild(routes),
  LayoutModule,
  FormsModule,
  ReactiveFormsModule,
  ToastrModule.forRoot({positionClass: 'toast-bottom-right'}),
  NgbModule,
  TranslateModule
],exports: [
  RouterModule
], providers: [
  ValidatorsService
],declarations: [
  ExaminationsIndexComponent,
  ExaminationsShowComponent,
  ExaminationsEditComponent,
  FetalMovementsChartDirective
]})


export class ExaminationsModule  {

}
