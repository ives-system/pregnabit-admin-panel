var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var theme_component_1 = require('./theme.component');
var router_1 = require('@angular/router');
var auth_guard_1 = require("../auth/_guards/auth.guard");
var role_guard_1 = require("../auth/_guards/role.guard");
var routes = [
    {
        "path": "",
        "component": theme_component_1.ThemeComponent,
        "canActivate": [auth_guard_1.AuthGuard],
        "children": [
            {
                "path": "index",
                "loadChildren": ".\/components\/dashboard\/dashboard.module#DashboardModule"
            },
            {
                "path": "admin_users",
                "loadChildren": ".\/components\/admin_users\/admin_users.module#AdminUsersModule",
                "canActivate": [role_guard_1.RoleGuard],
                "data": {
                    "expectedRoles": ['admin', 'superadmin']
                }
            },
            {
                "path": "merchant_users",
                "loadChildren": ".\/components\/merchant_users\/merchant_users.module#MerchantUsersModule",
                "canActivate": [role_guard_1.RoleGuard],
                "data": {
                    "expectedRoles": ['admin', 'superadmin']
                }
            },
            {
                "path": "devices",
                "loadChildren": ".\/components\/devices\/devices.module#DevicesModule",
                "canActivate": [role_guard_1.RoleGuard],
                "data": {
                    "expectedRoles": ['admin', 'superadmin', 'merchant']
                }
            },
            {
                "path": "call_centers",
                "loadChildren": ".\/components\/call_centers\/call_centers.module#CallCentersModule",
                "canActivate": [role_guard_1.RoleGuard],
                "data": {
                    "expectedRoles": ['admin', 'superadmin']
                }
            },
            {
                "path": "medical_units",
                "loadChildren": ".\/components\/medical_units\/medical_units.module#MedicalUnitsModule",
                "canActivate": [role_guard_1.RoleGuard],
                "data": {
                    "expectedRoles": ['admin', 'superadmin', 'merchant']
                }
            },
            {
                "path": "doctors",
                "loadChildren": ".\/components\/doctors\/doctors.module#DoctorsModule",
                "canActivate": [role_guard_1.RoleGuard],
                "data": {
                    "expectedRoles": ['admin', 'superadmin', 'merchant']
                }
            },
            {
                "path": "patients",
                "loadChildren": ".\/components\/patients\/patients.module#PatientsModule",
                "canActivate": [role_guard_1.RoleGuard],
                "data": {
                    "expectedRoles": ['admin', 'superadmin']
                }
            },
            {
                "path": "unaccepted_patients",
                "loadChildren": ".\/components\/unaccepted_patients\/unaccepted_patients.module#UnacceptedPatientsModule",
                "canActivate": [role_guard_1.RoleGuard],
                "data": {
                    "expectedRoles": ['admin', 'superadmin']
                }
            },
            {
                "path": "documentation_previews",
                "loadChildren": ".\/components\/documentation_previews\/documentation_previews.module#DocumentationPreviewsModule",
                "canActivate": [role_guard_1.RoleGuard],
                "data": {
                    "expectedRoles": ['admin', 'superadmin']
                }
            },
            {
                "path": "login_histories",
                "loadChildren": ".\/components\/login_histories\/login_histories.module#LoginHistoriesModule",
                "canActivate": [role_guard_1.RoleGuard],
                "data": {
                    "expectedRoles": ['admin', 'superadmin']
                }
            },
            {
                "path": "device_logs",
                "loadChildren": ".\/components\/device_logs\/device_logs.module#DeviceLogsModule",
                "canActivate": [role_guard_1.RoleGuard],
                "data": {
                    "expectedRoles": ['admin', 'superadmin']
                }
            },
            {
                "path": "reported_errors",
                "loadChildren": ".\/components\/reported_errors\/reported_errors.module#ReportedErrorsModule",
                "canActivate": [role_guard_1.RoleGuard],
                "data": {
                    "expectedRoles": ['admin', 'superadmin']
                }
            },
            {
                "path": "examinations",
                "loadChildren": ".\/components\/examinations\/examinations.module#ExaminationsModule",
                "canActivate": [role_guard_1.RoleGuard],
                "data": {
                    "expectedRoles": ['admin', 'superadmin']
                }
            },
            {
                "path": "unauthorized_examinations",
                "loadChildren": ".\/components\/unauthorized_examinations\/unauthorized_examinations.module#UnauthorizedExaminationsModule",
                "canActivate": [role_guard_1.RoleGuard],
                "data": {
                    "expectedRoles": ['admin', 'superadmin']
                }
            },
            {
                "path": "accepted_logs",
                "loadChildren": ".\/components\/accepted_logs\/accepted_logs.module#AcceptedLogsModule",
                "canActivate": [role_guard_1.RoleGuard],
                "data": {
                    "expectedRoles": ['admin', 'superadmin']
                }
            },
            {
                "path": "certificates",
                "loadChildren": ".\/components\/certificates\/certificates.module#CertificatesModule",
                "canActivate": [role_guard_1.RoleGuard],
                "data": {
                    "expectedRoles": ['admin', 'superadmin']
                }
            },
            {
                "path": "access",
                "loadChildren": ".\/components\/access\/access.module#AccessModule",
                "canActivate": [role_guard_1.RoleGuard],
                "data": {
                    "expectedRoles": ['superadmin']
                }
            },
            {
                "path": "header\/actions",
                "loadChildren": ".\/pages\/default\/header\/header-actions\/header-actions.module#HeaderActionsModule"
            },
            {
                "path": "header\/profile",
                "loadChildren": ".\/pages\/default\/header\/header-profile\/header-profile.module#HeaderProfileModule"
            },
            {
                "path": "404",
                "loadChildren": ".\/pages\/default\/not-found\/not-found.module#NotFoundModule"
            },
            {
                "path": "",
                "redirectTo": "index",
                "pathMatch": "full"
            }
        ]
    },
    {
        "path": "**",
        "redirectTo": "404",
        "pathMatch": "full"
    }
];
var ThemeRoutingModule = (function () {
    function ThemeRoutingModule() {
    }
    ThemeRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forChild(routes)],
            exports: [router_1.RouterModule]
        })
    ], ThemeRoutingModule);
    return ThemeRoutingModule;
})();
exports.ThemeRoutingModule = ThemeRoutingModule;
//# sourceMappingURL=theme-routing.module.js.map