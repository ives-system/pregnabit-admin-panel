var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var router_1 = require('@angular/router');
var helpers_1 = require('../helpers');
var core_2 = require('@ng-idle/core');
var ThemeComponent = (function () {
    function ThemeComponent(_script, _router, idle, keepalive, authenticationService, userService, translate) {
        var _this = this;
        this._script = _script;
        this._router = _router;
        this.idle = idle;
        this.keepalive = keepalive;
        this.authenticationService = authenticationService;
        this.userService = userService;
        this.translate = translate;
        idle.setIdle(5);
        idle.setTimeout(600);
        idle.setInterrupts(core_2.DEFAULT_INTERRUPTSOURCES);
        keepalive.interval(2);
        idle.onTimeout.subscribe(function () { return _this._router.navigate(['/logout']); });
        keepalive.onPing.subscribe(function () {
            if (localStorage.getItem("token")) {
                _this.authenticationService.heartbeat().then(function (result) {
                }, function (err) {
                    _this._router.navigate(['/logout']);
                });
            }
        });
        idle.watch();
    }
    ThemeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.userService.fetchCurrentUser();
        this._script.load('body', 'assets/vendors/base/vendors.bundle.js', 'assets/demo/default/base/scripts.bundle.js')
            .then(function (result) {
            helpers_1.Helpers.setLoading(false);
            // optional js to be loaded once
            _this._script.load('head', 'assets/vendors/custom/fullcalendar/fullcalendar.bundle.js');
        });
        this._router.events.subscribe(function (route) {
            if (route instanceof router_1.NavigationStart) {
                mLayout.closeMobileAsideMenuOffcanvas();
                mLayout.closeMobileHorMenuOffcanvas();
                mApp.scrollTop();
                helpers_1.Helpers.setLoading(true);
                // hide visible popover
                $('[data-toggle="m-popover"]').popover('hide');
            }
            if (route instanceof router_1.NavigationEnd) {
                // init required js
                mApp.init();
                mUtil.init();
                helpers_1.Helpers.setLoading(false);
                // content m-wrapper animation
                var animation = 'm-animate-fade-in-up';
                $('.m-wrapper').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function (e) {
                    $('.m-wrapper').removeClass(animation);
                }).removeClass(animation).addClass(animation);
            }
        });
    };
    ThemeComponent = __decorate([
        core_1.Component({
            selector: ".m-grid.m-grid--hor.m-grid--root.m-page",
            templateUrl: "./theme.component.html",
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], ThemeComponent);
    return ThemeComponent;
})();
exports.ThemeComponent = ThemeComponent;
//# sourceMappingURL=theme.component.js.map