import {NgModule} from '@angular/core';
import { ThemeComponent } from './theme.component';
import {Routes, RouterModule} from '@angular/router';
import {AuthGuard} from "../auth/_guards/auth.guard";
import {RoleGuard} from "../auth/_guards/role.guard";


const routes: Routes = [
    {
        "path": "",
        "component": ThemeComponent,
        "canActivate": [AuthGuard],
        "children": [
            {
                "path": "index",
                "loadChildren": ".\/components\/dashboard\/dashboard.module#DashboardModule"
            },
            {
                "path": "admin_users",
                "loadChildren": ".\/components\/admin_users\/admin_users.module#AdminUsersModule",
                "canActivate": [RoleGuard],
                "data": {
                  "expectedRoles": ['admin', 'superadmin']
                }
            },
            {
                "path": "merchant_users",
                "loadChildren": ".\/components\/merchant_users\/merchant_users.module#MerchantUsersModule",
                "canActivate": [RoleGuard],
                "data": {
                  "expectedRoles": ['admin', 'superadmin']
                }
            },
            {
                "path": "devices",
                "loadChildren": ".\/components\/devices\/devices.module#DevicesModule",
                "canActivate": [RoleGuard],
                "data": {
                  "expectedRoles": ['admin', 'superadmin', 'merchant']
                }
            },
            {
                "path": "call_centers",
                "loadChildren": ".\/components\/call_centers\/call_centers.module#CallCentersModule",
                "canActivate": [RoleGuard],
                "data": {
                  "expectedRoles": ['admin', 'superadmin']
                }
            },
            {
                "path": "medical_units",
                "loadChildren": ".\/components\/medical_units\/medical_units.module#MedicalUnitsModule",
                "canActivate": [RoleGuard],
                "data": {
                  "expectedRoles": ['admin', 'superadmin', 'merchant']
                }
            },
            {
                "path": "doctors",
                "loadChildren": ".\/components\/doctors\/doctors.module#DoctorsModule",
                "canActivate": [RoleGuard],
                "data": {
                  "expectedRoles": ['admin', 'superadmin', 'merchant']
                }
            },
            {
                "path": "patients",
                "loadChildren": ".\/components\/patients\/patients.module#PatientsModule",
                "canActivate": [RoleGuard],
                "data": {
                  "expectedRoles": ['admin', 'superadmin']
                }
            },
            {
              "path": "unaccepted_patients",
              "loadChildren": ".\/components\/unaccepted_patients\/unaccepted_patients.module#UnacceptedPatientsModule",
              "canActivate": [RoleGuard],
              "data": {
                "expectedRoles": ['admin', 'superadmin']
              }
            },
            {
              "path": "documentation_previews",
              "loadChildren": ".\/components\/documentation_previews\/documentation_previews.module#DocumentationPreviewsModule",
              "canActivate": [RoleGuard],
              "data": {
                "expectedRoles": ['admin', 'superadmin']
              }
            },
            {
              "path": "login_histories",
              "loadChildren": ".\/components\/login_histories\/login_histories.module#LoginHistoriesModule",
              "canActivate": [RoleGuard],
              "data": {
                "expectedRoles": ['admin', 'superadmin']
              }
            },
            {
              "path": "device_logs",
              "loadChildren": ".\/components\/device_logs\/device_logs.module#DeviceLogsModule",
              "canActivate": [RoleGuard],
              "data": {
                "expectedRoles": ['admin', 'superadmin']
              }
            },
            {
              "path": "reported_errors",
              "loadChildren": ".\/components\/reported_errors\/reported_errors.module#ReportedErrorsModule",
              "canActivate": [RoleGuard],
              "data": {
                "expectedRoles": ['admin', 'superadmin']
              }
            },
            {
              "path": "examinations",
              "loadChildren": ".\/components\/examinations\/examinations.module#ExaminationsModule",
              "canActivate": [RoleGuard],
              "data": {
                "expectedRoles": ['admin', 'superadmin']
              }
            },
            {
              "path": "unauthorized_examinations",
              "loadChildren": ".\/components\/unauthorized_examinations\/unauthorized_examinations.module#UnauthorizedExaminationsModule",
              "canActivate": [RoleGuard],
              "data": {
                "expectedRoles": ['admin', 'superadmin']
              }
            },
            {
              "path": "accepted_logs",
              "loadChildren": ".\/components\/accepted_logs\/accepted_logs.module#AcceptedLogsModule",
              "canActivate": [RoleGuard],
              "data": {
                "expectedRoles": ['admin', 'superadmin']
              }
            },
            {
              "path": "certificates",
              "loadChildren": ".\/components\/certificates\/certificates.module#CertificatesModule",
              "canActivate": [RoleGuard],
              "data": {
                "expectedRoles": ['admin', 'superadmin']
              }
            },
            {
              "path": "access",
              "loadChildren": ".\/components\/access\/access.module#AccessModule",
              "canActivate": [RoleGuard],
              "data": {
                "expectedRoles": ['superadmin']
              }
            },
            {
                "path": "header\/actions",
                "loadChildren": ".\/pages\/default\/header\/header-actions\/header-actions.module#HeaderActionsModule"
            },
            {
                "path": "header\/profile",
                "loadChildren": ".\/pages\/default\/header\/header-profile\/header-profile.module#HeaderProfileModule"
            },
            {
                "path": "404",
                "loadChildren": ".\/pages\/default\/not-found\/not-found.module#NotFoundModule"
            },
            {
                "path": "",
                "redirectTo": "index",
                "pathMatch": "full"
            }
        ]
    },
    {
        "path": "**",
        "redirectTo": "404",
        "pathMatch": "full"
    }
];

@NgModule({
	imports: [RouterModule.forChild(routes)],
	exports: [RouterModule]
})
export class ThemeRoutingModule {}
