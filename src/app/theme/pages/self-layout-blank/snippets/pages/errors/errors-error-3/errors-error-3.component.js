var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var helpers_1 = require('../../../../../../../helpers');
var ErrorsError3Component = (function () {
    function ErrorsError3Component() {
    }
    ErrorsError3Component.prototype.ngOnInit = function () {
    };
    ErrorsError3Component.prototype.ngAfterViewInit = function () {
        helpers_1.Helpers.bodyClass('m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default');
    };
    ErrorsError3Component = __decorate([
        core_1.Component({
            selector: ".m-grid.m-grid--hor.m-grid--root.m-page",
            templateUrl: "./errors-error-3.component.html",
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], ErrorsError3Component);
    return ErrorsError3Component;
})();
exports.ErrorsError3Component = ErrorsError3Component;
//# sourceMappingURL=errors-error-3.component.js.map