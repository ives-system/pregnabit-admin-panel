var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var helpers_1 = require('../../../../../../../helpers');
var UserLogin1Component = (function () {
    function UserLogin1Component(_script) {
        this._script = _script;
    }
    UserLogin1Component.prototype.ngOnInit = function () {
    };
    UserLogin1Component.prototype.ngAfterViewInit = function () {
        this._script.load('.m-grid.m-grid--hor.m-grid--root.m-page', 'assets/snippets/pages/user/login.js');
        helpers_1.Helpers.bodyClass('m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default');
    };
    UserLogin1Component = __decorate([
        core_1.Component({
            selector: ".m-grid.m-grid--hor.m-grid--root.m-page",
            templateUrl: "./user-login-1.component.html",
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], UserLogin1Component);
    return UserLogin1Component;
})();
exports.UserLogin1Component = UserLogin1Component;
//# sourceMappingURL=user-login-1.component.js.map