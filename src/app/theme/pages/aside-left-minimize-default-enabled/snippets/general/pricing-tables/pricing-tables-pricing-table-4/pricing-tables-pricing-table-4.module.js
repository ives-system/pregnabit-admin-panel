var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var router_1 = require('@angular/router');
var pricing_tables_pricing_table_4_component_1 = require('./pricing-tables-pricing-table-4.component');
var layout_module_1 = require('../../../../../../layouts/layout.module');
var aside_left_minimize_default_enabled_component_1 = require('../../../../aside-left-minimize-default-enabled.component');
var routes = [
    {
        "path": "",
        "component": aside_left_minimize_default_enabled_component_1.AsideLeftMinimizeDefaultEnabledComponent,
        "children": [
            {
                "path": "",
                "component": pricing_tables_pricing_table_4_component_1.PricingTablesPricingTable4Component
            }
        ]
    }
];
var PricingTablesPricingTable4Module = (function () {
    function PricingTablesPricingTable4Module() {
    }
    PricingTablesPricingTable4Module = __decorate([
        core_1.NgModule({ imports: [
                common_1.CommonModule, router_1.RouterModule.forChild(routes), layout_module_1.LayoutModule
            ], exports: [
                router_1.RouterModule
            ], declarations: [
                pricing_tables_pricing_table_4_component_1.PricingTablesPricingTable4Component
            ] })
    ], PricingTablesPricingTable4Module);
    return PricingTablesPricingTable4Module;
})();
exports.PricingTablesPricingTable4Module = PricingTablesPricingTable4Module;
//# sourceMappingURL=pricing-tables-pricing-table-4.module.js.map