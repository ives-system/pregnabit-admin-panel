var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var helpers_1 = require('../../../../../../../helpers');
var PricingTablesPricingTable1Component = (function () {
    function PricingTablesPricingTable1Component() {
    }
    PricingTablesPricingTable1Component.prototype.ngOnInit = function () {
    };
    PricingTablesPricingTable1Component.prototype.ngAfterViewInit = function () {
        helpers_1.Helpers.bodyClass('m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-aside-left--minimize m-brand--minimize m-footer--push m-aside--offcanvas-default');
    };
    PricingTablesPricingTable1Component = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./pricing-tables-pricing-table-1.component.html",
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], PricingTablesPricingTable1Component);
    return PricingTablesPricingTable1Component;
})();
exports.PricingTablesPricingTable1Component = PricingTablesPricingTable1Component;
//# sourceMappingURL=pricing-tables-pricing-table-1.component.js.map