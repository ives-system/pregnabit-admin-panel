var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var forms_1 = require('@angular/forms');
var router_1 = require('@angular/router');
var primeng_component_1 = require('./primeng.component');
var primeng_input_component_1 = require('./input/primeng-input.component');
var primeng_button_component_1 = require('./button/primeng-button.component');
var primeng_panel_component_1 = require('./panel/primeng-panel.component');
var default_component_1 = require('../../default.component');
var layout_module_1 = require('../../../../layouts/layout.module');
var primeng_1 = require('primeng/primeng');
var routes = [
    {
        path: "",
        component: default_component_1.DefaultComponent,
        children: [
            {
                path: "",
                component: primeng_component_1.PrimengComponent,
                children: [
                    { path: 'input', component: primeng_input_component_1.PrimeNgInputComponent },
                    { path: 'button', component: primeng_button_component_1.PrimeNgButtonComponent },
                    { path: 'panel', component: primeng_panel_component_1.PrimeNgPanelComponent },
                ]
            }
        ]
    },
];
var PrimengModule = (function () {
    function PrimengModule() {
    }
    PrimengModule = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule, router_1.RouterModule.forChild(routes),
                layout_module_1.LayoutModule,
                forms_1.FormsModule,
                // primeng modules
                primeng_1.ButtonModule,
                primeng_1.CheckboxModule,
                primeng_1.ChipsModule,
                primeng_1.CodeHighlighterModule,
                primeng_1.ColorPickerModule,
                primeng_1.InputMaskModule,
                primeng_1.GrowlModule,
                primeng_1.InputTextModule,
                primeng_1.MultiSelectModule,
                primeng_1.RadioButtonModule,
                primeng_1.SelectButtonModule,
                primeng_1.SplitButtonModule,
                primeng_1.TabViewModule,
                primeng_1.AccordionModule,
                primeng_1.PanelModule,
                primeng_1.FieldsetModule
            ], declarations: [
                primeng_component_1.PrimengComponent,
                primeng_input_component_1.PrimeNgInputComponent,
                primeng_button_component_1.PrimeNgButtonComponent,
                primeng_panel_component_1.PrimeNgPanelComponent
            ]
        })
    ], PrimengModule);
    return PrimengModule;
})();
exports.PrimengModule = PrimengModule;
//# sourceMappingURL=primeng.module.js.map