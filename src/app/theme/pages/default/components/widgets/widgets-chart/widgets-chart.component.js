var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var WidgetsChartComponent = (function () {
    function WidgetsChartComponent(_script) {
        this._script = _script;
    }
    WidgetsChartComponent.prototype.ngOnInit = function () {
    };
    WidgetsChartComponent.prototype.ngAfterViewInit = function () {
        this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper', 'assets/vendors/custom/gmaps/gmaps.js', 'assets/app/js/dashboard.js');
    };
    WidgetsChartComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./widgets-chart.component.html",
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], WidgetsChartComponent);
    return WidgetsChartComponent;
})();
exports.WidgetsChartComponent = WidgetsChartComponent;
//# sourceMappingURL=widgets-chart.component.js.map