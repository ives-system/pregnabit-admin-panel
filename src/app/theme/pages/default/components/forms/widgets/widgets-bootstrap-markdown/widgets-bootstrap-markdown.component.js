var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var WidgetsBootstrapMarkdownComponent = (function () {
    function WidgetsBootstrapMarkdownComponent(_script) {
        this._script = _script;
    }
    WidgetsBootstrapMarkdownComponent.prototype.ngOnInit = function () {
    };
    WidgetsBootstrapMarkdownComponent.prototype.ngAfterViewInit = function () {
        this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper', 'assets/demo/default/custom/components/forms/widgets/bootstrap-markdown.js');
        $('[data-provide="markdown"]').markdown();
    };
    WidgetsBootstrapMarkdownComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./widgets-bootstrap-markdown.component.html",
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], WidgetsBootstrapMarkdownComponent);
    return WidgetsBootstrapMarkdownComponent;
})();
exports.WidgetsBootstrapMarkdownComponent = WidgetsBootstrapMarkdownComponent;
//# sourceMappingURL=widgets-bootstrap-markdown.component.js.map