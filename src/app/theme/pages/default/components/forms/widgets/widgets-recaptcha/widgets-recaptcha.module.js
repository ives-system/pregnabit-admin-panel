var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var common_1 = require('@angular/common');
var ng_recaptcha_1 = require('ng-recaptcha');
var router_1 = require('@angular/router');
var widgets_recaptcha_component_1 = require('./widgets-recaptcha.component');
var layout_module_1 = require('../../../../../../layouts/layout.module');
var default_component_1 = require('../../../../default.component');
var routes = [
    {
        "path": "",
        "component": default_component_1.DefaultComponent,
        "children": [
            {
                "path": "",
                "component": widgets_recaptcha_component_1.WidgetsRecaptchaComponent
            }
        ]
    }
];
var WidgetsRecaptchaModule = (function () {
    function WidgetsRecaptchaModule() {
    }
    WidgetsRecaptchaModule = __decorate([
        core_1.NgModule({ imports: [
                ng_recaptcha_1.RecaptchaModule.forRoot(),
                common_1.CommonModule, router_1.RouterModule.forChild(routes), layout_module_1.LayoutModule
            ], exports: [
                router_1.RouterModule
            ], declarations: [
                widgets_recaptcha_component_1.WidgetsRecaptchaComponent
            ] })
    ], WidgetsRecaptchaModule);
    return WidgetsRecaptchaModule;
})();
exports.WidgetsRecaptchaModule = WidgetsRecaptchaModule;
//# sourceMappingURL=widgets-recaptcha.module.js.map