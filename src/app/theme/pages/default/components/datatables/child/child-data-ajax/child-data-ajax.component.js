var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var ChildDataAjaxComponent = (function () {
    function ChildDataAjaxComponent(_script) {
        this._script = _script;
    }
    ChildDataAjaxComponent.prototype.ngOnInit = function () {
    };
    ChildDataAjaxComponent.prototype.ngAfterViewInit = function () {
        this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper', 'assets/demo/default/custom/components/datatables/child/data-ajax.js');
    };
    ChildDataAjaxComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./child-data-ajax.component.html",
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], ChildDataAjaxComponent);
    return ChildDataAjaxComponent;
})();
exports.ChildDataAjaxComponent = ChildDataAjaxComponent;
//# sourceMappingURL=child-data-ajax.component.js.map