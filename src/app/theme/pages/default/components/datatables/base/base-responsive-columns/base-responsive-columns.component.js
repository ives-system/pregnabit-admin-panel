var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var BaseResponsiveColumnsComponent = (function () {
    function BaseResponsiveColumnsComponent(_script) {
        this._script = _script;
    }
    BaseResponsiveColumnsComponent.prototype.ngOnInit = function () {
    };
    BaseResponsiveColumnsComponent.prototype.ngAfterViewInit = function () {
        this._script.load('.m-grid__item.m-grid__item--fluid.m-wrapper', 'assets/demo/default/custom/components/datatables/base/responsive-columns.js');
    };
    BaseResponsiveColumnsComponent = __decorate([
        core_1.Component({
            selector: ".m-grid__item.m-grid__item--fluid.m-wrapper",
            templateUrl: "./base-responsive-columns.component.html",
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], BaseResponsiveColumnsComponent);
    return BaseResponsiveColumnsComponent;
})();
exports.BaseResponsiveColumnsComponent = BaseResponsiveColumnsComponent;
//# sourceMappingURL=base-responsive-columns.component.js.map