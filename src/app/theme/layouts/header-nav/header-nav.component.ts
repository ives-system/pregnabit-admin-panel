import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import {Router} from "@angular/router";
import { Helpers } from '../../../helpers';
import {AuthenticationService} from "../../../auth/_services/authentication.service";
import {SwitchLanguage} from "../../../_services/switch_language";
import { TranslateService } from '@ngx-translate/core';


declare let mLayout: any;
@Component({
  selector: "app-header-nav",
  templateUrl: "./header-nav.component.html",
  encapsulation: ViewEncapsulation.None,
})
export class HeaderNavComponent implements OnInit, AfterViewInit {


  constructor(
    private _authService: AuthenticationService,
    private _router: Router,
    private switchLanguage: SwitchLanguage,
    private translate: TranslateService
  )  {}

  ngOnInit()  {

  }

  ngAfterViewInit()  {

  mLayout.initHeader();

  }

  setLanguage(lang) {
    this.switchLanguage.switchLanguage(lang)
  }
}
