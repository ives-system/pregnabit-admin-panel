var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var AsideNavComponent = (function () {
    function AsideNavComponent(userService) {
        this.userService = userService;
    }
    AsideNavComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.userService.user != undefined) {
            this.currentUser = this.userService.user;
        }
        else {
            setTimeout(function () {
                _this.currentUser = _this.userService.user;
            }, 1000);
        }
    };
    AsideNavComponent.prototype.ngAfterViewInit = function () {
        mLayout.initAside();
        var menu = mLayout.getAsideMenu();
        var item = $(menu).find('a[href="' + window.location.pathname + '"]').parent('.m-menu__item');
        $(menu).data('menu').setActiveItem(item);
    };
    AsideNavComponent = __decorate([
        core_1.Component({
            selector: "app-aside-nav",
            templateUrl: "./aside-nav.component.html",
            encapsulation: core_1.ViewEncapsulation.None
        })
    ], AsideNavComponent);
    return AsideNavComponent;
})();
exports.AsideNavComponent = AsideNavComponent;
//# sourceMappingURL=aside-nav.component.js.map