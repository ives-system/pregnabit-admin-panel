import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Helpers } from '../../../helpers';
import {UserService} from "../../../auth/_services/user.service";
import {Observable} from 'rxjs/Observable';


declare let mLayout: any;
@Component({
selector: "app-aside-nav",
templateUrl: "./aside-nav.component.html",
encapsulation: ViewEncapsulation.None,
})
export class AsideNavComponent implements OnInit, AfterViewInit {

currentUser: Observable<any>;

constructor(
  private userService: UserService
)  {

}
ngOnInit()  {
  if (this.userService.user != undefined) {
    this.currentUser = this.userService.user;
  } else {
    setTimeout(() => {
      this.currentUser = this.userService.user;
    }, 1000)
  }

}

ngAfterViewInit()  {
mLayout.initAside();
let menu = mLayout.getAsideMenu(); let item = $(menu).find('a[href="' + window.location.pathname + '"]').parent('.m-menu__item'); (<any>$(menu).data('menu')).setActiveItem(item);
}

}
