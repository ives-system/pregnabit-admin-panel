import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { Helpers } from '../helpers';
import {AuthenticationService} from "../auth/_services/authentication.service";
import { ScriptLoaderService } from '../_services/script-loader.service';
import {Idle, DEFAULT_INTERRUPTSOURCES} from '@ng-idle/core';
import {Keepalive} from '@ng-idle/keepalive';
import {UserService} from "../auth/_services/user.service";
import { TranslateService } from '@ngx-translate/core';


declare let mApp: any;
declare let mUtil: any;
declare let mLayout: any;
@Component({
selector: ".m-grid.m-grid--hor.m-grid--root.m-page",
templateUrl: "./theme.component.html",
encapsulation: ViewEncapsulation.None,
})
export class ThemeComponent implements OnInit {


  constructor(
    private _script: ScriptLoaderService,
    private _router: Router,
    private idle: Idle,
    private keepalive: Keepalive,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private translate: TranslateService
  ) {
    idle.setIdle(5);
    idle.setTimeout(1800); //30 minutes
    idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
    keepalive.interval(2);

    idle.onTimeout.subscribe(() => this._router.navigate(['/logout']));

    keepalive.onPing.subscribe(() => {
      if(localStorage.getItem("token")) {
        this.authenticationService.heartbeat().then((result) => {
        }, (err) => {
          this._router.navigate(['/logout'])
        });
      }
    });
    idle.watch();

  }


  ngOnInit()  {

      this.userService.fetchCurrentUser();
      this._script.load('body', 'assets/vendors/base/vendors.bundle.js','assets/demo/default/base/scripts.bundle.js')
        .then(result => {
          Helpers.setLoading(false);
          // optional js to be loaded once
          this._script.load('head', 'assets/vendors/custom/fullcalendar/fullcalendar.bundle.js');
        });
      this._router.events.subscribe((route) => {
        if (route instanceof NavigationStart) {
          (<any>mLayout).closeMobileAsideMenuOffcanvas();
  (<any>mLayout).closeMobileHorMenuOffcanvas();
          (<any>mApp).scrollTop();
          Helpers.setLoading(true);
          // hide visible popover
          (<any>$('[data-toggle="m-popover"]')).popover('hide');
        }
        if (route instanceof NavigationEnd) {
          // init required js
          (<any>mApp).init();
          (<any>mUtil).init();
          Helpers.setLoading(false);
          // content m-wrapper animation
          let animation = 'm-animate-fade-in-up';
          $('.m-wrapper').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function (e) {
            $('.m-wrapper').removeClass(animation);
          }).removeClass(animation).addClass(animation);
        }
      });
  }

}
