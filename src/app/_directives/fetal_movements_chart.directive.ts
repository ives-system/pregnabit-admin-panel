import { Directive, Input, Output, EventEmitter, ElementRef } from '@angular/core';

@Directive({
  selector: '[pbFetalMovementsChart]'
})
export class FetalMovementsChartDirective {
  @Input('fma') fma:any;
  @Input('marginLeft') marginLeft:any;
  @Input('marginRight') marginRight:any;
  @Input('chartWidth') chartWidth:any;

  @Output() fmaReady: EventEmitter<any> = new EventEmitter();

  initialWidth:any;
  initialHeight:any;
  ctx:any;
  cnt:any;
  size:any;
  height:any;
  step:any;
  index:any;
  array:Array<number> = new Array(1800);
  element:any;

  constructor(el: ElementRef) {
    this.element = el;


  }

  ngOnInit() {
    for(var i = 0;i<this.array.length;i++) {
      this.array[i] = i+1;
    }


    this.initialWidth = this.element.nativeElement.width;
    this.initialHeight = this.element.nativeElement.height;
    this.element.nativeElement.width = this.chartWidth;
    this.element.nativeElement.height = this.initialHeight;


    this.ctx = this.element.nativeElement.getContext('2d');
    this.size = this.element.nativeElement.width-this.marginLeft-this.marginRight;
    this.height = this.initialHeight;

    this.step = this.size/(30.0*60.0);
    this.ctx.fillStyle = "#FFFFFF";
    this.ctx.fillRect(this.marginLeft,0,this.size,this.height);
    this.cnt = 0;
    this.ctx.fillStyle = "#732aab";

    for(let second in this.array) {
      let temp:any = this.fma[parseInt(this.cnt)]/1000;

      while(this.cnt <= this.fma.length && parseInt(temp) < parseInt(second)) {
        this.cnt += 1;
        temp = this.fma[parseInt(this.cnt)]/1000;
      }
      if(this.cnt <= this.fma.length && parseInt(temp) == parseInt(second)) {
        this.index = this.cnt;
      } else {
        this.index = -1;
      }
      if(this.index != -1) {
        this.ctx.fillRect(this.marginLeft+parseInt(second)*this.step,1,Math.max(this.step*2, 2),this.marginLeft+this.size)
      }
    }

    this.ctx.fillStyle = "#c8c8c8";

    this.ctx.fillRect(this.marginLeft,0,this.size,1);
    this.ctx.fillRect(this.marginLeft,this.height-1,this.size,1);
    this.ctx.fillRect(this.marginLeft,0,1,this.height);
    this.ctx.fillRect(this.marginLeft+this.size,0,1,this.height);

    this.fmaReady.emit([this.element.nativeElement.toDataURL("image/png")]);
  }
}
