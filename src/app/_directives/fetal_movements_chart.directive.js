var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var FetalMovementsChartDirective = (function () {
    function FetalMovementsChartDirective(el) {
        this.fmaReady = new core_1.EventEmitter();
        this.array = new Array(1800);
        this.element = el;
    }
    FetalMovementsChartDirective.prototype.ngOnInit = function () {
        for (var i = 0; i < this.array.length; i++) {
            this.array[i] = i + 1;
        }
        this.initialWidth = this.element.nativeElement.width;
        this.initialHeight = this.element.nativeElement.height;
        this.element.nativeElement.width = this.chartWidth;
        this.element.nativeElement.height = this.initialHeight;
        this.ctx = this.element.nativeElement.getContext('2d');
        this.size = this.element.nativeElement.width - this.marginLeft - this.marginRight;
        this.height = this.initialHeight;
        this.step = this.size / (30.0 * 60.0);
        this.ctx.fillStyle = "#FFFFFF";
        this.ctx.fillRect(this.marginLeft, 0, this.size, this.height);
        this.cnt = 0;
        this.ctx.fillStyle = "#732aab";
        for (var second in this.array) {
            var temp = this.fma[parseInt(this.cnt)] / 1000;
            while (this.cnt <= this.fma.length && parseInt(temp) < parseInt(second)) {
                this.cnt += 1;
                temp = this.fma[parseInt(this.cnt)] / 1000;
            }
            if (this.cnt <= this.fma.length && parseInt(temp) == parseInt(second)) {
                this.index = this.cnt;
            }
            else {
                this.index = -1;
            }
            if (this.index != -1) {
                this.ctx.fillRect(this.marginLeft + parseInt(second) * this.step, 1, Math.max(this.step * 2, 2), this.marginLeft + this.size);
            }
        }
        this.ctx.fillStyle = "#c8c8c8";
        this.ctx.fillRect(this.marginLeft, 0, this.size, 1);
        this.ctx.fillRect(this.marginLeft, this.height - 1, this.size, 1);
        this.ctx.fillRect(this.marginLeft, 0, 1, this.height);
        this.ctx.fillRect(this.marginLeft + this.size, 0, 1, this.height);
        this.fmaReady.emit([this.element.nativeElement.toDataURL("image/png")]);
    };
    __decorate([
        core_1.Input('fma')
    ], FetalMovementsChartDirective.prototype, "fma");
    __decorate([
        core_1.Input('marginLeft')
    ], FetalMovementsChartDirective.prototype, "marginLeft");
    __decorate([
        core_1.Input('marginRight')
    ], FetalMovementsChartDirective.prototype, "marginRight");
    __decorate([
        core_1.Input('chartWidth')
    ], FetalMovementsChartDirective.prototype, "chartWidth");
    __decorate([
        core_1.Output()
    ], FetalMovementsChartDirective.prototype, "fmaReady");
    FetalMovementsChartDirective = __decorate([
        core_1.Directive({
            selector: '[pbFetalMovementsChart]'
        })
    ], FetalMovementsChartDirective);
    return FetalMovementsChartDirective;
})();
exports.FetalMovementsChartDirective = FetalMovementsChartDirective;
//# sourceMappingURL=fetal_movements_chart.directive.js.map