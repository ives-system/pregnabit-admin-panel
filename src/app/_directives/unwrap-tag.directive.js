var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var helpers_1 = require('../helpers');
var UnwrapTagDirective = (function () {
    function UnwrapTagDirective(el) {
        this.el = el;
    }
    UnwrapTagDirective.prototype.ngAfterViewInit = function () {
        var nativeElement = this.el.nativeElement;
        helpers_1.Helpers.unwrapTag(nativeElement);
    };
    UnwrapTagDirective = __decorate([
        core_1.Directive({
            selector: "[appunwraptag]"
        })
    ], UnwrapTagDirective);
    return UnwrapTagDirective;
})();
exports.UnwrapTagDirective = UnwrapTagDirective;
//# sourceMappingURL=unwrap-tag.directive.js.map