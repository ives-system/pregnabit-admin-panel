import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ValidatorsService } from '../_services/validators.service';

@Injectable()
export class EmailValidator {

  debouncer: any;

  constructor(public validatorsService: ValidatorsService){

  }

  checkEmail(control: FormControl): any {

    clearTimeout(this.debouncer);

    return new Promise(resolve => {

      this.debouncer = setTimeout(() => {

        this.validatorsService.adminUserEmailUniqueness(control.value).then((res) => {
          if(res) {
            resolve(null)
          } else {
            resolve({
              unique: true
            })
          }
        });

      }, 1000);

    });
  }

}
