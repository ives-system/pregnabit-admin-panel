var PasswordValidation = (function () {
    function PasswordValidation() {
    }
    PasswordValidation.MatchPassword = function (AC) {
        var password = AC.get('password').value;
        var confirmPassword = AC.get('passwordConfirmation').value;
        if (password != confirmPassword) {
            AC.get('passwordConfirmation').setErrors({ pwmatch: true });
        }
        else {
            return null;
        }
    };
    return PasswordValidation;
})();
exports.PasswordValidation = PasswordValidation;
//# sourceMappingURL=password_validator.js.map