import {AbstractControl} from '@angular/forms';
export class PasswordValidation {

    static MatchPassword(AC: AbstractControl) {
        let password = AC.get('password').value;
        let confirmPassword = AC.get('passwordConfirmation').value;
        if(password != confirmPassword) {
            AC.get('passwordConfirmation').setErrors( {pwmatch: true} )
        } else {
            return null
        }
    }
}