var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var platform_browser_1 = require('@angular/platform-browser');
var core_1 = require('@angular/core');
var theme_component_1 = require('./theme/theme.component');
var layout_module_1 = require('./theme/layouts/layout.module');
var animations_1 = require("@angular/platform-browser/animations");
var ng_bootstrap_1 = require('@ng-bootstrap/ng-bootstrap');
var keepalive_1 = require('@ng-idle/keepalive');
var http_1 = require('@angular/common/http');
var core_2 = require('@ngx-translate/core');
var translate_http_loader_1 = require('./_services/translate_http_loader');
var switch_language_1 = require("./_services/switch_language");
var app_routes_1 = require('./app.routes');
var app_component_1 = require('./app.component');
var script_loader_service_1 = require("./_services/script-loader.service");
var theme_routing_module_1 = require("./theme/theme-routing.module");
var auth_module_1 = require("./auth/auth.module");
function HttpLoaderFactory(http) {
    return new translate_http_loader_1.TranslateHttpLoader(http);
}
exports.HttpLoaderFactory = HttpLoaderFactory;
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                theme_component_1.ThemeComponent,
                app_component_1.AppComponent,
            ],
            imports: [
                layout_module_1.LayoutModule,
                platform_browser_1.BrowserModule,
                animations_1.BrowserAnimationsModule,
                app_routes_1.AppRoutingModule,
                theme_routing_module_1.ThemeRoutingModule,
                http_1.HttpClientModule,
                auth_module_1.AuthModule,
                keepalive_1.NgIdleKeepaliveModule.forRoot(),
                ng_bootstrap_1.NgbModule.forRoot(),
                core_2.TranslateModule.forRoot({
                    loader: {
                        provide: core_2.TranslateLoader,
                        useFactory: HttpLoaderFactory,
                        deps: [http_1.HttpClient]
                    } })
            ],
            exports: [core_2.TranslateModule],
            providers: [script_loader_service_1.ScriptLoaderService, http_1.HttpClientModule, switch_language_1.SwitchLanguage
            ],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
})();
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map