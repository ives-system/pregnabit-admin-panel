import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogoutComponent } from "./auth/logout/logout.component";
import { PasswordComponent } from "./auth/password/password.component";

const routes: Routes = [
  {path: 'login', loadChildren: './auth/auth.module#AuthModule'},
  {path: 'logout', component: LogoutComponent},
  {path: 'password/edit', component: PasswordComponent},
  {path: '', redirectTo: 'index', pathMatch: 'full'},
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule { }
