export function changedAuditValue(change) {
  if (Array.isArray(change[1]) && change[1].length > 1) {
    return change[1][0]
  } else {
    return change[1]
  }
}

export function newAuditValue(change) {
  if (Array.isArray(change[1]) && change[1].length > 1) {
    return change[1][1]
  } else {
    return change[1]
  }
}
