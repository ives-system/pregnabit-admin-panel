export default class CountrySelector {
  static select(obj) {
    this.getIP(this.getCountryCode, obj);
  }

  static getIP(callback, intlTelInputCallback) {
    $.ajax({
      url: 'https://api.ipify.org/?format=json',
      success: function(resp) {
        var ip = (resp && resp.ip) ? resp.ip : "";
        callback(ip, intlTelInputCallback);
      }
    });
  }

  static getCountryCode(ip, obj){
    $.get("https://ipapi.co/json/", function (response) {
      const countryCode = response.country.toLowerCase()
      obj.intlTelInput('setCountry', countryCode);
    }, "json");
  }
}
