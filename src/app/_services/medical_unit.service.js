var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
require('rxjs/add/operator/toPromise');
var MedicalUnitService = (function () {
    function MedicalUnitService(httpClient) {
        this.httpClient = httpClient;
    }
    MedicalUnitService.prototype.list = function () {
        return this.httpClient.get('/medical_units/list', {})
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    MedicalUnitService.prototype.index = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.get('/medical_units', data)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    MedicalUnitService.prototype.show = function (id) {
        return this.httpClient.get('/medical_units/' + id)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    MedicalUnitService.prototype.resendConfirmationEmail = function (id) {
        return this.httpClient.get('/medical_units/resend_confirmation_email/' + id)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    MedicalUnitService.prototype.getCrmStatus = function (id) {
        return this.httpClient.get('/medical_units/get_crm_status/' + id)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    MedicalUnitService.prototype.getAudits = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.get('/medical_units/' + data['id'] + '/audits', data)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    MedicalUnitService.prototype.create = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.post('/medical_units', { medical_unit: data })
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    MedicalUnitService.prototype.update = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.patch('/medical_units/' + data['id'], { medical_unit: data })
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    MedicalUnitService.prototype.updatePassword = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.patch('/medical_units/' + data['id'] + '/update_password', { medical_unit: data })
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    MedicalUnitService = __decorate([
        core_1.Injectable()
    ], MedicalUnitService);
    return MedicalUnitService;
})();
exports.MedicalUnitService = MedicalUnitService;
//# sourceMappingURL=medical_unit.service.js.map