import {Injectable} from "@angular/core";
import {HttpClient} from './http_client';
import 'rxjs/add/operator/toPromise';




@Injectable()
export class CallCenterService {
  constructor(public httpClient: HttpClient) {}

  index(data = {}): Promise<any> {
    return this.httpClient.get('/call_centers' , data)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }


  show(id): Promise<any> {
    return this.httpClient.get('/call_centers/' + id)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  resendConfirmationEmail(id): Promise<any> {
    return this.httpClient.get(`/call_centers/${id}/resend_confirmation_email`)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  getAudits(data = {}): Promise<any> {
    return this.httpClient.get('/call_centers/' + data['id'] + '/audits' , data)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  create(data = {}): Promise<any> {
    return this.httpClient.post('/call_centers', {call_center: data})
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  update(data = {}): Promise<any> {
    return this.httpClient.patch('/call_centers/' + data['id'], {call_center: data})
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  updatePassword(data = {}): Promise<any> {
    return this.httpClient.patch('/call_centers/' + data['id'] + '/update_password', {call_center: data})
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }


}
