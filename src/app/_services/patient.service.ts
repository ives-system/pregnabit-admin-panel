import {Injectable} from "@angular/core";
import {HttpClient} from './http_client';
import 'rxjs/add/operator/toPromise';




@Injectable()
export class PatientService {
  constructor(public httpClient: HttpClient) {}

  list(data = {}): Promise<any> {
    return this.httpClient.get('/patients/list' , data)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  index(data = {}): Promise<any> {
    return this.httpClient.get('/patients' , data)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  pregnancyWeek(data = {}): Promise<any> {
    return this.httpClient.get('/patients/' + data['id'] + '/pregnancy_week' , data)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }


  show(id): Promise<any> {
    return this.httpClient.get('/patients/' + id)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  resendConfirmationEmail(id): Promise<any> {
    return this.httpClient.get(`/patients/${id}/resend_confirmation_email`)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  create(data = {}): Promise<any> {
    return this.httpClient.post('/patients', {patient: data})
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  getAudits(data = {}): Promise<any> {
    return this.httpClient.get('/patients/' + data['id'] + '/audits' , data)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  update(data = {}): Promise<any> {
    console.log('patient update', data)
    return this.httpClient.patch('/patients/' + data['id'], {patient: data})
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  updatePassword(data = {}): Promise<any> {
    return this.httpClient.patch('/patients/' + data['id'] + '/update_password', {patient: data})
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

}
