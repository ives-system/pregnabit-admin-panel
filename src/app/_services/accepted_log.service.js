var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
require('rxjs/add/operator/toPromise');
var AcceptedLogService = (function () {
    function AcceptedLogService(httpClient) {
        this.httpClient = httpClient;
    }
    AcceptedLogService.prototype.index = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.get('/accepted_logs', data)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    AcceptedLogService.prototype.show = function (id) {
        return this.httpClient.get('/accepted_logs/' + id)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    AcceptedLogService.prototype.create = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.post('/accepted_logs', { accepted_log: data })
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    AcceptedLogService.prototype.update = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.patch('/accepted_logs/' + data['id'], { accepted_log: data })
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    AcceptedLogService.prototype.delete = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.delete('/accepted_logs/' + data['id'])
            .toPromise()
            .then(function (response) { return response.json(); });
    };
    AcceptedLogService = __decorate([
        core_1.Injectable()
    ], AcceptedLogService);
    return AcceptedLogService;
})();
exports.AcceptedLogService = AcceptedLogService;
//# sourceMappingURL=accepted_log.service.js.map