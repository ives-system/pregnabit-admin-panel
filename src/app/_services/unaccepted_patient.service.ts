import {Injectable} from "@angular/core";
import {HttpClient} from './http_client';
import 'rxjs/add/operator/toPromise';




@Injectable()
export class UnacceptedPatientService {
  constructor(public httpClient: HttpClient) {}


  index(data = {}): Promise<any> {
    return this.httpClient.get('/unaccepted_patients' , data)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }


  show(id): Promise<any> {
    return this.httpClient.get('/unaccepted_patients/' + id)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  resendConfirmationEmail(id): Promise<any> {
    return this.httpClient.get(`/unaccepted_patients/${id}/resend_confirmation_email`)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  update(data = {}): Promise<any> {
    return this.httpClient.patch('/unaccepted_patients/' + data['id'], {patient: data})
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }
}
