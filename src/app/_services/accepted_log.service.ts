import {Injectable} from "@angular/core";
import {HttpClient} from './http_client';
import 'rxjs/add/operator/toPromise';





@Injectable()
export class AcceptedLogService {
  constructor(public httpClient: HttpClient) {}

  index(data = {}): Promise<any> {
    return this.httpClient.get('/accepted_logs' , data)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  show(id): Promise<any> {
    return this.httpClient.get('/accepted_logs/' + id)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  create(data = {}): Promise<any> {
    return this.httpClient.post('/accepted_logs', {accepted_log: data})
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  update(data = {}): Promise<any> {
    return this.httpClient.patch('/accepted_logs/' + data['id'], {accepted_log: data})
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  delete(data = {}): Promise<any> {
    return this.httpClient.delete('/accepted_logs/' + data['id'])
      .toPromise()
      .then(response => response.json());
  }

}
