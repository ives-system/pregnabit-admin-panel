var http_1 = require('@angular/http');
require("rxjs/add/operator/map");
var environment_1 = require("../../environments/environment");
var url = environment_1.environment.url;
var apiUrl = environment_1.environment.apiUrl;
var TranslateHttpLoader = (function () {
    function TranslateHttpLoader(http) {
        this.http = http;
        this.digestPath = ".json";
    }
    TranslateHttpLoader.prototype.getTranslation = function (lang) {
        var _this = this;
        var headers = new http_1.Headers();
        this.getDigestPath(lang).subscribe(function (data) {
            _this.digestPath = "-" + data;
        });
        return this.http.get(url + ("/assets/locales/" + lang + this.digestPath), { headers: headers });
    };
    TranslateHttpLoader.prototype.getDigestPath = function (lang) {
        var headers = new http_1.Headers();
        return this.http.get(apiUrl + "/digest_path", { headers: headers, params: { lang: lang } })
            .map(function (res) { return res['data']; });
    };
    return TranslateHttpLoader;
})();
exports.TranslateHttpLoader = TranslateHttpLoader;
//# sourceMappingURL=translate_http_loader.js.map