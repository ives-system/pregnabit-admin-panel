var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
require('rxjs/add/operator/toPromise');
var AdminUserService = (function () {
    function AdminUserService(httpClient) {
        this.httpClient = httpClient;
    }
    AdminUserService.prototype.index = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.get('/admin_users', data)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    AdminUserService.prototype.show = function (id) {
        return this.httpClient.get('/admin_users/' + id)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    AdminUserService.prototype.create = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.post('/admin_users', { admin_user: data })
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    AdminUserService.prototype.update = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.patch('/admin_users/' + data['id'], { admin_user: data })
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    AdminUserService.prototype.updatePassword = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.patch('/admin_users/' + data['id'] + '/update_password', { admin_user: data })
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    AdminUserService = __decorate([
        core_1.Injectable()
    ], AdminUserService);
    return AdminUserService;
})();
exports.AdminUserService = AdminUserService;
//# sourceMappingURL=admin_user.service.js.map