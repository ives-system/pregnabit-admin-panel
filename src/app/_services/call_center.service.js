var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
require('rxjs/add/operator/toPromise');
var CallCenterService = (function () {
    function CallCenterService(httpClient) {
        this.httpClient = httpClient;
    }
    CallCenterService.prototype.index = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.get('/call_centers', data)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    CallCenterService.prototype.show = function (id) {
        return this.httpClient.get('/call_centers/' + id)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    CallCenterService.prototype.resendConfirmationEmail = function (id) {
        return this.httpClient.get('/call_centers/resend_confirmation_email/' + id)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    CallCenterService.prototype.getAudits = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.get('/call_centers/' + data['id'] + '/audits', data)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    CallCenterService.prototype.create = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.post('/call_centers', { call_center: data })
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    CallCenterService.prototype.update = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.patch('/call_centers/' + data['id'], { call_center: data })
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    CallCenterService.prototype.updatePassword = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.patch('/call_centers/' + data['id'] + '/update_password', { call_center: data })
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    CallCenterService = __decorate([
        core_1.Injectable()
    ], CallCenterService);
    return CallCenterService;
})();
exports.CallCenterService = CallCenterService;
//# sourceMappingURL=call_center.service.js.map