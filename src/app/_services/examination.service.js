var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
require('rxjs/add/operator/toPromise');
var ExaminationService = (function () {
    function ExaminationService(httpClient) {
        this.httpClient = httpClient;
    }
    ExaminationService.prototype.index = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.get('/examinations', data)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    ExaminationService.prototype.show = function (id) {
        return this.httpClient.get('/examinations/' + id)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    ExaminationService.prototype.update = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.patch('/examinations/' + data['id'], { examination: data })
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    ExaminationService.prototype.getDescriptions = function (id) {
        return this.httpClient.get('/examinations/' + id + '/descriptions')
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    ExaminationService.prototype.getComments = function (id) {
        return this.httpClient.get('/examinations/' + id + '/comments')
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    ExaminationService.prototype.downloadFile = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.get('/examinations/' + data['id'] + '/download_file', { file: data['file'] })
            .toPromise()
            .then(function (response) { return response['_body']; });
    };
    ExaminationService.prototype.deleteComment = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.delete('/examinations/' + data['id'] + '/comments/' + data['comment_id'])
            .toPromise()
            .then(function (response) { return response.json(); });
    };
    ExaminationService.prototype.deleteDescription = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.delete('/examinations/' + data['id'] + '/descriptions/' + data['description_id'])
            .toPromise()
            .then(function (response) { return response.json(); });
    };
    ExaminationService.prototype.getStats = function () {
        return this.httpClient.get('/examinations/stats')
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    ExaminationService = __decorate([
        core_1.Injectable()
    ], ExaminationService);
    return ExaminationService;
})();
exports.ExaminationService = ExaminationService;
//# sourceMappingURL=examination.service.js.map