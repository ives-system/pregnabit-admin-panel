var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var http_2 = require('@angular/http');
var environment_1 = require('../../environments/environment');
var apiUrl = environment_1.environment.apiUrl;
var HttpClient = (function () {
    function HttpClient(http) {
        this.http = http;
    }
    HttpClient.prototype.get = function (url, data) {
        if (data === void 0) { data = {}; }
        var headers = new http_1.Headers();
        headers.append('X-Authentication-Token', localStorage.getItem('token'));
        var params = new http_2.URLSearchParams();
        if (data != null) {
            for (var key in data) {
                if (data[key] != null) {
                    params.set(key, data[key].toString());
                }
            }
        }
        return this.http.get(apiUrl + url, { headers: headers, search: params });
    };
    HttpClient.prototype.post = function (url, data) {
        if (data === void 0) { data = {}; }
        var headers = new http_1.Headers();
        headers.append('X-Authentication-Token', localStorage.getItem('token'));
        return this.http.post(apiUrl + url, data, { headers: headers });
    };
    HttpClient.prototype.put = function (url, data) {
        if (data === void 0) { data = {}; }
        var headers = new http_1.Headers();
        headers.append('X-Authentication-Token', localStorage.getItem('token'));
        return this.http.put(apiUrl + url, data, { headers: headers });
    };
    HttpClient.prototype.patch = function (url, data) {
        if (data === void 0) { data = {}; }
        var headers = new http_1.Headers();
        headers.append('X-Authentication-Token', localStorage.getItem('token'));
        return this.http.patch(apiUrl + url, data, { headers: headers });
    };
    HttpClient.prototype.delete = function (url, data) {
        if (data === void 0) { data = {}; }
        var headers = new http_1.Headers();
        headers.append('X-Authentication-Token', localStorage.getItem('token'));
        return this.http.delete(apiUrl + url, { headers: headers, body: data });
    };
    HttpClient = __decorate([
        core_1.Injectable()
    ], HttpClient);
    return HttpClient;
})();
exports.HttpClient = HttpClient;
//# sourceMappingURL=http_client.js.map