import {Injectable} from "@angular/core";
import {HttpClient} from './http_client';
import 'rxjs/add/operator/toPromise';


@Injectable()
export class ValidatorsService {
  constructor(public httpClient: HttpClient) {}


  adminUserEmailUniqueness(email): Promise<any> {
    return this.httpClient.get('/validators/admin_user_email_uniqueness', {email: email})
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }


}
