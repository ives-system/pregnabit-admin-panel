import {Injectable} from "@angular/core";
import {HttpClient} from './http_client';
import 'rxjs/add/operator/toPromise';





@Injectable()
export class AdminUserService {
  constructor(public httpClient: HttpClient) {}

  index(data = {}): Promise<any> {
    return this.httpClient.get('/admin_users' , data)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }


  show(id): Promise<any> {
    return this.httpClient.get('/admin_users/' + id)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  create(data = {}): Promise<any> {
    return this.httpClient.post('/admin_users', {admin_user: data})
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  update(data = {}): Promise<any> {
    return this.httpClient.patch('/admin_users/' + data['id'], {admin_user: data})
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  updatePassword(data = {}): Promise<any> {
    return this.httpClient.patch('/admin_users/' + data['id'] + '/update_password', {admin_user: data})
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

}
