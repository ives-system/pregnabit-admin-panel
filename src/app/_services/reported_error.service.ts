import {Injectable} from "@angular/core";
import {HttpClient} from './http_client';
import 'rxjs/add/operator/toPromise';




@Injectable()
export class ReportedErrorService {
  constructor(public httpClient: HttpClient) {}


  index(data = {}): Promise<any> {
    return this.httpClient.get('/reported_errors' , data)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  show(id): Promise<any> {
    return this.httpClient.get('/reported_errors/' + id)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  update(data = {}): Promise<any> {
    return this.httpClient.patch('/reported_errors/' + data['id'], {reported_error: data})
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }


}
