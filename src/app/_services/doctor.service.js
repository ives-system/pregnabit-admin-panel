var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
require('rxjs/add/operator/toPromise');
var DoctorService = (function () {
    function DoctorService(httpClient) {
        this.httpClient = httpClient;
    }
    DoctorService.prototype.list = function () {
        return this.httpClient.get('/doctors/list', {})
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    DoctorService.prototype.index = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.get('/doctors', data)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    DoctorService.prototype.show = function (id) {
        return this.httpClient.get('/doctors/' + id)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    DoctorService.prototype.resendConfirmationEmail = function (id) {
        return this.httpClient.get('/doctors/resend_confirmation_email/' + id)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    DoctorService.prototype.getCrmStatus = function (id) {
        return this.httpClient.get('/doctors/get_crm_status/' + id)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    DoctorService.prototype.create = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.post('/doctors', { doctor: data })
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    DoctorService.prototype.getAudits = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.get('/doctors/' + data['id'] + '/audits', data)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    DoctorService.prototype.update = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.patch('/doctors/' + data['id'], { doctor: data })
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    DoctorService.prototype.updatePassword = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.patch('/doctors/' + data['id'] + '/update_password', { doctor: data })
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    DoctorService = __decorate([
        core_1.Injectable()
    ], DoctorService);
    return DoctorService;
})();
exports.DoctorService = DoctorService;
//# sourceMappingURL=doctor.service.js.map