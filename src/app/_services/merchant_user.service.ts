import {Injectable} from "@angular/core";
import {HttpClient} from './http_client';
import 'rxjs/add/operator/toPromise';



@Injectable()
export class MerchantUserService {
  constructor(public httpClient: HttpClient) {}

  index(data = {}): Promise<any> {
    return this.httpClient.get('/merchant_users', data)
      .toPromise()
      .then(response => response.json())
      .then(response => response)
  }

  list(): Promise<any> {
    return this.httpClient.get('/merchant_users/list' , {})
      .toPromise()
      .then(response => response.json())
      .then(response => response)  }


  show(id): Promise<any> {
    return this.httpClient.get('/merchant_users/' + id)
      .toPromise()
      .then(response => response.json())
      .then(response => response)  }

  create(data = {}): Promise<any> {
    return this.httpClient.post('/merchant_users', {merchant_user: data})
      .toPromise()
      .then(response => response.json())
      .then(response => response)  }

  update(data = {}): Promise<any> {
    return this.httpClient.patch('/merchant_users/' + data['id'], {merchant_user: data})
      .toPromise()
      .then(response => response.json())
      .then(response => response)  }

  updatePassword(data = {}): Promise<any> {
    return this.httpClient.patch('/merchant_users/' + data['id'] + '/update_password', {merchant_user: data})
      .toPromise()
      .then(response => response.json())
      .then(response => response)  }

}
