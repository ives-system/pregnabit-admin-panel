import {Injectable} from "@angular/core";
import {HttpClient} from './http_client';
import 'rxjs/add/operator/toPromise';




@Injectable()
export class LoginHistoryService {
  constructor(public httpClient: HttpClient) {}


  index(data = {}): Promise<any> {
    return this.httpClient.get('/login_histories' , data)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }



}
