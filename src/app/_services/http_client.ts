import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import {URLSearchParams} from '@angular/http';
import { environment } from '../../environments/environment';

let apiUrl = environment.apiUrl;

@Injectable()
export class HttpClient {
  constructor(
    private http: Http
  ) {}

  get(url, data = {}) {
    let headers = new Headers();
    headers.append('X-Authentication-Token', localStorage.getItem('token'));
    let params: URLSearchParams = new URLSearchParams();

    if (data != null) {
      for (let key in data) {
        if (data[key] != null) {
          params.set(key, data[key].toString());
        }
      }
    }

    return this.http.get(apiUrl+url, {headers: headers, search: params, withCredentials: true});
  }

  post(url, data = {}) {
    let headers = new Headers();
    headers.append('X-Authentication-Token', localStorage.getItem('token'));
    return this.http.post(apiUrl+url, data, {headers: headers, withCredentials: true});
  }

  put(url, data = {}) {
    let headers = new Headers();
    headers.append('X-Authentication-Token', localStorage.getItem('token'));
    return this.http.put(apiUrl+url, data, {headers: headers, withCredentials: true});
  }

  patch(url, data = {}) {
    let headers = new Headers();
    headers.append('X-Authentication-Token', localStorage.getItem('token'));
    return this.http.patch(apiUrl+url, data, {headers: headers, withCredentials: true});
  }

  delete(url, data = {}) {
    let headers = new Headers();
    headers.append('X-Authentication-Token', localStorage.getItem('token'));
    return this.http.delete(apiUrl+url, {headers: headers, body: data, withCredentials: true});
  }
}
