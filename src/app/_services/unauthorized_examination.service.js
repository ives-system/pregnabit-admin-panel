var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
require('rxjs/add/operator/toPromise');
var UnauthorizedExaminationService = (function () {
    function UnauthorizedExaminationService(httpClient) {
        this.httpClient = httpClient;
    }
    UnauthorizedExaminationService.prototype.index = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.get('/unauthorized_examinations', data)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    UnauthorizedExaminationService.prototype.show = function (id) {
        return this.httpClient.get('/unauthorized_examinations/' + id)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    UnauthorizedExaminationService.prototype.update = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.put('/unauthorized_examinations/' + data['id'], { unauthorized_examination: data })
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    UnauthorizedExaminationService.prototype.authorize = function (id) {
        return this.httpClient.get('/unauthorized_examinations/' + id + '/authorize')
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    UnauthorizedExaminationService = __decorate([
        core_1.Injectable()
    ], UnauthorizedExaminationService);
    return UnauthorizedExaminationService;
})();
exports.UnauthorizedExaminationService = UnauthorizedExaminationService;
//# sourceMappingURL=unauthorized_examination.service.js.map