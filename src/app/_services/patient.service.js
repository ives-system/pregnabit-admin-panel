var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
require('rxjs/add/operator/toPromise');
var PatientService = (function () {
    function PatientService(httpClient) {
        this.httpClient = httpClient;
    }
    PatientService.prototype.list = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.get('/patients/list', data)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    PatientService.prototype.index = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.get('/patients', data)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    PatientService.prototype.pregnancyWeek = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.get('/patients/' + data['id'] + '/pregnancy_week', data)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    PatientService.prototype.show = function (id) {
        return this.httpClient.get('/patients/' + id)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    PatientService.prototype.resendConfirmationEmail = function (id) {
        return this.httpClient.get('/patients/resend_confirmation_email/' + id)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    PatientService.prototype.create = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.post('/patients', { patient: data })
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    PatientService.prototype.getAudits = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.get('/patients/' + data['id'] + '/audits', data)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    PatientService.prototype.update = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.patch('/patients/' + data['id'], { patient: data })
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    PatientService.prototype.updatePassword = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.patch('/patients/' + data['id'] + '/update_password', { patient: data })
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    PatientService = __decorate([
        core_1.Injectable()
    ], PatientService);
    return PatientService;
})();
exports.PatientService = PatientService;
//# sourceMappingURL=patient.service.js.map