import {Injectable} from "@angular/core";
import {HttpClient} from './http_client';
import 'rxjs/add/operator/toPromise';




@Injectable()
export class DoctorService {
  constructor(public httpClient: HttpClient) {}

  list(): Promise<any> {
    return this.httpClient.get('/doctors/list' , {})
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  index(data = {}): Promise<any> {
    return this.httpClient.get('/doctors' , data)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }


  show(id): Promise<any> {
    return this.httpClient.get('/doctors/' + id)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  resendConfirmationEmail(id): Promise<any> {
    return this.httpClient.get(`/doctors/${id}/resend_confirmation_email`)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  getCrmStatus(id): Promise<any> {
    return this.httpClient.get('/doctors/get_crm_status/' + id)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  create(data = {}): Promise<any> {
    return this.httpClient.post('/doctors', {doctor: data})
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  getAudits(data = {}): Promise<any> {
    return this.httpClient.get('/doctors/' + data['id'] + '/audits' , data)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  update(data = {}): Promise<any> {
    return this.httpClient.patch('/doctors/' + data['id'], {doctor: data})
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  updatePassword(data = {}): Promise<any> {
    return this.httpClient.patch('/doctors/' + data['id'] + '/update_password', {doctor: data})
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }


}
