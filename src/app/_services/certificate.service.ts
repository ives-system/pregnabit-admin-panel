import {Injectable} from "@angular/core";
import {HttpClient} from './http_client';
import 'rxjs/add/operator/toPromise';





@Injectable()
export class CertificateService {
  constructor(public httpClient: HttpClient) {}

  index(data = {}): Promise<any> {
    return this.httpClient.get('/certificates' , data)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  show(id): Promise<any> {
    return this.httpClient.get('/certificates/' + id)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  activate(id): Promise<any> {
    return this.httpClient.get('/certificates/activate/' + id)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  deactivate(id): Promise<any> {
    return this.httpClient.get('/certificates/deactivate/' + id)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

}
