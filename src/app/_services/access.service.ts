import {Injectable} from "@angular/core";
import {HttpClient} from './http_client';
import 'rxjs/add/operator/toPromise';





@Injectable()
export class AccessService {
  constructor(public httpClient: HttpClient) {}

  index(): Promise<any> {
    return this.httpClient.get('/access')
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  updateComment(comment): Promise<any> {
    return this.httpClient.patch('/access/update_comment', {comment: comment})
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  lockForPatients(): Promise<any> {
    return this.httpClient.patch('/access/lock_portal_for_patient')
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  unlockForPatients(): Promise<any> {
    return this.httpClient.patch('/access/unlock_portal_for_patient')
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  lockForDoctors(): Promise<any> {
    return this.httpClient.patch('/access/lock_portal_for_doctors')
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  unlockForDoctors(): Promise<any> {
    return this.httpClient.patch('/access/unlock_portal_for_doctors')
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  lockForCallCenters(): Promise<any> {
    return this.httpClient.patch('/access/lock_portal_for_call_centers')
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  unlockForCallCenters(): Promise<any> {
    return this.httpClient.patch('/access/unlock_portal_for_call_centers')
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

}
