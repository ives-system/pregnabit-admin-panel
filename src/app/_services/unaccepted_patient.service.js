var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
require('rxjs/add/operator/toPromise');
var UnacceptedPatientService = (function () {
    function UnacceptedPatientService(httpClient) {
        this.httpClient = httpClient;
    }
    UnacceptedPatientService.prototype.index = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.get('/unaccepted_patients', data)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    UnacceptedPatientService.prototype.show = function (id) {
        return this.httpClient.get('/unaccepted_patients/' + id)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    UnacceptedPatientService.prototype.resendConfirmationEmail = function (id) {
        return this.httpClient.get('/unaccepted_patients/resend_confirmation_email/' + id)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    UnacceptedPatientService.prototype.update = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.patch('/unaccepted_patients/' + data['id'], { patient: data })
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    UnacceptedPatientService = __decorate([
        core_1.Injectable()
    ], UnacceptedPatientService);
    return UnacceptedPatientService;
})();
exports.UnacceptedPatientService = UnacceptedPatientService;
//# sourceMappingURL=unaccepted_patient.service.js.map