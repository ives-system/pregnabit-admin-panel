var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require('@angular/core');
var chart_js_1 = require('chart.js');
var PregnabitChart = (function () {
    function PregnabitChart() {
    }
    PregnabitChart.prototype.chartTextStyle = function () {
        return { color: '#7f8c8d', fontSize: '13', fontName: "'Fira Sans', sans-serif" };
    };
    PregnabitChart.prototype.chart = function (cols, series, height, width, vAxis, hAxis, chartArea) {
    };
    PregnabitChart.prototype.showTime = function (t) {
        var seconds = parseInt(t) * 60;
        var minutes = seconds / 60;
        var result = [];
        if (minutes != 0 || (minutes == 0 && seconds == 0)) {
            result.push(minutes + " min");
        }
        if (seconds != 0 || (minutes == 0 && seconds == 0)) {
            result.push(seconds % 60 + " s");
        }
        var stringResult = result.join(' ');
        return stringResult;
    };
    PregnabitChart.prototype.chartHeartRate = function (heartRatesArray, chartImg, scale, showHrLine) {
        if (scale === void 0) { scale = 1; }
        if (showHrLine === void 0) { showHrLine = true; }
        var pointRadius = 2;
        if (showHrLine) {
            pointRadius = 0;
        }
        var scatterChartData = {
            datasets: [{
                    data: heartRatesArray.fhr,
                    borderColor: '#88b26a',
                    pointBackgroundColor: '#88b26a',
                    fill: false,
                    pointRadius: 0,
                    pointHitRadius: 5,
                    lineTension: 0,
                    borderWidth: 1,
                    showLine: true
                }, {
                    data: heartRatesArray.hr,
                    borderColor: '#77b8e4',
                    pointBackgroundColor: '#77b8e4',
                    fill: false,
                    pointRadius: pointRadius,
                    pointHitRadius: 5,
                    lineTension: 0,
                    borderWidth: 1,
                    showLine: showHrLine
                }, {
                    data: [{ x: 0.03, y: 110 }, { x: 30, y: 110 }],
                    borderColor: '#f2ffe8',
                    fill: false,
                    pointRadius: pointRadius,
                    pointHitRadius: 5,
                    lineTension: 0,
                    borderWidth: 1,
                    showLine: true
                }, {
                    data: [{ x: 0.03, y: 160 }, { x: 30, y: 160 }],
                    borderColor: '#f2ffe8',
                    backgroundColor: 'rgba(242,255,232,0.5)',
                    fill: 2,
                    pointRadius: pointRadius,
                    pointHitRadius: 5,
                    lineTension: 0,
                    borderWidth: 1,
                    showLine: true
                }
            ]
        };
        var ref = this;
        var options = {
            animation: false,
            responsive: false,
            legend: {
                display: false
            },
            scales: {
                xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: '[min]'
                        },
                        ticks: {
                            min: 0,
                            max: 30,
                            stepSize: 1
                        }
                    }],
                yAxes: [{
                        ticks: {
                            min: 40,
                            max: 240,
                            stepSize: 20
                        }
                    }]
            },
            tooltips: {
                callbacks: {
                    afterLabel: function (tooltipItem, data) {
                        if (tooltipItem.datasetIndex == 0) {
                            return "FHR " + tooltipItem.yLabel;
                        }
                        else {
                            return "HR " + tooltipItem.yLabel;
                        }
                    },
                    label: function (tooltipItem, data) {
                        return ref.showTime(parseFloat(tooltipItem.xLabel));
                    }
                }
            },
            bands: {
                bottomBound: 110,
                topBound: 160,
                highlightColor: '#f2ffe8'
            }
        };
        var canvas = document.getElementById("chart-hr");
        var ctx = canvas.getContext("2d");
        var config = {
            data: scatterChartData,
            options: options
        };
        var chart = chart_js_1.Chart.Scatter(ctx, config);
        chart.chart.canvas.setAttribute('scale', scale);
        chart.render(0, true);
        chartImg['hr'] = chart.toBase64Image();
        return chart;
    };
    PregnabitChart.prototype.customTooltip = function (time, val, type) {
        return "<div style='border: 1px solid; background-color: white;box-shadow: 3px 3px 5px #888888;width: 65px;position: relative;left:-50px;top: -10px;z-index: 9999'>#{@showTime(time)}<br>#{type} #{parseInt(val)}</div>";
    };
    PregnabitChart.prototype.chartCramps = function (crampArray, chartImg, scale) {
        if (scale === void 0) { scale = 1; }
        var cols = [
            {
                id: "time",
                label: "Czas",
                type: "number",
                p: {}
            },
            {
                id: "cramp",
                label: "Skurcz macicy",
                type: "number",
                p: {}
            },
            {
                type: 'string',
                role: 'tooltip',
                p: { 'html': true }
            }
        ];
        var scatterChartData = {
            datasets: [{
                    data: crampArray,
                    borderColor: '#9f6dc5',
                    pointBackgroundColor: '#9f6dc5',
                    fill: false,
                    pointRadius: 0,
                    pointHitRadius: 5,
                    lineTension: 0,
                    borderWidth: 1,
                    showLine: true
                }]
        };
        var ref = this;
        var options = {
            animation: false,
            responsive: false,
            legend: {
                display: false
            },
            tooltips: {
                callbacks: {
                    afterLabel: function (tooltipItem, data) {
                        return "UC " + tooltipItem.yLabel;
                    },
                    label: function (tooltipItem, data) {
                        return ref.showTime(parseFloat(tooltipItem.xLabel));
                    }
                }
            },
            scales: {
                xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: '[min]'
                        },
                        ticks: {
                            min: 0,
                            max: 30,
                            stepSize: 1
                        }
                    }],
                yAxes: [{
                        ticks: {
                            min: 0,
                            max: 100,
                            stepSize: 10
                        }
                    }]
            }
        };
        var canvas = document.getElementById("chart-cramps");
        var ctx = canvas.getContext("2d");
        var config = {
            data: scatterChartData,
            options: options
        };
        var chart = chart_js_1.Chart.Scatter(ctx, config);
        chart.chart.canvas.setAttribute('scale', scale);
        chart.render(0, true);
        chartImg['cramps'] = chart.toBase64Image();
        return chart;
    };
    PregnabitChart = __decorate([
        core_1.Injectable()
    ], PregnabitChart);
    return PregnabitChart;
})();
exports.PregnabitChart = PregnabitChart;
//# sourceMappingURL=pregnabit_chart.js.map