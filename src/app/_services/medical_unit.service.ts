import {Injectable} from "@angular/core";
import {HttpClient} from './http_client';
import 'rxjs/add/operator/toPromise';




@Injectable()
export class MedicalUnitService {
  constructor(public httpClient: HttpClient) {}

  list(): Promise<any> {
    return this.httpClient.get('/medical_units/list' , {})
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  index(data = {}): Promise<any> {
    return this.httpClient.get('/medical_units' , data)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }


  show(id): Promise<any> {
    return this.httpClient.get('/medical_units/' + id)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  resendConfirmationEmail(id): Promise<any> {
    return this.httpClient.get(`/medical_units/${id}/resend_confirmation_email`)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  getCrmStatus(id): Promise<any> {
    return this.httpClient.get('/medical_units/get_crm_status/' + id)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  getAudits(data = {}): Promise<any> {
    return this.httpClient.get('/medical_units/' + data['id'] + '/audits' , data)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  create(data = {}): Promise<any> {
    return this.httpClient.post('/medical_units', {medical_unit: data})
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  update(data = {}): Promise<any> {
    return this.httpClient.patch('/medical_units/' + data['id'], {medical_unit: data})
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  updatePassword(data = {}): Promise<any> {
    return this.httpClient.patch('/medical_units/' + data['id'] + '/update_password', {medical_unit: data})
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

}
