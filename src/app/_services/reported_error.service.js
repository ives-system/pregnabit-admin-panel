var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
require('rxjs/add/operator/toPromise');
var ReportedErrorService = (function () {
    function ReportedErrorService(httpClient) {
        this.httpClient = httpClient;
    }
    ReportedErrorService.prototype.index = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.get('/reported_errors', data)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    ReportedErrorService.prototype.show = function (id) {
        return this.httpClient.get('/reported_errors/' + id)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    ReportedErrorService.prototype.update = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.patch('/reported_errors/' + data['id'], { reported_error: data })
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    ReportedErrorService = __decorate([
        core_1.Injectable()
    ], ReportedErrorService);
    return ReportedErrorService;
})();
exports.ReportedErrorService = ReportedErrorService;
//# sourceMappingURL=reported_error.service.js.map