var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
require('rxjs/add/operator/toPromise');
var DeviceService = (function () {
    function DeviceService(httpClient) {
        this.httpClient = httpClient;
    }
    DeviceService.prototype.index = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.get('/devices', data)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    DeviceService.prototype.list = function () {
        return this.httpClient.get('/devices/list', {})
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    DeviceService.prototype.show = function (id) {
        return this.httpClient.get('/devices/' + id)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    DeviceService.prototype.create = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.post('/devices', { device: data })
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    DeviceService.prototype.update = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.patch('/devices/' + data['id'], { device: data })
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    DeviceService.prototype.getAudits = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.get('/devices/' + data['id'] + '/audits', data)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    DeviceService = __decorate([
        core_1.Injectable()
    ], DeviceService);
    return DeviceService;
})();
exports.DeviceService = DeviceService;
//# sourceMappingURL=device.service.js.map