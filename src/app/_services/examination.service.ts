import {Injectable} from "@angular/core";
import {HttpClient} from './http_client';
import 'rxjs/add/operator/toPromise';





@Injectable()
export class ExaminationService {
  constructor(public httpClient: HttpClient) {}

  index(data = {}): Promise<any> {
    return this.httpClient.get('/examinations' , data)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  show(id): Promise<any> {
    return this.httpClient.get('/examinations/' + id)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  update(data = {}): Promise<any> {
    return this.httpClient.patch('/examinations/' + data['id'], {examination: data})
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  getDescriptions(id): Promise<any> {
    return this.httpClient.get('/examinations/' + id + '/descriptions')
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  getComments(id): Promise<any> {
    return this.httpClient.get('/examinations/' + id + '/comments')
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  downloadFile(data = {}): Promise<any> {
    return this.httpClient.get('/examinations/' + data['id'] + '/download_file', {file: data['file']})
      .toPromise()
      .then(response => response['_body']);
  }

  deleteComment(data = {}): Promise<any> {
    return this.httpClient.delete('/examinations/' + data['id'] + '/comments/' + data['comment_id'])
      .toPromise()
      .then(response => response.json());
  }

  deleteDescription(data = {}): Promise<any> {
    return this.httpClient.delete('/examinations/' + data['id'] + '/descriptions/' + data['description_id'])
      .toPromise()
      .then(response => response.json());
  }

  getStats(): Promise<any> {
    return this.httpClient.get('/examinations/stats')
      .toPromise()
      .then(response => response.json());
  }

  getAudits(data= {}): Promise<any> {
    return this.httpClient.get('/examinations/' + data['id'] + '/audits', data)
      .toPromise()
      .then(response => response.json());
  }


}
