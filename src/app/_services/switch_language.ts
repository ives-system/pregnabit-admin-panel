import { Injectable} from "@angular/core";
import { TranslateService } from '@ngx-translate/core';
import { TranslateModule } from '@ngx-translate/core';
import {HttpClient} from './http_client';

@Injectable()
export class SwitchLanguage{

  constructor(
    public translate: TranslateService,
    public httpClient: HttpClient ) {}

  switchLanguage(language: string) {
    this.translate.use(language);
    localStorage.setItem('language', language);
    location.reload();
  }

}
