import {Injectable} from "@angular/core";
import {HttpClient} from './http_client';
import 'rxjs/add/operator/toPromise';





@Injectable()
export class DeviceService {
  constructor(public httpClient: HttpClient) {}

  index(data = {}): Promise<any> {
    return this.httpClient.get('/devices' , data)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  list(): Promise<any> {
    return this.httpClient.get('/devices/list' , {})
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }


  show(id): Promise<any> {
    return this.httpClient.get('/devices/' + id)
      .toPromise()
      .then(res => res.json())
      .then(res => res)
      .catch(e => console.log(e))

  }

  create(data = {}): Promise<any> {
    return this.httpClient.post('/devices', {device: data})
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  update(data = {}): Promise<any> {
    return this.httpClient.patch('/devices/' + data['id'], {device: data})
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  getAudits(data = {}): Promise<any> {
    return this.httpClient.get('/devices/' + data['id'] + '/audits', data)
      .toPromise()
      .then( res => res.json())
      .then(res => res)
      .catch(e => console.log('e', e))
  }
}
