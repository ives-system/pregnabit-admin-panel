import {Injectable} from "@angular/core";
import {HttpClient} from './http_client';
import 'rxjs/add/operator/toPromise';





@Injectable()
export class UnauthorizedExaminationService {
  constructor(public httpClient: HttpClient) {}

  index(data = {}): Promise<any> {
    return this.httpClient.get('/unauthorized_examinations' , data)
      .toPromise()
      .then(res => res.json())
      .then(res => res)
  }

  show(id): Promise<any> {
    return this.httpClient.get('/unauthorized_examinations/' + id)
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  update(data = {}): Promise<any> {
    return this.httpClient.put('/unauthorized_examinations/' + data['id'], {unauthorized_examination: data})
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

  authorize(id): Promise<any> {
    return this.httpClient.get('/unauthorized_examinations/' + id + '/authorize')
      .toPromise()
      .then(res => res.json())
      .then(res => res)  }

}
