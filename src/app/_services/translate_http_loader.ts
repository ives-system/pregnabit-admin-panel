import {Http, Headers} from '@angular/http';
import {TranslateLoader} from "@ngx-translate/core";
import { Observable } from 'rxjs/Observable';
import "rxjs/add/operator/map";
import {environment} from "../../environments/environment";

let url = environment.url;
let apiUrl = environment.apiUrl;


export class TranslateHttpLoader implements TranslateLoader {

  constructor(
    private http: Http
  ) {}


  public getTranslation(lang: string): any {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json')
    let language = lang
    if (lang === null) {
      language = 'pl'
    }
    return this.http.get(apiUrl + `/locales?lang=${language}`, { headers: headers, withCredentials: true })
  }


}
