var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
require('rxjs/add/operator/toPromise');
var AccessService = (function () {
    function AccessService(httpClient) {
        this.httpClient = httpClient;
    }
    AccessService.prototype.index = function () {
        return this.httpClient.get('/access')
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    AccessService.prototype.updateComment = function (comment) {
        return this.httpClient.patch('/access/update_comment', { comment: comment })
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    AccessService.prototype.lockForPatients = function () {
        return this.httpClient.patch('/access/lock_portal_for_patient')
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    AccessService.prototype.unlockForPatients = function () {
        return this.httpClient.patch('/access/unlock_portal_for_patient')
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    AccessService.prototype.lockForDoctors = function () {
        return this.httpClient.patch('/access/lock_portal_for_doctors')
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    AccessService.prototype.unlockForDoctors = function () {
        return this.httpClient.patch('/access/unlock_portal_for_doctors')
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    AccessService.prototype.lockForCallCenters = function () {
        return this.httpClient.patch('/access/lock_portal_for_call_centers')
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    AccessService.prototype.unlockForCallCenters = function () {
        return this.httpClient.patch('/access/unlock_portal_for_call_centers')
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    AccessService = __decorate([
        core_1.Injectable()
    ], AccessService);
    return AccessService;
})();
exports.AccessService = AccessService;
//# sourceMappingURL=access.service.js.map