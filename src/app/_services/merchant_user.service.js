var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
require('rxjs/add/operator/toPromise');
var MerchantUserService = (function () {
    function MerchantUserService(httpClient) {
        this.httpClient = httpClient;
    }
    MerchantUserService.prototype.index = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.get('/merchant_users', data)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    MerchantUserService.prototype.list = function () {
        return this.httpClient.get('/merchant_users/list', {})
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    MerchantUserService.prototype.show = function (id) {
        return this.httpClient.get('/merchant_users/' + id)
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    MerchantUserService.prototype.create = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.post('/merchant_users', { merchant_user: data })
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    MerchantUserService.prototype.update = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.patch('/merchant_users/' + data['id'], { merchant_user: data })
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    MerchantUserService.prototype.updatePassword = function (data) {
        if (data === void 0) { data = {}; }
        return this.httpClient.patch('/merchant_users/' + data['id'] + '/update_password', { merchant_user: data })
            .toPromise()
            .then(function (response) { return response.json().data; });
    };
    MerchantUserService = __decorate([
        core_1.Injectable()
    ], MerchantUserService);
    return MerchantUserService;
})();
exports.MerchantUserService = MerchantUserService;
//# sourceMappingURL=merchant_user.service.js.map