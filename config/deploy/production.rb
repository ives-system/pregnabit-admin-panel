ask(:environment_dev_name, 'preprod')

server "#{fetch(:environment_dev_name)}.pregnabit.com", user: 'ubuntu', roles: %w{web app db}

set :branch, 'master'
