lock '3.11.0'

set :application, 'admin'
set :repo_url, 'git@bitbucket.org:rafal_piekara/admin-panel.git'
set :deploy_to, '/var/www/admin'

namespace :deploy do

  desc 'Admin panel build'
  task :admin_deploy do
      on roles(:web) do
#         execute "cd #{release_path} && npm install"
        execute "cd #{release_path} && yarn install"
      end
       on roles(:web) do
         execute "cd #{release_path} && ./node_modules/@angular/cli/bin/ng build --env=#{fetch(:environment_dev_name)}"
       end
  end
    after 'deploy:published', :admin_deploy
end
